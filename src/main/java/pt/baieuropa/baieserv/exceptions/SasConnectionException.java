package pt.baieuropa.baieserv.exceptions;

public class SasConnectionException extends Exception {

    public SasConnectionException(String input) {
        super(input);
    }

}
