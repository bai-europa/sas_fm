package pt.baieuropa.baieserv.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;

@Slf4j
public class ConfigUtils {
	
	private static Properties config;
	public static Properties loadProps(String fileName) {
		try {
			config = new Properties();
			InputStream inputStream = new FileInputStream(fileName);
			if (inputStream != null) {

				config.load(inputStream);

			} else {
				throw new FileNotFoundException("property file '" + fileName + "' not found in the classpath");

			}
		} catch (IOException ex) {
			log.error("Error reading propertie file\n"+ex.getMessage());
		}
		return config;
	}

}
