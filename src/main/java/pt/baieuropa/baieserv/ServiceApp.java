/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import pt.baieuropa.baieserv.processor.Process;
import pt.baieuropa.baieserv.utils.ConfigUtils;

import java.util.Properties;

@SpringBootApplication
@ComponentScan({"pt.baieuropa.baieserv.service.controllers"})
public class ServiceApp {

    public static Process proc;
    public static Properties config = ConfigUtils.loadProps(System.getenv("URLS_CONFIGURATION"));
    public static String partiesURL = config.getProperty("partiesURL");
    public static String relationsURL = config.getProperty("relationsURL");

    public static void main(String[] args) {
        SpringApplication.run(ServiceApp.class, args);
        proc = new Process("SERVICE");
    }

    public static Process getProc() {
        return proc;
    }

}
