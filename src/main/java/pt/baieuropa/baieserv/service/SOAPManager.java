package pt.baieuropa.baieserv.service;


import javax.xml.soap.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;

@Slf4j
public class SOAPManager {

    private static HashMap<String, Object> map = new HashMap<>();

    private static String action = "";
    private static String bodyNamespace = "";
    private static String bodyNamespaceURI = "";
    private static String elementNamespace = "xsd";
    private static String elementNamespaceURI = "";
    private static String headerName = "SOAPAction";

    public void callSoapWebService(String soapEndpointUrl, String soapAction, String receivedBodyNamespace,
                                String receivedBodyNamespaceURI, String receivedElementNamespace,
                                String receivedXSDNamespaceURI, HashMap<String, Object> receivedMap) {
        try {
            log.debug("Creating SOAP Connection...");
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();
            log.debug("SOAP Connection Established.");

            // Set's global variables
            bodyNamespace = receivedBodyNamespace;
            bodyNamespaceURI = receivedBodyNamespaceURI;
            elementNamespace = receivedElementNamespace;
            elementNamespaceURI = receivedXSDNamespaceURI;
            action = soapAction;
            map = receivedMap;

            // Send SOAP Message to SOAP Server
            try {
                SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(soapEndpointUrl + soapAction), soapEndpointUrl);
                //soapResponse.writeTo(System.out);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                soapResponse.writeTo(baos);
                log.debug("SOAP Response: \n" + baos.toString());
            } catch (Exception ex) {
                log.error("Exception while sending SOAP Message to SOAP Server: \n" + ex.getMessage());
            }

            soapConnection.close();
            log.debug("SOAP Connection Closed.");
        } catch (Exception e) {
            log.info("\nError occurred while sending SOAP Request to Server!" +
                     "\nMake sure you have the correct endpoint URL and SOAPAction!" +
                     "\n" + e.getMessage());

        }
    }

    public SOAPMessage createSOAPRequest(String soapAction) throws Exception {
        log.debug("Creating SOAP Request...");

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        try {
            Thread.sleep(500);
            createSoapEnvelope(soapMessage);

            MimeHeaders headers = soapMessage.getMimeHeaders();
            headers.addHeader(headerName, soapAction);

            soapMessage.saveChanges();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapMessage.writeTo(out);
            String strMsg = new String(out.toByteArray());

            log.debug("\nRequest Received: \n" + strMsg + "\n\n");

        } catch (Exception ex) {
            log.error("Exception while Creating SOAP Request: \n" + ex.getMessage());
        }

        log.debug("SOAP Request Created");
        return soapMessage;
    }

    public void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {
        log.debug("Creating SOAP envelope...");
        SOAPPart soapPart = soapMessage.getSOAPPart();

        try {
            // SOAP Envelope
            SOAPEnvelope envelope = soapPart.getEnvelope();
            envelope.addNamespaceDeclaration(bodyNamespace, bodyNamespaceURI);
            envelope.addNamespaceDeclaration(elementNamespace, elementNamespaceURI);

            // SOAP Body
            SOAPBody soapBody = envelope.getBody();
            SOAPElement soapBodyElem = soapBody.addChildElement(action, bodyNamespace);
            SOAPElement dados = soapBodyElem.addChildElement("dados", bodyNamespace);
            for(String key : map.keySet()) {
                if(map.get(key) != null)
                    dados.addChildElement(key, elementNamespace).addTextNode(map.get(key).toString());
            }

//            QName soapAction = new QName(myNamespaceURIACT, action, myNamespaceACT);
//            SOAPElement soapBodyAction = soapBody.addChildElement(soapAction);
//
//            QName soapDados = new QName(myNamespaceURIACT, "dados", myNamespaceACT);
//            SOAPElement soapBodyDados = soapBodyAction.addChildElement(soapDados);
//
//            for(String key : map.keySet()) {
//                if(map.get(key) != null)
//                    soapBodyAction.addChildElement(key, myNamespaceURIXSD).addTextNode(map.get(key).toString());
//            }

            // action
//            QName childName = new QName(myNamespaceURIACT,action,"act");
//            SOAPBodyElement soapBodyElem = soapBody.addBodyElement(childName);

            // dados
//            QName childName2 = new QName(myNamespaceURIACT,"dados","act");
//            SOAPBodyElement soapBodyElem2 = soapBody.addBodyElement(childName2);

//            soapBodyElem.addChildElement(soapBodyElem2);

//            QName temp = new QName(myNamespaceURIXSD,"","xsd");
//            SOAPBodyElement tempBody = soapBody.addBodyElement(temp);
//            for(String key : map.keySet()) {
//                if(map.get(key) != null)
//                    tempBody.addChildElement(key, myNamespaceURIXSD).addTextNode(map.get(key).toString());
//
//                    soapBodyElem2.addChildElement(tempBody);
//            }

        } catch (Exception ex) {
            log.error("Exception while creting SOAP envelope: \n" + ex.getMessage());
        }
        log.debug("SOAP Envelope Created.");
    }
}
