package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.informacaoLogin.InformacaoLogin;
import pt.baieuropa.baieserv.models.informacaoLogin.SecObjAddRq;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.transactionsSender.InfoLoginSender;

import javax.ws.rs.Consumes;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/login")
public class LoginController {

    protected static SasConnections sasConnections;
    public static InfoLoginSender infoLoginSender;


    /**
     * Informação Login
     *
     * @param content
     * @return
     */
    @RequestMapping(value = "/info")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean InfLogin(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n---------------------------------Login Information---------------------------------n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            InformacaoLogin informacaoLogin = new InformacaoLogin();
            SecObjAddRq secObjAddRq = new SecObjAddRq();
            secObjAddRq.setRqUID("");//BAIE 1 id da transação
            pt.baieuropa.baieserv.models.informacaoLogin.SecObjInfo secObjInfo = new pt.baieuropa.baieserv.models.informacaoLogin.SecObjInfo();
            secObjInfo.setPrcDt("");//BAIE 2 data e  hora transação
            secObjInfo.setStrongAuth("");//BAIE autenticação forte
            secObjInfo.setSecObjPurpose("");//BAIE 10 action type
            secObjInfo.setSecObjIP("");//BAIE 5 ip
            secObjInfo.setSecObjGeoLoc("");//BAIE 6 geolocalização
            secObjInfo.setDevice("");//BAIE 7 dispositivo
            secObjInfo.setBrowser("");//BAIE 8 browser
            secObjInfo.setOPSystem("");//BAIE 9 sistema operativo

            pt.baieuropa.baieserv.models.informacaoLogin.PartyInfo partyInfo = new pt.baieuropa.baieserv.models.informacaoLogin.PartyInfo();
            partyInfo.setLoginName("");//BAIE 3 id internet banking
            partyInfo.setPartyId("");//BAIE 4 id cliente

            secObjAddRq.setSecObjInfo(secObjInfo);
            secObjAddRq.setPartyInfo(partyInfo);
            informacaoLogin.setSecObjAddRq(secObjAddRq);


            finalObject = informacaoLogin;


            infoLoginSender = new InfoLoginSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = infoLoginSender.processInfoLogin(secObjAddRq);
                sasConnections.closeStreams();
            }


        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;


    }
}
