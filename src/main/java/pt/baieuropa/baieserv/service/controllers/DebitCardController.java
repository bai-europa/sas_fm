package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.MongoInternal;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.DebitCardsJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.DebitCardSender;

import javax.ws.rs.Consumes;
import java.util.HashMap;
import java.io.IOException;
import org.json.JSONException;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/debitCard")
public class DebitCardController {

    protected static SasConnections sasConnections;

    public static DebitCardSender debitCardSender;


    /**
     * Transferencia interna com Cartão -> transaction 1010, 1020
     *
     * @param
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createDebitCard(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.info("\n\n---------------------------------Debit Card-------------------------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();

            MongoWritter mongoWritter = new MongoWritter();
            DebitCardsJsonBuilder debitCardsJsonBuilder = new DebitCardsJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = debitCardsJsonBuilder.chooseDebitCards(content);

            finalObject = pmtAddRq;

            debitCardSender = new DebitCardSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = debitCardSender.processDebitCardTransaction(pmtAddRq);
                sasConnections.closeStreams();
            }

            //BAIE cria nova estrutura com os dados recebidos
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Debit Card Transaction on Mongo\n");
            mongoWritter.insertNewMessage("DebitCard", content, mongoInternal, responseHashMap);
            log.debug("Debit Card transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n", e);
            return false;
        }
        return true;
    }
}
