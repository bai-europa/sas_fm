package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.MongoInternal;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.SepaDDJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.SepaDDSender;

import javax.ws.rs.Consumes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/sepaDD")
public class SepaDDController {


    protected static SasConnections sasConnections;

    public static SepaDDSender sepaDDSender;

    /**
     * Movimentos Parqueados e Realizados - "Débitos Diretos"
     */
    @RequestMapping(value = "/movimentosParqueados")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean movimentosParqueados(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        List<PmtAddRq> pmtAddRqList = new ArrayList<>();
        try {
            log.debug("\n\n---------------Parked and Performed Movements - Direct Debit-----------------\n");
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            SepaDDJsonBuilder sepaDDJsonBuilder = new SepaDDJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRqList = sepaDDJsonBuilder.chooseParkedMovementsType(content);

            sepaDDSender = new SepaDDSender();
            sasConnections = new SasConnections();
            boolean connectionSasSucess = true;
            connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");

                //TODO Correr lista pmtAddRqList e executar pedidos
                for (PmtAddRq sepa : pmtAddRqList) {
                    responseHashMap = sepaDDSender.processSepaDD(sepa);
                }
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Internal Transaction on Mongo\n");
            mongoWritter.insertNewMessage("SepaDD", content, mongoInternal, responseHashMap);
            log.debug("Internal transaction Inserted Successfully On Mongo\n");
        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }

    /**
     * Movimentos Parqueados e Realizados - "Débitos Diretos"
     */
    @RequestMapping(value = "/rTransactions")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean rTransactions(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.debug("\n\n---------------R-Transactions - Direct Debit-----------------\n");
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            SepaDDJsonBuilder sepaDDJsonBuilder = new SepaDDJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = sepaDDJsonBuilder.chooseRTransactionType(content);

            sepaDDSender = new SepaDDSender();
            sasConnections = new SasConnections();
            boolean connectionSasSucess = true;
            connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = sepaDDSender.processSepaDD(pmtAddRq);
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Internal Transaction on Mongo\n");
            mongoWritter.insertNewMessage("SepaDD", content, mongoInternal, responseHashMap);
            log.debug("Internal transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }
}
