package pt.baieuropa.baieserv.service.controllers;


import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.MongoInternal;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.GeneralJsonBuilder;
import pt.baieuropa.baieserv.service.jsonBuilder.InternalJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.InternalSender;
import java.io.IOException;
import org.json.JSONException;
import java.util.HashMap;
import javax.ws.rs.Consumes;


@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/internal")
public class InternalController {

    protected static SasConnections sasConnections;
    public static InternalSender internalSender;


    /**
     * Transferencia interna digital -> transaction 1010, 1020
     *
     * @param
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Digital")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createDigitalInternal(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n---------------------------------Transferência interna--------------------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();

            MongoWritter mongoWritter = new MongoWritter();
            InternalJsonBuilder internalJsonBuilder = new InternalJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = internalJsonBuilder.chooseInternalType(content, transactionDirection);

            internalSender = new InternalSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = true;
            connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = internalSender.processInternalTransaction(pmtAddRq, "D");
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Internal Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Digital Internal", content, mongoInternal, responseHashMap);
            log.debug("Internal transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");

            return false;
        }
        return true;
    }

    /**
     * Transferencia interna Manual -> transaction 1010, 1020
     *
     * @param
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Manual")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createManualInternal(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.info("\n\n---------------------------------Transferência interna----------------------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            GeneralJsonBuilder generalJsonBuilder = new GeneralJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            InternalJsonBuilder internalJsonBuilder = new InternalJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = internalJsonBuilder.chooseInternalType(content, transactionDirection);

            finalObject = pmtAddRq;

            internalSender = new InternalSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = internalSender.processInternalTransaction(pmtAddRq, "M");
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Internal Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Manual Internal", content, mongoInternal, responseHashMap);
            log.debug("Internal transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return true;
        }
        return true;
    }

    /**
     * Transferencia interna com Cartão -> transaction 1010, 1020
     *
     * @param
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Card")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createCardInternal(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n---------------------------------Internal Transaction----------------------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            MongoWritter mongoWritter = new MongoWritter();
            InternalJsonBuilder internalJsonBuilder = new InternalJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = internalJsonBuilder.chooseInternalType(content, transactionDirection);

            finalObject = pmtAddRq;

            internalSender = new InternalSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = internalSender.processInternalTransaction(pmtAddRq, "C");
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert Internal Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Card Internal", content, mongoInternal, responseHashMap);
            log.debug("Internal transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }


}
