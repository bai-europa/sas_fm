package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.CustPayeeModRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.BenefOrStandingOrderJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.ChangeBenefOrStandingOrdersSender;

import javax.ws.rs.Consumes;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/benefOrStandingOrders")
public class BenefOrStandingOrdersController {


    protected static SasConnections sasConnections;

    public static ChangeBenefOrStandingOrdersSender changeBenefOrStandingOrdersSender;


    /**
     * Alteração de beneficiários de confiança
     *
     * @param content
     * @return
     */
    @RequestMapping(value = "/changeBeneficiarios")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean changeBeneficiarios(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n----------------Change Beneficiary or standing orders-------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            CustPayeeModRq custPayeeModRq = new CustPayeeModRq();
            BenefOrStandingOrderJsonBuilder benefOrStandingOrderJsonBuilder = new BenefOrStandingOrderJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            custPayeeModRq = benefOrStandingOrderJsonBuilder.createBenefOrStandingOrdersStructure(content);

            finalObject = custPayeeModRq;

            changeBenefOrStandingOrdersSender = new ChangeBenefOrStandingOrdersSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                changeBenefOrStandingOrdersSender.processChangeBenefOrStandingOrders(custPayeeModRq, "B");
                sasConnections.closeStreams();
            }
        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }


    /**
     * Alteração de standing olders
     *
     * @param content
     * @return
     */
    @RequestMapping(value = "/changeStandingOrders")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean changeStandingOrders(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.debug("\n\n-----------------Alteração de beneficiários de confirança ou standing orders------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            CustPayeeModRq custPayeeModRq = new CustPayeeModRq();
            BenefOrStandingOrderJsonBuilder benefOrStandingOrderJsonBuilder = new BenefOrStandingOrderJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            custPayeeModRq = benefOrStandingOrderJsonBuilder.createBenefOrStandingOrdersStructure(content);

            finalObject = custPayeeModRq;


            changeBenefOrStandingOrdersSender = new ChangeBenefOrStandingOrdersSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                changeBenefOrStandingOrdersSender.processChangeBenefOrStandingOrders(custPayeeModRq, "S");
                sasConnections.closeStreams();
            }
        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }

}
