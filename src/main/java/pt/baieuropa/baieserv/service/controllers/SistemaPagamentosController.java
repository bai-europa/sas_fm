package pt.baieuropa.baieserv.service.controllers;


import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.ServiceApp;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.ResponseFraudEvaluation;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.transactionsSender.OPCSender;
import pt.baieuropa.baieserv.service.transactionsSender.OPESender;
import pt.baieuropa.baieserv.service.transactionsSender.OPRSender;

import java.util.HashMap;
import javax.ws.rs.Consumes;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/SP")
public class SistemaPagamentosController {

    protected static SasConnections sasConnections;

    public static OPRSender oprSender;
    public static OPCSender opcSender;
    public static OPESender opeSender;

    //BAIE----------------------------------------Sistema de Pagamentos-------------------------------------------------

    /**
     * Operações enviadas
     *
     * @param content
     * @return
     * @throws Exception,SasConnectionException
     */
    @RequestMapping(value = "/ope")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createOPE(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        //BAIE Retirar codigo de operação do json recebido
        JSONObject jsonObject = new JSONObject(content);
        String codOperacao = jsonObject.getString("aqo_acct_num");
        //BAIE Colocar codigo de operação na estrutura de resposta
        ResponseFraudEvaluation responseFraudEvaluation = new ResponseFraudEvaluation();
        responseFraudEvaluation.setOperation_nr(codOperacao);
        try {
            log.info("\n\n---------------------------------OPE-----------------------------\n");

            MongoWritter mongoWritter = new MongoWritter();
            opeSender = new OPESender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = opeSender.processOPE(content);
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            log.debug("Insert OPE on Mongo\n");
            mongoWritter.insertNewMessage("OPE", content, null, responseHashMap);
            log.debug("OPE Inserted Successfully On Mongo\n");

            //BAIE vai buscar o código de operação ao json recebido
            //BAIE e o return code da SAS para mapear a estrutura de resposta
            String status = responseHashMap.get("rrr_action_code");

            log.debug("Operation Number : " + codOperacao + "\n");
            //BAIE Resposta da SAS
            //todo corrigir lógica consoante o significado da resposta
            if (status.equals("0") || status.equals("1")) { //BAIE se for 0 ou 1 é aprovado
                responseFraudEvaluation.setStatus("Aprovado");
                log.debug("OPE Aprovada\n");
            } else if (status.equals("4")) {//BAIE se for 4 é rejeitado
                responseFraudEvaluation.setStatus("Rejeitado");
                log.debug("OPE Rejeitada\n");
            }
//            responseFraudEvaluation.setStatus("Aprovado");
//            log.debug("OPE Aprovada\n");

            finalObject = responseFraudEvaluation;
            //BAIE publish finalobject
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");

        } catch (SasConnectionException ex) {
            //BAIE erro de conexão à SAS
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            //BAIE Se ocorrer erro durante o processamento da operação
            log.error("Process request error\n");
            responseFraudEvaluation.setStatus("Erro a processar a operação" + codOperacao + "\n");
            log.debug("OPE com erro\n");
            //BAIE Converter objecto para json
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");
            //BAIE publicar resposta no NATS
            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");
            return true;
        }
        return true;
    }


    /**
     * OPC
     *
     * @param content
     * @return
     * @throws Exception,SasConnectionException
     */
    @RequestMapping(value = "/opc")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createOPC(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        //BAIE Retirar codigo de operação do json recebido
        JSONObject jsonObject = new JSONObject(content);
        String codOperacao = jsonObject.getString("rua_80byte_nls_string_001");
        //BAIE Colocar codigo de operação na estrutura de resposta
        ResponseFraudEvaluation responseFraudEvaluation = new ResponseFraudEvaluation();
        responseFraudEvaluation.setOperation_nr(codOperacao);
        try {
            log.debug("\n\n---------------------------------OPC-----------------------------\n");

            MongoWritter mongoWritter = new MongoWritter();
            opcSender = new OPCSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = opcSender.processOPC(content);
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            log.debug("Insert OPC on Mongo\n");
            mongoWritter.insertNewMessage("OPC", content, content, responseHashMap);
            log.debug("OPC Inserted Successfully On Mongo\n");

            //BAIE vai buscar o código de operação ao json recebido
            // e o return code da SAS para mapear a estrutura de resposta
            String status = responseHashMap.get("rrr_action_code");

            log.debug("Operation Number : " + codOperacao + "\n");
            //BAIE Resposta da SAS
            //todo corrigir lógica consoante o significado da resposta
            if (status.equals("0") || status.equals("1")) { //BAIE se for 0 ou 1 é aprovado
                responseFraudEvaluation.setStatus("Aprovado");
                log.debug("OPC Aprovada\n");
            } else if (status.equals("4")) {//BAIE se for 4 é rejeitado
                responseFraudEvaluation.setStatus("Rejeitado");
                log.debug("OPC Rejeitada\n");
            }
//            responseFraudEvaluation.setStatus("Aprovado");
//            log.debug("OPC Aprovada\n");

            finalObject = responseFraudEvaluation;
            //BAIE publish finalobject
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");

        } catch (SasConnectionException ex) {
            //BAIE erro de conexão à SAS
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            //BAIE Se ocorrer erro durante o processamento da operação
            log.error("Process request error\n");
            responseFraudEvaluation.setStatus("Erro a processar a operação" + codOperacao + "\n");
            log.debug("OPC "+codOperacao +" com erro\n");
            //BAIE Converter objecto para json
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");
            //BAIE publicar resposta no NATS
            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");
            return true;
        }
        return true;
    }


    /**
     * Operações recebidas
     *
     * @param content
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/opr")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createOPR(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        //BAIE Retirar codigo de operação do json recebido
        JSONObject jsonObject = new JSONObject(content);
        String codOperacao = jsonObject.getString("rua_80byte_nls_string_001");
        //BAIE Colocar codigo de operação na estrutura de resposta
        ResponseFraudEvaluation responseFraudEvaluation = new ResponseFraudEvaluation();
        responseFraudEvaluation.setOperation_nr(codOperacao);
        try {
            log.debug("\n\n---------------------------------OPR-----------------------------\n");

            MongoWritter mongoWritter = new MongoWritter();
            oprSender = new OPRSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = oprSender.processOPR(content);
                sasConnections.closeStreams();
            }

            //BAIE Inserir no MongoDB
            log.debug("Insert OPR on Mongo\n");
            mongoWritter.insertNewMessage("OPR", content, null, responseHashMap);
            log.debug("OPR Inserted Successfully On Mongo\n");

            //BAIE vai buscar o código de operação ao json recebido
            // e o return code da SAS para mapear a estrutura de resposta
            String status = responseHashMap.get("rrr_action_code");

            log.debug("Operation Number : " + codOperacao + "\n");
            //BAIE Resposta da SAS
            //todo corrigir lógica consoante o significado da resposta
            if (status.equals("0") || status.equals("1")) { //BAIE se for 0 ou 4 é aprovado
                responseFraudEvaluation.setStatus("Aprovado");
                log.debug("OPR Aprovada\n");
            } else if (status.equals("4")){//BAIE se for 4 é rejeitado
                responseFraudEvaluation.setStatus("Rejeitado");
                log.debug("OPR Rejeitada\n");
            }
//            responseFraudEvaluation.setStatus("Aprovado");
//            log.debug("OPR Aprovada\n");

            finalObject = responseFraudEvaluation;
            //BAIE publish finalobject
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");

            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");


        } catch (SasConnectionException ex) {
            //BAIE erro de conexão à SAS
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            //BAIE Se ocorrer erro durante o processamento da operação
            log.error("Process request error\n");
            responseFraudEvaluation.setStatus("Erro a processar a operação" + codOperacao + "\n");
            log.debug("OPR com erro\n");
            //BAIE Converter objecto para json
            log.debug("Converting Object to JSON...");
            Gson gson = new Gson();
            String json = gson.toJson(finalObject);
            log.debug("Object Converted.");
            //BAIE publicar resposta no NATS
            log.debug("Publishing message on NATS...");
            ServiceApp.getProc().publish("FraudEvaluation:Created", json);
            log.debug("Message Published on FraudEvaluation:Created.\n");
            return true;
        }
        return true;
    }
}
