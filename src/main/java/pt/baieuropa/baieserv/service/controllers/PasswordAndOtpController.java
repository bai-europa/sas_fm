package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.SecObjModRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.PasswordAndOtpJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.ChangePasswordOrContactOTPSender;

import javax.ws.rs.Consumes;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/passwordAndOtpController")
public class PasswordAndOtpController {

    protected static SasConnections sasConnections;

    public static ChangePasswordOrContactOTPSender changePasswordOrContactOTPSender;

    /**
     * Alteracao de Password
     *
     * @param content
     * @return
     */
    @RequestMapping(value = "/changePassword")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean changePassword(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.debug("\n\n---------------------------------Change Password or OTP Contact---------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            SecObjModRq secObjModRq = new SecObjModRq();
            PasswordAndOtpJsonBuilder passwordAndOtpJsonBuilder = new PasswordAndOtpJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            secObjModRq = passwordAndOtpJsonBuilder.createPasswordOrOtpStructure(content);
            finalObject = secObjModRq;


            changePasswordOrContactOTPSender = new ChangePasswordOrContactOTPSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                changePasswordOrContactOTPSender.processChangePasswordOrContactOTP(secObjModRq, "P");
                sasConnections.closeStreams();
            }
        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }


    /**
     * Alteracao de Contacto OTP
     *
     * @param content
     * @return
     */
    @RequestMapping(value = "/changeContactOTP")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean changeContactOTP(@RequestBody String content) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.debug("\n\n---------------------------------Alteração Contacto OTP-------------------------------\n");
            log.debug("Content: " + content);
            //BAIE criação de estrutura com pojos
            SecObjModRq secObjModRq = new SecObjModRq();
            PasswordAndOtpJsonBuilder passwordAndOtpJsonBuilder = new PasswordAndOtpJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            secObjModRq = passwordAndOtpJsonBuilder.createPasswordOrOtpStructure(content);
            finalObject = secObjModRq;


            changePasswordOrContactOTPSender = new ChangePasswordOrContactOTPSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                changePasswordOrContactOTPSender.processChangePasswordOrContactOTP(secObjModRq, "C");
                sasConnections.closeStreams();
            }
        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }

}
