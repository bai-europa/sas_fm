package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.MongoAccountDetails;
import pt.baieuropa.baieserv.models.alteracaoDetalhesConta.AlteracaoDetalheConta;
import pt.baieuropa.baieserv.models.alteracaoDetalhesConta.PartyModRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.AccountDetailsJsonBuilder;
import pt.baieuropa.baieserv.service.transactionsSender.ChangeAccountDetailsSender;
import javax.ws.rs.Consumes;
import java.util.HashMap;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/changeAccountDetails")
public class AccountDetailsController {

    protected static SasConnections sasConnections;
    public static ChangeAccountDetailsSender changeAccountDetailsSender;

    /**
     * Alteração de detalhes de conta (nome, morada, contacto, limite)
     *
     * @return
     */
    @RequestMapping(value = "")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean changeAccountDetails(@RequestBody String content, String clientType) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n-----------------Change Account Details (Name, address, contact, limit)--------\n");
            //BAIE criação de estrutura com pojos
            AlteracaoDetalheConta alteracaoDetalheConta = new AlteracaoDetalheConta();
            PartyModRq partyModRq = new PartyModRq();

            AccountDetailsJsonBuilder accountDetailsJsonBuilder = new AccountDetailsJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            partyModRq = accountDetailsJsonBuilder.createAccountDetailsStructure(content, clientType);

            alteracaoDetalheConta.setPartyModRq(partyModRq);
            finalObject = alteracaoDetalheConta;

            changeAccountDetailsSender = new ChangeAccountDetailsSender();
            sasConnections = new SasConnections();

            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");

            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                //BAIE Envio de dados para a SAS
                responseHashMap = changeAccountDetailsSender.processChangeAccountDetails(partyModRq);
                sasConnections.closeStreams();
            }

            MongoAccountDetails mongoAccountDetails = new MongoAccountDetails(partyModRq);
            log.debug("Insert Account Details Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Account Details", content, mongoAccountDetails, responseHashMap);
            log.debug("Account Details transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error.\n");
            return true;
        }
        return true;
    }
}
