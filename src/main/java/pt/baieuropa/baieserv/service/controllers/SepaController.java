package pt.baieuropa.baieserv.service.controllers;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.models.MongoInternal;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;
import pt.baieuropa.baieserv.processor.database.MongoWritter;
import pt.baieuropa.baieserv.service.SasConnections;
import pt.baieuropa.baieserv.service.jsonBuilder.GeneralJsonBuilder;
import pt.baieuropa.baieserv.service.jsonBuilder.SepaJsonBuilder;
import java.io.IOException;
import org.json.JSONException;
import pt.baieuropa.baieserv.service.transactionsSender.SepaSender;

import java.util.HashMap;
import javax.ws.rs.Consumes;

@Slf4j
@RestController
@RequestMapping("/process-fraud-workflow/sepa")
public class SepaController {


    protected static SasConnections sasConnections;

    public static SepaSender sepaSender;


    /**
     * SEPA --> Transferencia sepa(CRTSP) com Digital
     *
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Digital")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createDigitalSepa(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.info("\n\n---------------------------------Digital SEPA Transaction-----------------------------\n");
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            GeneralJsonBuilder generalJsonBuilder = new GeneralJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            SepaJsonBuilder sepaJsonBuilder =  new SepaJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = sepaJsonBuilder.chooseSepaType(content, transactionDirection);//TODO----> verificar as excepções para não continuar se tiver erros

            finalObject = pmtAddRq;

            sepaSender = new SepaSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = sepaSender.processSepaTransaction(pmtAddRq, "D");
                sasConnections.closeStreams();
            }

            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert SEPA Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Digital SEPA", content, mongoInternal, responseHashMap);
            log.debug("SEPA transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n");
            return false;
        }
        return true;
    }


    /**
     * SEPA --> Transferencia sepa(CRTSP) com manual
     *
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Manual")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createManualSepa(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();

        try {
            log.info("\n\n---------------------------------Digital SEPA Transaction-----------------------------\n");
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            GeneralJsonBuilder generalJsonBuilder = new GeneralJsonBuilder();
            MongoWritter mongoWritter = new MongoWritter();
            SepaJsonBuilder sepaJsonBuilder =  new SepaJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = sepaJsonBuilder.chooseSepaType(content, transactionDirection);//BAIE Esta função vai invocar o parties e chamar a função para criar estruturas

            finalObject = pmtAddRq;

            sepaSender = new SepaSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = sepaSender.processSepaTransaction(pmtAddRq, "M");
                sasConnections.closeStreams();
            }

            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert SEPA Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Manual SEPA", content, mongoInternal, responseHashMap);
            log.debug("SEPA transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n", e);

            return false;
        }
        return true;
    }


    /**
     * SEPA --> Transferencia sepa(CRTSP) com Cartão
     *
     * @param content
     * @return
     * @throws IOException
     * @throws JSONException
     */
    @RequestMapping(value = "/Card")
    @Consumes(MediaType.APPLICATION_JSON_VALUE)
    public boolean createCardSepa(@RequestBody String content, @RequestParam String transactionDirection) throws Exception, SasConnectionException {
        Object finalObject = new Object();
        HashMap<String, String> responseHashMap = new HashMap<>();
        try {
            log.info("\n\n---------------------------------Digital SEPA Transaction-----------------------------\n");
            //BAIE criação de estrutura com pojos
            PmtAddRq pmtAddRq = new PmtAddRq();
            MongoWritter mongoWritter = new MongoWritter();
            SepaJsonBuilder sepaJsonBuilder =  new SepaJsonBuilder();
            //BAIE lê o json que contêm os campos da Banka preenchidos e preenche os devidos pojos
            pmtAddRq = sepaJsonBuilder.chooseSepaType(content, transactionDirection);
            finalObject = pmtAddRq;

            sepaSender = new SepaSender();
            sasConnections = new SasConnections();
            log.debug("Init Sas connections\n");
            boolean connectionSasSucess = sasConnections.connectToSas();
            log.debug("End Sas connections\n");
            if (connectionSasSucess == true) {
                log.debug("Init Sas configurations\n");
                responseHashMap = sepaSender.processSepaTransaction(pmtAddRq, "C");
                sasConnections.closeStreams();
            }

            MongoInternal mongoInternal = new MongoInternal(pmtAddRq);
            log.debug("Insert SEPA Transaction on Mongo\n");
            mongoWritter.insertNewMessage("Card SEPA", content, mongoInternal, responseHashMap);
            log.debug("SEPA transaction Inserted Successfully On Mongo\n");

        } catch (SasConnectionException ex) {
            log.error(ex.getMessage());
            return false;
        } catch (Exception e) {
            log.error("Process request error\n", e);
            return false;
        }
        return true;
    }


}
