package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.ResponseStructure;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class OPESender {

    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(OPESender.class);

    public OPESender() {
    }

    /**
     * Execute OPE
     * @param content
     * @return
     */
    public static HashMap<String, String> processOPE(String content) {
        JSONObject transactionJsonObject = new JSONObject(content);
        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        //BAIE SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction();

        //BAIE Segment known to exist
        //BAIE create fields
        logger.info("OPE Mapping\n");
//        Field smh_seg_id_version = api.getField("smh_seg_id_version");//Identificador do segmento
//        Field smh_msg_version = api.getField("smh_msg_version");    //Versão da API associada à solução
        Field smh_tran_type = api.getField("smh_tran_type");    //Tipo de transacção
        Field smh_cust_type = api.getField("smh_cust_type");    //Tipo de cliente
        Field smh_acct_type = api.getField("smh_acct_type");    //Tipo de operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");    //Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");    //Tipo de canal
        Field smh_activity_type = api.getField("smh_activity_type");    //Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");    //Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");    //Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");    //Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");    //Client Defined Transaction Type
        Field smh_priority = api.getField("smh_priority");    //Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");    //Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");    //Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");    //Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");
        Field smh_dest = api.getField("smh_dest");
        Field smh_multi_org_name = api.getField("smh_multi_org_name");    //Nome da multi-organização
//        Field rrr_seg_id_version = api.getField("rrr_seg_id_version");	//Identificador do segmento
//        Field rqo_seg_id_version = api.getField("rqo_seg_id_version");	//Identificador do segmento
        Field rqo_tran_date = api.getField("rqo_tran_date");    //Data da transacção
        Field rqo_tran_time = api.getField("rqo_tran_time");    //Hora da transação
        Field rqo_tran_date_alt = api.getField("rqo_tran_date_alt");    //Date of transaction arrival in local time
        Field rqo_tran_time_alt = api.getField("rqo_tran_time_alt");    //Time of transaction arrival in local time
//        Field xqo_seg_id_version = api.getField("xqo_seg_id_version");	//Identificador do segmento
        Field xqo_cust_num = api.getField("xqo_cust_num");    //Id Cliente BANKA (ordenante)
        Field xqo_cust_name = api.getField("xqo_cust_name");    //Nome do cliente (ordenante)
        Field xqo_cust_cntry_code = api.getField("xqo_cust_cntry_code");    //País do cliente (ordenante)
        Field xqo_cust_birth_dt = api.getField("xqo_cust_birth_dt");    //Data de nascimento do cliente (ordenante)
//        Field unm_seg_id_version = api.getField("unm_seg_id_version");	//Identificador do segmento
        Field unm_auth_mtd1 = api.getField("unm_auth_mtd1");    //Indicador de autenticação forte
//        Field hqo_seg_id_version = api.getField("hqo_seg_id_version");	//Identificador do segmento
        Field hqo_ob_userid = api.getField("hqo_ob_userid");    //Utilizador Internet Banking
//        Field rua_seg_id_version = api.getField("rua_seg_id_version");	//Identificador do segmento
        Field rua_numeric_001 = api.getField("rua_numeric_001");    //Data valor da transacção
        Field rua_numeric_003 = api.getField("rua_numeric_003");    //Data de abertura da operação do cliente (ordenante)
        Field rua_numeric_005 = api.getField("rua_numeric_005");    //Data de maturidade da operação do cliente (ordenante)
        Field rua_8byte_string_001 = api.getField("rua_8byte_string_001");    //Canal da transacção
        Field rua_8byte_string_002 = api.getField("rua_8byte_string_002");    //Tipo de transferência
        Field rua_20byte_string_005 = api.getField("rua_20byte_string_005");    //Id de Transação
        Field rua_30byte_string_002 = api.getField("rua_30byte_string_002");    //IBAN da conta do cliente (ordenante)
//        Field tbt_seg_id_version = api.getField("tbt_seg_id_version");	//Identificador do segmento
        Field tbt_tran_amt = api.getField("tbt_tran_amt");    //Montante da transacção em EUR
        Field tbt_tran_curr_code = api.getField("tbt_tran_curr_code");    //Moeda da transacção
//        Field tpp_seg_id_version = api.getField("tpp_seg_id_version");	//Identificador do segmento
        Field tpp_payee_payer_ind = api.getField("tpp_payee_payer_ind");    //Tipo de beneficiário
        Field tpp_num = api.getField("tpp_num"); //IBAN do beneficiário
        Field tpp_name = api.getField("tpp_name");    //Nome do beneficiário
        Field tpp_cntry_code = api.getField("tpp_cntry_code");    //País do beneficiário
        Field tpp_city = api.getField("tpp_city");    //Morada do beneficiário
        Field tpp_bank_num = api.getField("tpp_bank_num");    //BIC do banco do beneficiário
        Field tpp_bank_name = api.getField("tpp_bank_name");    //Nome do banco do beneficiário
        Field tpp_acct_num = api.getField("tpp_acct_num");    //Id da conta do beneficiário
        Field tpp_description = api.getField("tpp_description");    //Descritivo da transacção
//        Field dua_seg_id_version = api.getField("dua_seg_id_version"); 	//Identificador do segmento
//        Field aqo_seg_id_version = api.getField("aqo_seg_id_version");	//Identificador do segmento
        Field aqo_acct_num = api.getField("aqo_acct_num");    //Id da operação do cliente (ordenante)

        //BAIE Message Header Segment -->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "CS");
        smh_authenticate_mtd.encodeText(txn, "NC");
        smh_channel_type.encodeText(txn, "B");
        smh_activity_type.encodeText(txn, "BF");
        smh_activity_detail1.encodeText(txn, "NAP");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_client_tran_type.encodeText(txn, "MManual");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_source.encodeText(txn, "Manual");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "GLOBAL");

        //BAIE Common Segment -->Variáveis de resposta da solução
        txn.addSegmentIfMissing(SegmentType.RRR);

        //BAIE Date/Time Segment --> Informação sobre tempo e data de transacção
        txn.addSegmentIfMissing(SegmentType.RQO);
//        long t = System.currentTimeMillis();
        LocalDateTime d = LocalDateTime.parse(transactionJsonObject.getString("rqo_tran_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_date_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()

        //BAIE Detalhe sobre cliente
        txn.addSegmentIfMissing(SegmentType.XQO);
        xqo_cust_num.encodeText(txn, transactionJsonObject.getString("xqo_cust_num"));//Id Cliente BANKA (ordenante)
        xqo_cust_name.encodeText(txn, transactionJsonObject.getString("xqo_cust_name")); //Nome cliente ordenante
        xqo_cust_cntry_code.encodeText(txn, transactionJsonObject.getString("xqo_cust_cntry_code"));//País do cliente ordenante
        xqo_cust_birth_dt.encodeText(txn, transactionJsonObject.getString("xqo_cust_birth_dt")); //Data de nascimento do cliente ordenante

        //BAIE Informação de autenticação não relacionada com cartões
        txn.addSegment(SegmentType.UNM);
        unm_auth_mtd1.encodeText(txn, transactionJsonObject.getString("unm_auth_mtd1"));//Indicador de autenticação forte

        //BAIE Channel Segment
        txn.addSegment(SegmentType.HQO);
        hqo_ob_userid.encodeText(txn, transactionJsonObject.getString("hqo_ob_userid"));// Utilizador Internet Banking

        //BAIE Common Segment Variáveis genéricas
        txn.addSegment(SegmentType.RUA);
        String duedt = transactionJsonObject.getString("rua_numeric_001");// payment.getPmtInfo().getPmtInstruction().getDueDt();
        Integer datas = Integer.valueOf(duedt.substring(0, 4) + duedt.substring(5, 7) + duedt.substring(8, 10));
        rua_numeric_001.encodeDouble(txn, datas);// Data valor da transação

        String opendt = transactionJsonObject.getString("rua_numeric_003");//payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctInfo().getOpenDt();
        String dataOpendt = (opendt.substring(0, 4) + opendt.substring(5, 7) + opendt.substring(8, 10));
        rua_numeric_003.encodeText(txn, dataOpendt); //Data de abertura da operação do cliente (ordenante)

        String maturityDt = transactionJsonObject.getString("rua_numeric_005");//payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctInfo().getMaturityDt();
        String dataMaturityDt = (maturityDt.substring(0, 4) + maturityDt.substring(5, 7) + maturityDt.substring(8, 10));
        rua_numeric_005.encodeText(txn, dataMaturityDt);//Data de maturidade da operação do cliente (ordenante)
        //BAIE NOVAS
        rua_8byte_string_001.encodeText(txn, transactionJsonObject.getString("rua_8byte_string_001"));//Canal de transação
        rua_8byte_string_002.encodeText(txn, transactionJsonObject.getString("rua_8byte_string_002"));//Tipo de transferencia
        rua_20byte_string_005.encodeText(txn, transactionJsonObject.getString("rua_20byte_string_005")); //Id da transação
        rua_30byte_string_002.encodeText(txn, transactionJsonObject.getString("rua_30byte_string_002"));//IBAN da conta do cliente (ordenante)--><PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctKeys><IBAN>

        //BAIE Detalhe de transferência via canal digital
        txn.addSegment(SegmentType.TBT);
        String ammountValue = transactionJsonObject.getString("tbt_tran_amt").replace(",",".");
        Double ammount = Double.parseDouble(ammountValue);//Montante de transação em EUR --> <PmtAddRq><PmtInfo><CurAmt><Amt>

        tbt_tran_amt.encodeDouble(txn, ammount);
        tbt_tran_curr_code.encodeText(txn, transactionJsonObject.getString("tbt_tran_curr_code"));//Moeda da transação --><PmtAddRq><PmtInfo><CurAmt><CurCode>

        //BAIE Informação sobre TPP
        txn.addSegment(SegmentType.TPP);
        tpp_payee_payer_ind.encodeText(txn, transactionJsonObject.getString("tpp_payee_payer_ind"));//Tipo de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><IssuedIdent><IssuedIdentValue>
        String ibanBenef = transactionJsonObject.getString("tpp_num");
        if (ibanBenef.length()<=25) {//BAIE só aceita 25 caracteres
            tpp_num.encodeText(txn, transactionJsonObject.getString("tpp_num"));//IBAN de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><ToAcctKeys><IBAN>
        }else{
            tpp_num.encodeText(txn, "");//IBAN de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><ToAcctKeys><IBAN>
        }
        tpp_name.encodeText(txn, transactionJsonObject.getString("tpp_name"));//Nome de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><Contact><ContactName>
        tpp_cntry_code.encodeText(txn, transactionJsonObject.getString("tpp_cntry_code"));//País de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><IssuedIdent><GovIssuedIdent><CountryCode><CountryCodeValue>
        tpp_city.encodeText(txn, transactionJsonObject.getString("tpp_city"));//Morada de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><Contact><Locator><PostAddr><Addr1>
        tpp_bank_num.encodeText(txn, transactionJsonObject.getString("tpp_bank_num"));//BIC do banco do Beneficiário --> <PmtAddRq><PmtInfo><<PmtCreditDetail><ToAcctKeys><BIC>
        tpp_acct_num.encodeText(txn, transactionJsonObject.getString("tpp_acct_num"));//Id da conta do Beneficiário --><PmtAddRq><PmtInfo><PmtCreditDetail><ToAcctKeys><AcctId>
        Integer tamanho = transactionJsonObject.getString("tpp_description").length();
        if(tamanho>=100){
            String description = transactionJsonObject.getString("tpp_description").substring(0,99);
            tpp_description.encodeText(txn, description);//Descritivo da transação
        }else{
            tpp_description.encodeText(txn, transactionJsonObject.getString("tpp_description"));//Descritivo da transação
        }
        tpp_bank_name.encodeText(txn, transactionJsonObject.getString("tpp_bank_name"));

        //BAIE Activity details DUA
//        txn.addSegment(SegmentType.DUA);

        //BAIE Dimensões de operação
        txn.addSegment(SegmentType.AQO);
        aqo_acct_num.encodeText(txn, transactionJsonObject.getString("aqo_acct_num"));//Id da operação do cliente ordenante --> <PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctKeys><AcctId>

        txn.addSegment(SegmentType.HBP);

        //BAIE Detalhe sobre a decisão anterior
        txn.addSegment(SegmentType.ROB);

        //BAIE Detalhe sobre a chave de decisão
        txn.addSegment(SegmentType.RDK);

        //BAIE Variáveis genéricas
        txn.addSegment(SegmentType.RUR);

        txn.addSegment(SegmentType.AQD);


        //BAIE send
        try {
            logger.debug("Send transaction\n");
            send(lc, txn);
            logger.debug("Transaction sent.\n");
        } catch (Exception e) {
            logger.error("Error sending transaction.\n Transação: "+ rua_20byte_string_005 +"\n");
        }
        //BAIE Receive
        try {
            logger.debug("Receive response message from Sas\n");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error.\n Transação: "+ rua_20byte_string_005 +"\n");
            return null;
        }
        return responseStructuresHashMap;
    }
}
