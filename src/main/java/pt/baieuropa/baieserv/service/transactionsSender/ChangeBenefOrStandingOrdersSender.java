package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.models.ResponseStructure;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.CustPayeeModRq;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class ChangeBenefOrStandingOrdersSender {

    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(OPESender.class);

    public ChangeBenefOrStandingOrdersSender() {
    }


    /**
     * Execute Change beneficiarios ou Standing olders
     *
     * @param request
     * @param clientType variável que diferiencia se é beneficiarios ou standing olders
     * @return
     */
    public static HashMap<String, String> processChangeBenefOrStandingOrders(CustPayeeModRq request, String clientType) {
        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        // BAIE SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction(apie);

        //BAIE Segment known to exist
        //BAIE create fields
        logger.info("Change Beneficiários or Standing orders Mapping\n");
        //BAIE SMH Segment
        Field smh_tran_type = api.getField("smh_tran_type");//Tipo de transação
        Field smh_cust_type = api.getField("smh_cust_type");//Tipo de Cliente
        Field smh_acct_type = api.getField("smh_acct_type");//Tipo de Operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");//Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");//Tipo de Canal
        Field smh_activity_type = api.getField("smh_activity_type");//Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");//Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");//Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");//Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");//Client Defined Transaction Type
        Field smh_priority = api.getField("smh_priority");//Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");//Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");//Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");//Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");//smh_source
        Field smh_dest = api.getField("smh_dest");//smh_dest
        Field smh_multi_org_name = api.getField("smh_multi_org_name");//Nome de multi-organização
        //BAIE HQO Segment
        Field hqo_ob_userid = api.getField("hqo_ob_userid");//Utilizador internet banking
        //BAIE RUA Segment
        Field rua_20byte_string_005 = api.getField("rua_20byte_string_005");//Id da transação
        Field rua_numeric_007 = api.getField("rua_numeric_007"); //Data do movimento fixo
        Field rua_20byte_string_001 = api.getField("rua_20byte_string_001"); //Tipo de movimento fixo
        //BAIE RQO Segment
        Field rqo_tran_date = api.getField("rqo_tran_date"); //UTC Data da transação
        Field rqo_tran_time = api.getField("rqo_tran_time");//UTC Hora da transação
        Field rqo_tran_date_alt = api.getField("rqo_tran_date_alt");// Date of transaction arrival in local time
        Field rqo_tran_time_alt = api.getField("rqo_tran_time_alt");// Time of transaction arrival in local time
        //BAIE UNM Segment
        Field unm_auth_mtd1 = api.getField("unm_auth_mtd1"); //Indicador de autenticação forte
        //BAIE XQO Segment
        Field xqo_cust_num = api.getField("xqo_cust_num");//Id cliente banka
        //BAIE TNG Segment
        Field tng_sub_tran_type = api.getField("tng_sub_tran_type");//Tipo de alteração
        //BAIE DPP Segment
        Field dpp_num = api.getField("dpp_num");//IBAN do beneficiario
        Field dpp_name = api.getField("dpp_name");//Nome do beneficiario
        //BAIE DNC Segment
        Field dnc_new_limit = api.getField("dnc_new_limit");

        //BAIE Message Header Segment-->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "NA");
        smh_authenticate_mtd.encodeText(txn, "NC");
        smh_channel_type.encodeText(txn, "O");
        smh_activity_type.encodeText(txn, "NM");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_source.encodeText(txn, "Digital");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "BAIE");
        if (clientType.equals("B")) {//BAIE Beneficiarios
            smh_activity_detail1.encodeText(txn, "DPP");
            smh_client_tran_type.encodeText(txn, "TBenChng");
        } else if (clientType.equals("S")) {//BAIE standing orders
            smh_activity_detail1.encodeText(txn, "DNC");
            smh_client_tran_type.encodeText(txn, "StOrChng");
        }

        //BAIE Channel Segment
        txn.addSegment(SegmentType.HQO);
        //BAIE Utilizador Internet Banking --> <CustPayeeModRq><PartyInfo><LoginName>
        hqo_ob_userid.encodeText(txn, request.getPartyInfo().getLoginName());

        //BAIE Detalhe técnico
        txn.addSegment(SegmentType.HOB);

        //BAIE Common Segment Variáveis genéricas
        txn.addSegment(SegmentType.RUA);
        rua_20byte_string_005.encodeText(txn, request.getRqUID()); //BAIE Id da transação --><CustPayeeModRq><RqUID>
        if (clientType.equals("S")) {//standing orders
            rua_numeric_007.encodeText(txn, request.getCustPayeeInfo().getLastPmtDt());//BAIE data do movimento fixo <CustPayeeModRq><CustPayeeInfo><LastPmtDt>
            rua_20byte_string_001.encodeText(txn, "");//BAIE tipo de mov. fixo <CustPayeeModRq><CustPayeeInfo><DfltPmtData><Category>
//            rua_20byte_string_001.encodeText(txn, request.getCustPayeeInfo().getDfltPmtData().getCategory());
        }

        // BAIE Date/Time Segment --> Informação sobre tempo e data de transacção
        txn.addSegmentIfMissing(SegmentType.RQO);
        LocalDateTime d = LocalDateTime.parse(request.getPartyInfo().getPrcDt(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<CustPayeeModRq><PartyInfo><PrcDt>))
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(timepart(<CustPayeeModRq><PartyInfo><PrcDt>))
        rqo_tran_date_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<CustPayeeModRq><PartyInfo><PrcDt>))
        rqo_tran_time_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(timepart(<CustPayeeModRq><PartyInfo><PrcDt>))



        // BAIE Informação de autenticação não relacionada com cartões
        txn.addSegment(SegmentType.UNM);
        unm_auth_mtd1.encodeText(txn, "P");//BAIE Indicador de autenticação forte -->  <CustPayeeModRq><PartyInfo><StrongAuth>

        //BAIE Detalhe sobre cliente
        txn.addSegmentIfMissing(SegmentType.XQO);
        xqo_cust_num.encodeText(txn, "5000000");//BAIE Id Cliente BANKA (ordenante) --><CustPayeeModRq><PartyInfo><PartyId>

        //BAIE
        txn.addSegmentIfMissing(SegmentType.TNG);
        tng_sub_tran_type.encodeText(txn, "H");//BAIE Tipo de alteração --> <CustPayeeModRq><PartyInfo><SecObjPurpose>


        if (clientType.equals("B")) {//Beneficiarios
            //BAIE
            txn.addSegmentIfMissing(SegmentType.DPP);
            dpp_num.encodeText(txn, "");//BAIE numero do beneficiario <CustPayeeModRq><CustPayeeInfo><PayeeAcctNum>
            dpp_name.encodeText(txn, "");//BAIE nome do beneficiario <CustPayeeModRq><CustPayeeInfo><Name>
        } else if (clientType.equals("S")) {//Standing olders
            txn.addSegmentIfMissing(SegmentType.DNC);
            dnc_new_limit.encodeText(txn, "");//BAIE Valor do mov. fixo <CustPayeeModRq><CustPayeeInfo><LastPmtCurAmt><Amt>
        }

        // BAIE Common Segment
        txn.addSegmentIfMissing(SegmentType.RRR);

        //BAIE Detalhe sobre a decisão anterior
        txn.addSegment(SegmentType.ROB);

        //BAIE Detalhe sobre a chave de decisão
        txn.addSegment(SegmentType.RDK);

        //BAIE Variáveis genéricas
        txn.addSegment(SegmentType.RUR);


        //BAIE send
        try {
            logger.debug("Send transaction\n");
            send(lc, txn);
            logger.debug("Transaction sent.\n");
        } catch (Exception e) {
            logger.error("Error sending transaction.\n");
        }
        //BAIE Receive
        try {
            logger.debug("Receive response message from Sas");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error\n");
            return null;
        }
        return responseStructuresHashMap;
    }

}
