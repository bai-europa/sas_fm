package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.models.ResponseStructure;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class SepaDDSender {

    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(SepaDDSender.class);

    /**
     * Execute SEPA DD
     *
     * @param payment
     * @return
     * @throws Exception
     */
    public static HashMap<String, String> processSepaDD(PmtAddRq payment) throws Exception {

        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        // BAIE SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction(apie);

        //BAIE Segment known to exist
        // BAIE create fields
        logger.debug("Sepa DD Transaction Mapping\n");
        Field smh_seg_id_version = api.getField("smh_seg_id_version");    //	Identificador do segmento
        Field smh_msg_version = api.getField("smh_msg_version");    //	Versão da API associada à solução
        Field smh_tran_type = api.getField("smh_tran_type");    //	Tipo de transacção
        Field smh_cust_type = api.getField("smh_cust_type");    //	Tipo de cliente
        Field smh_acct_type = api.getField("smh_acct_type");    //	Tipo de operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");    //	Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");    //	Tipo de canal
        Field smh_activity_type = api.getField("smh_activity_type");    //	Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");    //	Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");    //	Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");    //	Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");    //	Tipo de mensagem
        Field smh_priority = api.getField("smh_priority");    //	Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");    //	Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");    //	Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");    //	Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");    //	Symbolic message source name
        Field smh_dest = api.getField("smh_dest");    //	Symbolic message destination name
        Field smh_multi_org_name = api.getField("smh_multi_org_name");    //	Nome da multi-organização
        Field rrr_seg_id_version = api.getField("rrr_seg_id_version");    //	Identificador do segmento
        Field rqo_seg_id_version = api.getField("rqo_seg_id_version");    //	Identificador do segmento
        Field rqo_tran_date = api.getField("rqo_tran_date");    //	Data da transacção
        Field rqo_tran_time = api.getField("rqo_tran_time");    //	Hora da transação
        Field rqo_tran_date_alt = api.getField("rqo_tran_date_alt");    //	Data de cobrança
        Field rua_seg_id_version = api.getField("rua_seg_id_version");    //	Identificador do segmento
        Field rua_numeric_002 = api.getField("rua_numeric_002");    //	Data de assinatura da autorização
        Field rua_4byte_string_001 = api.getField("rua_4byte_string_001");    //	Motivo da transferência
        Field rua_4byte_string_002 = api.getField("rua_4byte_string_002");    //	Categoria do motivo da transferência
        Field rua_4byte_string_004 = api.getField("rua_4byte_string_004");    //	Tipo de serviço
        Field rua_8byte_string_001 = api.getField("rua_8byte_string_001");    //	Canal da transacção
        Field rua_10byte_string_001 = api.getField("rua_10byte_string_001");    //	Instrumento
        Field rua_20byte_string_002 = api.getField("rua_20byte_string_002");    //	BIC do banco do devedor
        Field rua_20byte_string_003 = api.getField("rua_20byte_string_003");    //	Identificação do último devedor
        Field rua_80byte_nls_string_001 = api.getField("rua_80byte_nls_string_001");    //	Id da transacção
        Field rob_seg_id_version = api.getField("rob_seg_id_version");    //	Identificador do segmento
        Field rdk_seg_id_version = api.getField("rdk_seg_id_version");    //	Identificador do segmento
        Field rur_seg_id_version = api.getField("rur_seg_id_version");    //	Identificador do segmento
        Field rur_4byte_string_001 = api.getField("rur_4byte_string_001");    //	Tipo de movimento
        Field rur_30byte_string_001 = api.getField("rur_30byte_string_001");    //	Motivo para alteração da autorização
        Field rur_30byte_string_002 = api.getField("rur_30byte_string_002");    //	Referência da cobrança atribuída pelo credor
        Field rur_30byte_string_003 = api.getField("rur_30byte_string_003");    //	Identificação da autorização original
        Field xqo_seg_id_version = api.getField("xqo_seg_id_version");    //	Identificador do segmento
        Field xqo_cust_num = api.getField("xqo_cust_num");    //	ID Entidade BANKA do devedor
        Field xqo_cust_name = api.getField("xqo_cust_name");    //	Nome do devedor
        Field xqo_cust_cntry_code = api.getField("xqo_cust_cntry_code");    //	País do devedor
        Field aqo_seg_id_version = api.getField("aqo_seg_id_version");    //	Identificador do segmento
        Field aqo_acct_num = api.getField("aqo_acct_num");    //	IBAN do devedor
        Field aqd_seg_id_version = api.getField("aqd_seg_id_version");    //	Identificador do segmento
        Field tbt_seg_id_version = api.getField("tbt_seg_id_version");    //	Identificador do segmento
        Field tbt_tran_amt = api.getField("tbt_tran_amt");    //	Montante a liquidar em (EUR)
        Field tbt_tran_curr_code = api.getField("tbt_tran_curr_code");    //	Moeda da transacção
        Field tpp_seg_id_version = api.getField("tpp_seg_id_version");    //	Identificador do segmento
        Field tpp_name = api.getField("tpp_name");    //	Nome do credor
        Field tpp_cntry_code = api.getField("tpp_cntry_code");    //	País do credor
        Field tpp_bank_num = api.getField("tpp_bank_num");    //	BIC do banco do credor
        Field tpp_acct_num = api.getField("tpp_acct_num");    //	IBAN do credor

        // BAIE Message Header Segment-->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "CS");
        smh_authenticate_mtd.encodeText(txn, "NC");
        smh_channel_type.encodeText(txn, "N");
        smh_activity_type.encodeText(txn, "BF");
        smh_activity_detail1.encodeText(txn, "NAP");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_client_tran_type.encodeText(txn, "SEPADD");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_source.encodeText(txn, "SEPADD");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "GLOBAL");
        //BAIE RRR
        txn.addSegment(SegmentType.RRR);
        //BAIE RQO
        txn.addSegment(SegmentType.RQO);
        LocalDateTime d = LocalDateTime.parse(payment.getPmtInfo().getPrcDt(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_date_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()

        //BAIE RUA
        txn.addSegment(SegmentType.RUA);
        rua_numeric_002.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getAuthorizationDt());
        rua_4byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getAuthorizationDt());
        rua_4byte_string_002.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getTransferReason());
        rua_4byte_string_004.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getTransferReasonCd());
        rua_8byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getPmtMethod());
        rua_10byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getInstrument());
        rua_20byte_string_002.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getIssuedIdent().getIssuedIdentType());
        rua_20byte_string_003.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getContact().getOrgContactName());
        rua_80byte_nls_string_001.encodeText(txn, payment.getRqUID());
        //BAIE ROB
        txn.addSegment(SegmentType.ROB);
        //BAIE RDK
        txn.addSegment(SegmentType.RDK);
        //BAIE RUR
        txn.addSegment(SegmentType.RUR);
        rur_4byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getMovementCd());
        rur_30byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getChangeReason());
        rur_30byte_string_002.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getDDReference());
        rur_30byte_string_003.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getOrgAuthorization());

        //BAIE XQO
        txn.addSegment(SegmentType.XQO);
        xqo_cust_num.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getRefIdent());
        xqo_cust_name.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getContact().getContactName());
        xqo_cust_cntry_code.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getIssuedIdent().getGovIssuedIdent().getCountryCode().getCountryCodeValue());

        //BAIE AQO
        txn.addSegment(SegmentType.AQO);
        aqo_acct_num.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctKeys().getIBAN());
        //BAIE AQD
        txn.addSegment(SegmentType.AQD);
        //BAIE TBT
        txn.addSegment(SegmentType.TBT);
        tbt_tran_amt.encodeText(txn, payment.getPmtInfo().getCurAmt().getAmt());
        tbt_tran_curr_code.encodeText(txn, "EUR");
        //BAIE TPP
        txn.addSegment(SegmentType.TPP);
        tpp_name.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getContact().getContactName());
        tpp_cntry_code.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getIssuedIdent().getGovIssuedIdent().getCountryCode().getCountryCodeValue());
        tpp_bank_num.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getToAcctKeys().getBIC());
        tpp_acct_num.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getToAcctKeys().getIBAN());

        //BAIE send transaction
        try {
            logger.debug("Send transaction\n");
            send(lc, txn);
            logger.debug("Transaction sent.\n");
        } catch (Exception e) {
            logger.error("Error sending transaction.\n Transação: "+rua_80byte_nls_string_001+"\n");
        }
        //BAIE Receive
        try {
            logger.debug("Receive response message from Sas\n");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error. \n Transação: "+rua_80byte_nls_string_001+"\n");
            return null;
        }
        return responseStructuresHashMap;
    }


}
