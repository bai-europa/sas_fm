package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.models.ResponseStructure;
import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class DebitCardSender {


    //BAIE Init logger
    private static final Logger logger = Logger.getLogger(DebitCardSender.class);

    /**
     * Execute Card Transaction
     * Mapeamento para os campos do evaluate transaction online
     *
     * @param payment
     * @throws Exception
     */
    public static HashMap<String, String> processDebitCardTransaction(PmtAddRq payment) throws Exception {
        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        //SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction();

        //BAIE Segment known to exist
        //BAIE create fields
        logger.info("Card Transaction Mapping\n");
        logger.info("Mapping Field smh\n");
        //Field smh_seg_id_version = api.getField("smh_seg_id_version");// BAIE Identificador do segmento --> segmento SMH
        Field smh_tran_type = api.getField("smh_tran_type");//Tipo de transação
        Field smh_cust_type = api.getField("smh_cust_type");//Tipo de Cliente
        Field smh_acct_type = api.getField("smh_acct_type");//Tipo de Operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");//Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");//Tipo de Canal
        Field smh_activity_type = api.getField("smh_activity_type");//Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");//Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");//Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");//Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");//Client Defined Transaction Type
        Field smh_priority = api.getField("smh_priority");//Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");//Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");//Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");//Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");//smh_source
        Field smh_dest = api.getField("smh_dest");//smh_dest
        Field smh_multi_org_name = api.getField("smh_multi_org_name");//Nome de multi-organização
        logger.info("Mapping Field rqo\n");
//        Field rrr_seg_id_version = api.getField("rrr_seg_id_version");// BAIE Identificador do segmento --> segmento RRR
//        Field rqo_seg_id_version = api.getField("rqo_seg_id_version");//BAIE Identificador do segmento --> segmento RQO
        Field rqo_tran_date = api.getField("rqo_tran_date"); //UTC Data da transação
        Field rqo_tran_time = api.getField("rqo_tran_time");//UTC Hora da transação
        Field rqo_tran_date_alt = api.getField("rqo_tran_date_alt");// Date of transaction arrival in local time
        Field rqo_tran_time_alt = api.getField("rqo_tran_time_alt");// Time of transaction arrival in local time
        logger.info("Mapping Field xqo\n");
        Field xqo_seg_id_version = api.getField("xqo_seg_id_version");// Identificador do segmento
        Field xqo_cust_num = api.getField("xqo_cust_num");// Id Cliente BANKA (ordenante)
        Field xqo_cust_name = api.getField("xqo_cust_name");// Nome do cliente ordenante
        Field xqo_cust_cntry_code = api.getField("xqo_cust_cntry_code");// País do cliente ordenante
        Field xqo_cust_birth_dt = api.getField("xqo_cust_birth_dt");// Data de nascimento cliente ordenante
        logger.info("Mapping Field unm\n");
//        Field unm_seg_id_version = api.getField("unm_seg_id_version"); //BAIE Identificador do segmento --> segmento UNM
        Field unm_auth_mtd1 = api.getField("unm_auth_mtd1"); //Indicador de autenticação forte
        logger.info("Mapping Field hqo\n");
//        Field hqo_seg_id_version = api.getField("hqo_seg_id_version"); //BAIE Identificador do segmento --> segmento HQO
        Field hqo_ob_userid = api.getField("hqo_ob_userid"); //Utilizador Internet Banking
        logger.info("Mapping Field rua\n");
//        Field rua_seg_id_version = api.getField("rua_seg_id_version"); //BAIE Identificador do segmento --> segmento RUA
        Field rua_numeric_001 = api.getField("rua_numeric_001"); // Data valor da transacção
        Field rua_numeric_003 = api.getField("rua_numeric_003"); // Data de abertura da operação do cliente (ordenante)
        Field rua_numeric_005 = api.getField("rua_numeric_005"); // Data de maturidade da operação do cliente (ordenante)
        Field rua_8byte_string_001 = api.getField("rua_8byte_string_001"); // Canal da transacção
        Field rua_8byte_string_002 = api.getField("rua_8byte_string_002"); //Tipo de transferencia
        Field rua_80byte_nls_string_001 = api.getField("rua_80byte_nls_string_001"); //Id da transação
        Field rua_80byte_nls_string_002 = api.getField("rua_80byte_nls_string_002"); //IBAN da conta do cliente (ordenante)
        Field rua_80byte_nls_string_003 = api.getField("rua_80byte_nls_string_003"); //Id da transação
        Field rua_20byte_string_005 = api.getField("rua_20byte_string_005"); //Id da transação
        Field rua_30byte_string_001 = api.getField("rua_30byte_string_001"); //IBAN da conta do cliente (ordenante)
        Field rua_30byte_string_002 = api.getField("rua_30byte_string_002"); //Id da transação
        logger.info("Mapping Field tbt\n");
////        Field tbt_seg_id_version = api.getField("tbt_seg_id_version"); //BAIE Identificador do segmento --> segmento TBT
        Field tbt_tran_amt = api.getField("tbt_tran_amt");//Montante da transacção em EUR
        Field tbt_tran_curr_code = api.getField("tbt_tran_curr_code");//Moeda da transacção
        logger.info("Mapping Field tpp\n");
//        Field tpp_seg_id_version = api.getField("tpp_seg_id_version"); //BAIE Identificador do segmento --> segmento TPP
        Field tpp_payee_payer_ind = api.getField("tpp_payee_payer_ind");//Tipo de Beneficiário
        Field tpp_num = api.getField("tpp_num");//IBAN do Beneficiário
        Field tpp_name = api.getField("tpp_name");// Nome do Beneficiário
        Field tpp_cntry_code = api.getField("tpp_cntry_code");//País de Beneficiário
        Field tpp_city = api.getField("tpp_city");//Morada de Beneficiário
        Field tpp_bank_num = api.getField("tpp_bank_num");//BIC do banco do Beneficiário
        Field tpp_bank_name = api.getField("tpp_bank_name");//Nome do banco do Beneficiário
        Field tpp_acct_num = api.getField("tpp_acct_num");//Id da conta do Beneficiário
        Field tpp_description = api.getField("tpp_description");//Descritivo da transação
        logger.info("Mapping Field aqo\n");
//        Field aqo_seg_id_version = api.getField("aqo_seg_id_version"); //BAIE Identificador do segmento --> segmento AQO
        Field aqo_acct_num = api.getField("aqo_acct_num");//Id da operação do cliente
        //BAIE Segmento DUA
        logger.info("Mapping Field dua\n");
        Field dua_20byte_string_001 = api.getField("dua_20byte_string_001");
        Field dua_20byte_string_002 = api.getField("dua_20byte_string_002");
        Field dua_40byte_string_001 = api.getField("dua_40byte_string_001");
        Field dua_80byte_string_001 = api.getField("dua_80byte_string_001");
        Field dua_80byte_string_002 = api.getField("dua_80byte_string_002");

        //BAIE Message Header Segment-->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "CS");
        smh_authenticate_mtd.encodeText(txn, "NC");
        smh_activity_type.encodeText(txn, "BF");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "GLOBAL");
        smh_activity_detail1.encodeText(txn, "NAP");
        smh_channel_type.encodeText(txn, "C");
        smh_client_tran_type.encodeText(txn, "MCards");
        smh_source.encodeText(txn, "Cards");

        //BAIE Common Segment -->Variáveis de resposta da solução
        txn.addSegmentIfMissing(SegmentType.RRR);

        //BAIE Date/Time Segment --> Informação sobre tempo e data de transacção
        txn.addSegmentIfMissing(SegmentType.RQO);
//      long t = System.currentTimeMillis();
        LocalDateTime d = LocalDateTime.parse(payment.getPmtInfo().getPrcDt(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_date_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()

        //BAIE Detalhe sobre cliente
        txn.addSegmentIfMissing(SegmentType.XQO);
        xqo_cust_num.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getRefData().getRefIdent());//Id Cliente BANKA (ordenante) --> <PmtAddRq><PmtInfo><PmtInstruction><RefData><RefIdent>
        xqo_cust_name.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getContact().getContactName()); //Nome cliente ordenante --> <PmtAddRq><PmtInfo><DebtorData><PartyData><Contact><ContactName>
        xqo_cust_cntry_code.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctInfo().getTaxCountry().getCountryCodeValue());//País do cliente ordenante --><PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctInfo><TaxCountry><CountryCodeValue>
        xqo_cust_birth_dt.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getIssuedIdent().getBirthDt()); //Data de nascimento do cliente ordenante --><PmtAddRq><PmtInfo><DebtorData><PartyData><IssuedIdent><BirthDt>

        //BAIE Informação de autenticação não relacionada com cartões
        txn.addSegment(SegmentType.UNM);
        unm_auth_mtd1.encodeText(txn, "");
//        unm_auth_mtd1.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getStrongAuth());//Indicador de autenticação forte -->  <PmtAddRq><PmtInfo><PmtInstruction><StrongAuth>

        //BAIE Channel Segment
        txn.addSegment(SegmentType.HQO);
        hqo_ob_userid.encodeText(txn, payment.getPmtInfo().getDebtorData().getPartyData().getIssuedIdent().getIssuedIdentValue());// Utilizador Internet Banking --> <PmtAddRq><PmtInfo><DebtorData><PartyData><IssuedIdent><IssuedIdentValue>

        //BAIE Common Segment Variáveis genéricas
        txn.addSegment(SegmentType.RUA);
        String duedt = payment.getPmtInfo().getPmtInstruction().getDueDt();
        Integer datas = Integer.valueOf(duedt.substring(0, 4) + duedt.substring(5, 7) + duedt.substring(8, 10));
        rua_numeric_001.encodeDouble(txn, datas);// Data valor da transação --> <PmtAddRq><PmtInfo><PmtInstruction><DueDt> --> nas Transf. internas não está nesta tag

        String opendt = payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctInfo().getOpenDt();
        String dataOpendt = (opendt.substring(0, 4) + opendt.substring(5, 7) + opendt.substring(8, 10));
        rua_numeric_003.encodeText(txn, dataOpendt); //Data de abertura da operação do cliente (ordenante) --><PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctInfo><OpenDt>

        String maturityDt = payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctInfo().getMaturityDt();
        String dataMaturityDt = (maturityDt.substring(0, 4) + maturityDt.substring(5, 7) + maturityDt.substring(8, 10));
        rua_numeric_005.encodeText(txn, dataMaturityDt);//Data de maturidade da operação do cliente (ordenante) --> <PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctInfo><MaturityDt>
        rua_8byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getPmtMethod());//BAIE Canal de transação --><PmtAddRq><PmtInfo><PmtInstruction><PmtMethod>
        rua_8byte_string_002.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getPmtType());//BAIE Tipo de transferencia --> <PmtAddRq><PmtInfo><PmtInstruction><PmtType>
        rua_20byte_string_005.encodeText(txn, payment.getPmtInfo().getPmtAddRq().getRqUID()); //BAIE Id da transação --><PmtAddRq><PmtInfo><PmtAddRq><RqUID>
        rua_30byte_string_001.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getTPPId());//BAIE Id de TPP --><PmtAddRq><PmtInfo><PmtInstruction><TPPId>
        rua_30byte_string_002.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctKeys().getIBAN());//BAIE IBAN da conta do cliente (ordenante)--><PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctKeys><IBAN>

        //BAIE Detalhe de transferência via canal digital
        txn.addSegment(SegmentType.TBT);
        Double ammountValue = Double.parseDouble(payment.getPmtInfo().getCurAmt().getAmt());//Montante de transação em EUR --> <PmtAddRq><PmtInfo><CurAmt><Amt>
        tbt_tran_amt.encodeDouble(txn, ammountValue);
        tbt_tran_curr_code.encodeText(txn, payment.getPmtInfo().getCurAmt().getCurCode());//Moeda da transação --><PmtAddRq><PmtInfo><CurAmt><CurCode>

        //BAIE Informação sobre TPP
        txn.addSegment(SegmentType.TPP);
        tpp_payee_payer_ind.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getIssuedIdent().getIssuedIdentValue());//Tipo de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><IssuedIdent><IssuedIdentValue>
        tpp_num.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getToAcctKeys().getIBAN());//IBAN de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><ToAcctKeys><IBAN>
        tpp_name.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getContact().getContactName());//Nome de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><Contact><ContactName>
        tpp_cntry_code.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getIssuedIdent().getGovIssuedIdent().getCountryCode().getCountryCodeValue());//BAIE País de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><IssuedIdent><GovIssuedIdent><CountryCode><CountryCodeValue>
        tpp_city.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getUltimateCreditorData().getPartyData().getContact().getLocator().getPostAddr().getAddr1());//Morada de beneficiário --> <PmtAddRq><PmtInfo><PmtCreditDetail><UltimateCreditorData><PartyData><Contact><Locator><PostAddr><Addr1>
        tpp_bank_num.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getToAcctKeys().getBIC());//BIC do banco do Beneficiário --> <PmtAddRq><PmtInfo><<PmtCreditDetail><ToAcctKeys><BIC>
        tpp_acct_num.encodeText(txn, payment.getPmtInfo().getPmtCreditDetail().getToAcctKeys().getAcctId());//Id da conta do Beneficiário --><PmtAddRq><PmtInfo><PmtCreditDetail><ToAcctKeys><AcctId>
        tpp_description.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getMemo());//Descritivo da transação --> <PmtAddRq><PmtInfo><PmtInstruction><Memo>


        // BAIE Dimensões de operação
        txn.addSegment(SegmentType.AQO);
        aqo_acct_num.encodeText(txn, payment.getPmtInfo().getPmtInstruction().getFromAcctRef().getAcctKeys().getAcctId());//Id da operação do cliente ordenante --> <PmtAddRq><PmtInfo><PmtInstruction><FromAcctRef><AcctKeys><AcctId>

        txn.addSegment(SegmentType.HCT);

        //BAIE Detalhe sobre a decisão anterior
        txn.addSegment(SegmentType.ROB);

        //BAIE Detalhe sobre a chave de decisão
        txn.addSegment(SegmentType.RDK);

        //BAIE Variáveis genéricas
        txn.addSegment(SegmentType.RUR);

        //BAIE Detalhe de saldo de operação
        txn.addSegment(SegmentType.AQD);


        //BAIE send
        try {
            logger.debug("Send transaction\n");
            send(lc, txn);
            logger.debug("Transaction sent.");
        } catch (Exception e) {
            logger.error("Error sending transaction \n");
        }
        //BAIE Receive

        try {
            logger.debug("Receive response message from Sas");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error\n");
            return null;
        }
        return responseStructuresHashMap;
    }
}
