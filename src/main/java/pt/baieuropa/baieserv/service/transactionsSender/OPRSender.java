package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.ResponseStructure;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class OPRSender {

    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(OPRSender.class);

    public OPRSender() {
    }

    /**
     * Execute OPR
     *
     * @param content
     * @return
     */
    public static HashMap<String, String> processOPR(String content) {
        JSONObject transactionJsonObject = new JSONObject(content);
        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        //BAIE SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction();

        //BAIE Segment known to exist
        //BAIE create fields
        logger.info("OPR Mapping\n");
//        Field smh_seg_id_version = api.getField("smh_seg_id_version");//Identificador do segmento
//        Field smh_msg_version = api.getField("smh_msg_version");    //Versão da API associada à solução
        Field smh_tran_type = api.getField("smh_tran_type");    //Tipo de transacção
        Field smh_cust_type = api.getField("smh_cust_type");    //Tipo de cliente
        Field smh_acct_type = api.getField("smh_acct_type");    //Tipo de operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");    //Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");    //Tipo de canal
        Field smh_activity_type = api.getField("smh_activity_type");    //Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");    //Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");    //Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");    //Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");    //Client Defined Transaction Type
        Field smh_priority = api.getField("smh_priority");    //Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");    //Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");    //Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");    //Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");
        Field smh_dest = api.getField("smh_dest");
        Field smh_multi_org_name = api.getField("smh_multi_org_name");    //Nome da multi-organização
//        Field rrr_seg_id_version = api.getField("rrr_seg_id_version");	//Identificador do segmento
//        Field rqo_seg_id_version = api.getField("rqo_seg_id_version");	//Identificador do segmento
        Field rqo_tran_date = api.getField("rqo_tran_date");    //Data da transacção
        Field rqo_tran_time = api.getField("rqo_tran_time");    //Hora da transação
        Field rqo_tran_date_alt = api.getField("rqo_tran_date_alt");    //Date of transaction arrival in local time
        Field rqo_tran_time_alt = api.getField("rqo_tran_time_alt");    //Time of transaction arrival in local time
//        Field rua_seg_id_version = api.getField("rua_seg_id_version");	//Identificador do segmento
        Field rua_3byte_string_001 = api.getField("rua_3byte_string_001");    //Identificador do owner dos custos de transfererência
        Field rua_4byte_string_001 = api.getField("rua_4byte_string_001");//BAIE Motivo da transferência
        Field rua_4byte_string_002 = api.getField("rua_4byte_string_002");//BAIE Categoria do motivo da transferencia
        Field rua_4byte_string_003 = api.getField("rua_4byte_string_003");    //Tipo de operação
        Field rua_8byte_string_001 = api.getField("rua_8byte_string_001");    //Canal da transacção
        Field rua_10byte_string_001 = api.getField("rua_10byte_string_001");    //Instrumento
        Field rua_10byte_string_008 = api.getField("rua_10byte_string_008");    //Operador
        Field rua_20byte_string_002 = api.getField("rua_20byte_string_002");    //BIC do Banco do destinatário
        Field rua_20byte_string_003 = api.getField("rua_20byte_string_003");//BAIE Nome do ordenante original
        Field rua_20byte_string_004 = api.getField("rua_20byte_string_004");//BAIE Nome do ultimo destinatário
        Field rua_80byte_nls_string_001 = api.getField("rua_80byte_nls_string_001");    //Id da transacção
        Field rua_80byte_nls_string_002 = api.getField("rua_80byte_nls_string_002");    //BAIE Referência da transferência atribuida pelo ordenante
        Field rua_numeric_001 = api.getField("rua_numeric_001"); //Data valor da transação
//        Field rob_seg_id_version = api.getField("rob_seg_id_version");	//Identificador do segmento
//        Field rdk_seg_id_version = api.getField("rdk_seg_id_version");	//Identificador do segmento
//        Field rur_seg_id_version = api.getField("rur_seg_id_version"); 	//Identificador do segmento
//        Field xqo_seg_id_version = api.getField("xqo_seg_id_version");	//Identificador do segmento
        Field xqo_cust_num = api.getField("xqo_cust_num");//BAIE Id entidade BANKA do beneficiário
        Field xqo_cust_name = api.getField("xqo_cust_name");    //Nome do cliente (ordenante)
        Field xqo_cust_cntry_code = api.getField("xqo_cust_cntry_code");    //País do cliente (ordenante)
        Field xqo_address = api.getField("xqo_address");    //Morada do beneficiário
//        Field aqo_seg_id_version = api.getField("aqo_seg_id_version");	//Identificador do segmento
        Field aqo_acct_num = api.getField("aqo_acct_num");    //IBAN do beneficiário
        Field aqo_bill_curr_conv_rate = api.getField("aqo_bill_curr_conv_rate"); //Taxa de conversão
//        Field aqd_seg_id_version = api.getField("aqd_seg_id_version");    //Identificador do segmento
        Field tdp_seg_id_version = api.getField("tdp_seg_id_version");//Identificador do segmento
        Field tdp_billing_amt = api.getField("tdp_billing_amt");//BAIE Montante da transacção
        Field tdp_client_curr_code = api.getField("tdp_client_curr_code");// BAIE Moeda
//        Field tpp_seg_id_version = api.getField("tpp_seg_id_version");	//Identificador do segmento
        Field tpp_name = api.getField("tpp_name");    //Nome do beneficiário
        Field tpp_cntry_code = api.getField("tpp_cntry_code");    //País do beneficiário
        Field tpp_bank_num = api.getField("tpp_bank_num");    //BIC do banco do beneficiário
        Field tpp_acct_num = api.getField("tpp_acct_num");    //Id da conta do beneficiário
        Field tpp_description = api.getField("tpp_description");    //Descritivo da transacção
        Field tpp_address = api.getField("tpp_address");
        Field tbt_tran_amt = api.getField("tbt_tran_amt");    //Montante da transacção em EUR
        Field tbt_tran_curr_code = api.getField("tbt_tran_curr_code");    //Moeda da transacção

        //BAIE Message Header Segment-->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "CS");
        smh_authenticate_mtd.encodeText(txn, "NA");
        smh_channel_type.encodeText(txn, "N");
        smh_activity_type.encodeText(txn, "DP");
        smh_activity_detail1.encodeText(txn, "NAP");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_client_tran_type.encodeText(txn, "Recebida");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_source.encodeText(txn, "Recebida");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "GLOBAL");

        //BAIE Common Segment -->Variáveis de resposta da solução
        txn.addSegmentIfMissing(SegmentType.RRR);

        //BAIE Date/Time Segment --> Informação sobre tempo e data de transacção
        txn.addSegmentIfMissing(SegmentType.RQO);
//        long t = System.currentTimeMillis();
        LocalDateTime d = LocalDateTime.parse(transactionJsonObject.getString("rqo_tran_date"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_date_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()
        rqo_tran_time_alt.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart(<PmtAddRq><PmtInfo><PrcDt>))  payment.getPmtInfo().getPrcDt()

        //BAIE Common Segment Variáveis genéricas
        txn.addSegment(SegmentType.RUA);
        String duedt = transactionJsonObject.getString("rua_numeric_001");
        Integer datas = Integer.valueOf(duedt.substring(0, 4) + duedt.substring(5, 7) + duedt.substring(8, 10));
        rua_numeric_001.encodeDouble(txn, datas);// Data valor da transação

        rua_3byte_string_001.encodeText(txn, transactionJsonObject.getString("rua_3byte_string_001"));//Identificador do owner dos custos de transfererência
        rua_4byte_string_001.encodeText(txn, transactionJsonObject.getString("rua_4byte_string_001"));//Motivo da transferencia
        rua_4byte_string_002.encodeText(txn, transactionJsonObject.getString("rua_4byte_string_002"));//Categoria do motivo de transferencia
        rua_4byte_string_003.encodeText(txn, transactionJsonObject.getString("rua_4byte_string_003"));//Tipo de operação
        rua_8byte_string_001.encodeText(txn, transactionJsonObject.getString("rua_8byte_string_001")); //Canal da transação
        rua_10byte_string_001.encodeText(txn, transactionJsonObject.getString("rua_10byte_string_001"));//Intrumento
        rua_10byte_string_008.encodeText(txn, transactionJsonObject.getString("rua_10byte_string_008"));//Operador
        rua_20byte_string_002.encodeText(txn, transactionJsonObject.getString("rua_20byte_string_002"));//BIC do banco do destinatario
        if (transactionJsonObject.getString("rua_20byte_string_003").length() >= 20) {
            rua_20byte_string_003.encodeText(txn, transactionJsonObject.getString("rua_20byte_string_003").substring(0, 20));//Nome do ordenante original
        }
        if (transactionJsonObject.getString("rua_20byte_string_004").length() >= 20) {
            rua_20byte_string_004.encodeText(txn, transactionJsonObject.getString("rua_20byte_string_004").substring(0, 20));//nome do ultimo destinatario
        }
        rua_80byte_nls_string_001.encodeText(txn, transactionJsonObject.getString("rua_80byte_nls_string_001"));//Id fa transação
        rua_80byte_nls_string_002.encodeText(txn, transactionJsonObject.getString("rua_80byte_nls_string_001"));//Referência da transferência atribuida pelo ordenante

        //BAIE Detalhe sobre a decisão anterior
        txn.addSegment(SegmentType.ROB);

        //BAIE Detalhe sobre a chave de decisão
        txn.addSegment(SegmentType.RDK);

        //BAIE Variáveis genéricas
        txn.addSegment(SegmentType.RUR);

        //BAIE Detalhe sobre cliente
        txn.addSegmentIfMissing(SegmentType.XQO);
        xqo_cust_num.encodeText(txn, transactionJsonObject.getString("xqo_cust_num"));//Id da entidade da banka do beneficiario
        xqo_cust_name.encodeText(txn, transactionJsonObject.getString("xqo_cust_name")); //Nome do beneficiário
        xqo_cust_cntry_code.encodeText(txn, transactionJsonObject.getString("xqo_cust_cntry_code"));//País do beneficiário
        xqo_address.encodeText(txn, transactionJsonObject.getString("xqo_address")); //Morada do beneficiário

        //BAIE Dimensões de operação
        txn.addSegment(SegmentType.AQO);
        aqo_acct_num.encodeText(txn, transactionJsonObject.getString("aqo_acct_num"));//Id da operação do cliente ordenante
        Double taxaConversao = Double.parseDouble(transactionJsonObject.getString("aqo_bill_curr_conv_rate"));
        aqo_bill_curr_conv_rate.encodeDouble(txn, taxaConversao);//Taxa de conversão

        txn.addSegment(SegmentType.AQD);

        txn.addSegment(SegmentType.TDP);
        Double montante = Double.parseDouble(transactionJsonObject.getString("tdp_billing_amt").replace(",", "."));
        tdp_billing_amt.encodeDouble(txn, montante);//Montante da transacção
        tdp_client_curr_code.encodeText(txn, transactionJsonObject.getString("tdp_client_curr_code"));//Moeda


        //BAIE Informação sobre TPP
        txn.addSegment(SegmentType.TPP);
        tpp_name.encodeText(txn, transactionJsonObject.getString("tpp_name"));//Nome do ordenante
        tpp_cntry_code.encodeText(txn, transactionJsonObject.getString("tpp_cntry_code"));//País de ordenante
        tpp_bank_num.encodeText(txn, transactionJsonObject.getString("tpp_bank_num"));//BIC do banco do ordenante
        tpp_acct_num.encodeText(txn, transactionJsonObject.getString("tpp_acct_num"));//Id da conta do ordenante
        Integer tamanho = transactionJsonObject.getString("tpp_description").length();
        if(tamanho>=100){
            String description = transactionJsonObject.getString("tpp_description").substring(0,99);
            tpp_description.encodeText(txn, description);//Descritivo da transação
        }else{
            tpp_description.encodeText(txn, transactionJsonObject.getString("tpp_description"));//Descritivo da transação
        }
        tpp_address.encodeText(txn, transactionJsonObject.getString("tpp_address"));   //Morada do ordenante


        //BAIE send
        try {
            logger.debug("Send transaction\n");
            send(lc, txn);
            logger.debug("Transaction sent.\n");
        } catch (Exception e) {
            logger.error("Error sending transaction . \n Transação: "+ rua_80byte_nls_string_001 +"\n");
        }
        //BAIE Receive
        try {
            logger.debug("Receive response message from Sas\n");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error\n Transação: "+ rua_80byte_nls_string_001 +"\n");
            return null;
        }
        return responseStructuresHashMap;
    }
}
