package pt.baieuropa.baieserv.service.transactionsSender;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.models.ResponseStructure;
import pt.baieuropa.baieserv.models.alteracaoDetalhesConta.PartyModRq;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

import static pt.baieuropa.baieserv.service.SasConnections.*;

public class ChangeAccountDetailsSender {

    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(ChangeAccountDetailsSender.class);

    /**
     * Execute Change account details
     *
     * @param request
     * @return
     */
    public static HashMap<String, String> processChangeAccountDetails(PartyModRq request) {

        HashMap<String, String> responseStructuresHashMap = new HashMap<>();
        // SAS API objects
        MessageAPI api = MessageAPI.getDefault();
        MessageApiEncoding apie = MessageApiEncoding.getDefault();
        LengthCodec lc = LengthCodec.PREPEND2;
        Locale.setDefault(Locale.US);//BAIE É necessário para conversões de valores
        Transaction txn = new Transaction(apie);

        //BAIE Segment known to exist
        // BAIE create fields
        logger.info("Change Account details Mapping\n");
        //BAIE SMH Segment
        Field smh_tran_type = api.getField("smh_tran_type");//Tipo de transação
        Field smh_cust_type = api.getField("smh_cust_type");//Tipo de Cliente
        Field smh_acct_type = api.getField("smh_acct_type");//Tipo de Operação
        Field smh_authenticate_mtd = api.getField("smh_authenticate_mtd");//Método de autenticação
        Field smh_channel_type = api.getField("smh_channel_type");//Tipo de Canal
        Field smh_activity_type = api.getField("smh_activity_type");//Tipo de actividade
        Field smh_activity_detail1 = api.getField("smh_activity_detail1");//Detalhe actividade 1
        Field smh_activity_detail2 = api.getField("smh_activity_detail2");//Detalhe actividade 2
        Field smh_activity_detail3 = api.getField("smh_activity_detail3");//Detalhe actividade 3
        Field smh_client_tran_type = api.getField("smh_client_tran_type");//Client Defined Transaction Type
        Field smh_priority = api.getField("smh_priority");//Prioridade da mensagem
        Field smh_msg_type = api.getField("smh_msg_type");//Código de tipo de mensagem
        Field smh_resp_req = api.getField("smh_resp_req");//Código de tipo de resposta
        Field smh_sdd_ind = api.getField("smh_sdd_ind");//Código de estrutura de mensagem
        Field smh_source = api.getField("smh_source");//smh_source
        Field smh_dest = api.getField("smh_dest");//smh_dest
        Field smh_multi_org_name = api.getField("smh_multi_org_name");//Nome de multi-organização
        //BAIE HQO Segment
        Field hqo_ob_userid = api.getField("hqo_ob_userid");//Utilizador internet banking
        //BAIE RUA Segment
        Field rua_20byte_string_005 = api.getField("rua_20byte_string_005");//Id da transação
        //BAIE RQO Segment
        Field rqo_tran_date = api.getField("rqo_tran_date"); //UTC Data da transação
        Field rqo_tran_time = api.getField("rqo_tran_time");//UTC Hora da transação
        Field rqo_tran_date_client = api.getField("rqo_tran_date_client");// Client defined Transaction Expiration Date (UTC)
        Field rqo_tran_time_client = api.getField("rqo_tran_time_client");// Client defined Transaction Expiration Date (UTC)
        //BAIE UNM Segment
        Field unm_auth_mtd1 = api.getField("unm_auth_mtd1"); //Indicador de autenticação forte
        //BAIE XQO Segment
        Field xqo_cust_num = api.getField("xqo_cust_num");//Id cliente banka
        //BAIE TNG Segment
        Field tng_sub_tran_type = api.getField("tng_sub_tran_type");//Tipo de alteração
        //BAIE DNU Segment
        Field dnu_u_string1 = api.getField("dnu_u_string1");//Contacto(Morada, contacto telefonico ou email)
        Field dnu_u_string2 = api.getField("dnu_u_string2");//Nome do cliente
        Field dnu_u_string3 = api.getField("dnu_u_string3");//Lingua do internet banking
        Field dnu_u_amt1 = api.getField("dnu_u_amt1");//Valor do limite de transferencia

        //BAIE Message Header Segment-->Informação genérica sobre cliente, operação, transacção e canal
        txn.addSegment(SegmentType.SMH);
        smh_tran_type.encodeText(txn, "TRX");
        smh_cust_type.encodeText(txn, "B");
        smh_acct_type.encodeText(txn, "NA");
        smh_authenticate_mtd.encodeText(txn, "NC");
        smh_channel_type.encodeText(txn, "O");
        smh_activity_type.encodeText(txn, "NM");
        smh_activity_detail1.encodeText(txn, "DNU");
        smh_activity_detail2.encodeText(txn, "NAP");
        smh_activity_detail3.encodeText(txn, "NAP");
        smh_client_tran_type.encodeText(txn, "DtlChng");
        smh_priority.encodeText(txn, "2");
        smh_msg_type.encodeText(txn, "1");
        smh_resp_req.encodeText(txn, "1");
        smh_sdd_ind.encodeText(txn, "1");
        smh_source.encodeText(txn, "Digital");
        smh_dest.encodeText(txn, "SFME");
        smh_multi_org_name.encodeText(txn, "GLOBAL");

        //BAIE Channel Segment
        txn.addSegment(SegmentType.HQO);
        hqo_ob_userid.encodeText(txn, request.getPartyKeys().getLoginIdent().getLoginName());//BAIE Utilizador Internet Banking --> <PartyModRq><PartyKeys><LoginIdent><LoginName>

        //BAIE Detalhe técnico
        txn.addSegment(SegmentType.HOB);

        //BAIE Common Segment Variáveis genéricas
        txn.addSegment(SegmentType.RUA);
        rua_20byte_string_005.encodeText(txn, request.getRqUID()); //BAIE Id da transação --><PartyModRq><RqUID>

        //BAIE Date/Time Segment --> Informação sobre tempo e data de transacção
        txn.addSegmentIfMissing(SegmentType.RQO);
        LocalDateTime d = LocalDateTime.parse(request.getPartyInfo().getPrcDt(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        long t = d.toInstant(ZoneOffset.ofTotalSeconds(0)).toEpochMilli();
        rqo_tran_date.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart((<PartyModRq><PartyInfo><PrcDt>))))
        rqo_tran_time.encodeMilliTime(txn, t);//TODO alterar para UTC(timepart(<PartyModRq><PartyInfo><PrcDt>))
        rqo_tran_date_client.encodeMilliTime(txn, t);//TODO alterar para UTC(datepart((<PartyModRq><PartyInfo><PrcDt>))))
        rqo_tran_time_client.encodeMilliTime(txn, t);//TODO alterar para UTC(timepart(<PartyModRq><PartyInfo><PrcDt>))

        //BAIE Informação de autenticação não relacionada com cartões
        txn.addSegment(SegmentType.UNM);
        unm_auth_mtd1.encodeText(txn, "P");//BAIE Indicador de autenticação forte -->  <PartyModRq><PartyInfo><StrongAuth>

        //BAIE Detalhe sobre cliente
        txn.addSegmentIfMissing(SegmentType.XQO);
        //BAIE Id Cliente BANKA (ordenante) --><PartyModRq><PartyKeys><PartyId>
        xqo_cust_num.encodeText(txn, request.getPartyKeys().getPartyId());

        //BAIE
        txn.addSegmentIfMissing(SegmentType.TNG);
        //BAIE Tipo de alteração --> <PartyModRq><PartyInfo><SecObjPurpose>
        tng_sub_tran_type.encodeText(txn, request.getPartyInfo().getSecObjPurpose());

        //BAIE
        txn.addSegmentIfMissing(SegmentType.DNU);
        if (request.getPartyInfo().getSecObjPurpose().equals("MailChng") || request.getPartyInfo().getSecObjPurpose().equals("AddChng") || request.getPartyInfo().getSecObjPurpose().equals("CntChng")) {
            dnu_u_string1.encodeText(txn, request.getPartyInfo().getCorrespondence().getDesc());
        } else if (request.getPartyInfo().getSecObjPurpose().equals("NameChng")) {
            dnu_u_string2.encodeText(txn, request.getPartyInfo().getName());
        } else if (request.getPartyInfo().getSecObjPurpose().equals("LangChng")) {
            dnu_u_string3.encodeText(txn, request.getPartyInfo().getPartyPref().getLanguage());
        } else if (request.getPartyInfo().getSecObjPurpose().equals("LimChng")) {
            dnu_u_amt1.encodeText(txn, request.getPartyInfo().getPartyPref().getFinancialAmt());
        }

        // BAIE Common Segment
        txn.addSegmentIfMissing(SegmentType.RRR);

        //BAIE Detalhe sobre a decisão anterior
        txn.addSegment(SegmentType.ROB);

        //BAIE Detalhe sobre a chave de decisão
        txn.addSegment(SegmentType.RDK);

        //BAIE Variáveis genéricas
        txn.addSegment(SegmentType.RUR);


        //BAIE send
        try {
            logger.debug("Send operation\n");
            send(lc, txn);
            logger.debug("Operation sent.");
        } catch (Exception e) {
            logger.error("Error sending operation \n");
        }
        //BAIE Receive
        try {
            logger.debug("Receive response message from Sas");
            if (txn.responseRequired()) {
                txn = recv(lc, apie);
                api = txn.getApi();
                logger.debug("rcvd: " + txn);
                for (String name : respFields) {
                    Field f = api.getField(name);
                    logger.info("- " + name + ": " + f.decodeTrim(txn));
                    responseStructuresHashMap.put(name, f.decodeTrim(txn));
                }
                logger.debug("Received info\n");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            logger.error("Response Required error\n");
            return null;
        }
        return responseStructuresHashMap;
    }
}
