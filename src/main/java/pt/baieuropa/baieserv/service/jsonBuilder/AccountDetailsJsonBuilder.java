package pt.baieuropa.baieserv.service.jsonBuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.alteracaoDetalhesConta.*;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Slf4j
public class AccountDetailsJsonBuilder {

    /**
     * Leitura de json com campos recebidos via nats
     * Transações de detalhes de contas
     *
     * @param content
     * @param clientType
     * @return
     */
    public PartyModRq createAccountDetailsStructure(String content, String clientType) throws IOException, ParseException {
        log.info("Create Account Details Structure\n");
        JSONObject jsonObject = new JSONObject(content);

        PartyModRq partyModRq = new PartyModRq();
        //BAIE criação de estrutura com pojos

        partyModRq.setRqUID("1");//BAIE 1 id transação<-------------------------------------
        PartyInfo partyInfo = new PartyInfo();
        partyInfo.setPrcDt(""); //BAIE 2 data e hora transação<-------------------------------------
        if (clientType.equals("Enterprise")) {
            String orgDate = "";
            //BAIE verifica o valor da establishedDt e preenche caso esteja a vazio == 0
            if(jsonObject.getJSONObject("orgPartyInfo").getString("OrgEstablishDt").equals("0")){
                orgDate="19000101";
            }else{
                orgDate=jsonObject.getJSONObject("personPartyInfo").getString("establishedDt");
            }
            // This is the format date we want
            DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // This format date is actually present
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String d = mSDF.format(formatter.parse(orgDate));
            partyInfo.setPrcDt(d);//BAIE data em que é executado
        } else if (clientType.equals("Particular")) {
            String partyDate = "";
            //BAIE verifica o valor da establishedDt e preenche caso esteja a vazio == 0
            if(jsonObject.getJSONObject("personPartyInfo").getString("establishedDt").equals("0")){
                partyDate="19000101";
            }else{
                partyDate=jsonObject.getJSONObject("personPartyInfo").getString("establishedDt");
            }
            // This is the format date we want
            DateFormat mSDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // This format date is actually present
            SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
            String d = mSDF.format(formatter.parse(partyDate));
            partyInfo.setPrcDt(d);//BAIE data em que é executado
        }
        partyInfo.setSecObjPurpose(jsonObject.getString("actionType"));//BAIE 5 action type-->Tipo de alteração<-------------------------------------
        partyInfo.setStrongAuth("");//BAIE 6 autenticação forte<-------------------------------------
        //BAIE if Action Type = NameChng then <PartyModRq><PartyInfo><Name> else ""
        if (partyInfo.getSecObjPurpose().equals("NameChng")) {
            if (clientType.equals("Enterprise")) {
                partyInfo.setName(jsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONObject("orgName").getString("name"));//BAIE 8 detalhes de nome
            } else if (clientType.equals("Particular")) {
                partyInfo.setName(jsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONObject("personName").getString("fullName"));//BAIE 8 detalhes de nome
            }
        }
        Correspondence correspondence = new Correspondence();
        //BAIE if Action Type in (AddChng, CntChng, MailChng) then <PartyModRq><PartyInfo><Correspondence><Desc> else ""
        if (partyInfo.getSecObjPurpose().equals("AddChng") || partyInfo.getSecObjPurpose().equals("CntChng") || partyInfo.getSecObjPurpose().equals("MailChng")) {
            if (clientType.equals("Enterprise")) {
                JSONArray contactsArray = jsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONArray("contacts");
                for (int i = 0; i < contactsArray.length(); i++) {
                    JSONObject locator = contactsArray.getJSONObject(i).getJSONObject("locator");
                    try {
                        if(contactsArray.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                            correspondence.setDesc(locator.getString("addr1") + locator.getString("addr2"));//BAIE 7 detalhes de contacto , morada e email
                    } catch (Exception ex) {
                        // nothing to do
                    }
                }

            } else if (clientType.equals("Particular")) {
                JSONArray contactsArray = jsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONArray("contacts");
                for (int i = 0; i < contactsArray.length(); i++) {
                    JSONObject locator = contactsArray.getJSONObject(i).getJSONObject("locator");
                    try {
                        if(contactsArray.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                            correspondence.setDesc(locator.getString("addr1") + locator.getString("addr2"));//BAIE 7 detalhes de contacto , morada e email
                    } catch (Exception ex) {
                        // nothing to do
                    }
                }


            }
        }
        PartyPref partyPref = new PartyPref();
        //BAIE if Action Type = LangChng then <PartyModRq><PartyInfo><PartyPref><Language> else  ""
        if (partyInfo.getSecObjPurpose().equals("LangChng")) {
            if (clientType.equals("Enterprise")) {
                partyPref.setLanguage("");//BAIE 9 lingua interface
            } else if (clientType.equals("Particular")) {
                partyPref.setLanguage("");//BAIE 9 lingua interface
            }
        }
        //BAIE if Action Type = LimChng then <PartyModRq><PartyInfo><PartyPref><FinancialAmt> else  ""
        if (partyInfo.getSecObjPurpose().equals("LimChng")) {
            if (clientType.equals("Enterprise")) {
                partyPref.setFinancialAmt("");//BAIE 10 limite debito<-------------------------------------
            } else if (clientType.equals("Particular")) {
                partyPref.setFinancialAmt("");//BAIE 10 limite debito<-------------------------------------
            }
        }
        partyInfo.setCorrespondence(correspondence);
        partyInfo.setPartyPref(partyPref);

        PartyKeys partyKeys = new PartyKeys();
        LoginIdent loginIdent = new LoginIdent();

        if (clientType.equals("Enterprise")) {
            loginIdent.setLoginName("");//TODO 3 id internet banking --> exemplo é um email<-------------------------------------
        } else if (clientType.equals("Particular")) {
            loginIdent.setLoginName("");//TODO 3 id internet banking<-------------------------------------
        }
        partyKeys.setLoginIdent(loginIdent);
        partyKeys.setPartyId(jsonObject.getString("partyId")); //BAIE 4 id cliente

        partyModRq.setPartyInfo(partyInfo);
        partyModRq.setPartyKeys(partyKeys);

        return partyModRq;
    }
}
