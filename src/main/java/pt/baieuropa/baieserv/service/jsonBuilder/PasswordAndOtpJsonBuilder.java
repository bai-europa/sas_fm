package pt.baieuropa.baieserv.service.jsonBuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.IssuedIdent;
import pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.SecObjInfo;
import pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.SecObjModRq;

import java.io.IOException;

@Slf4j
public class PasswordAndOtpJsonBuilder {

    /**
     * Leitura de json com campos recebidos via nats
     * Transações de Alteração de Password OU Alteração Contacto OTP
     *
     * @param content
     * @return
     */
    public SecObjModRq createPasswordOrOtpStructure(String content) throws IOException {
        JSONObject json = new JSONObject(content);
        //BAIE criação de estrutura com pojos
        SecObjModRq secObjModRq = new SecObjModRq();
        secObjModRq.setRqUID("");//BAIE id transação
        SecObjInfo secObjInfo = new SecObjInfo();
        secObjInfo.setPrcDt("");//BAIE data e hora transação
        secObjInfo.setSecObjPurpose("");//BAIE action type
        secObjInfo.setStrongAuth("");//BAIE autenticação forte
        secObjInfo.setSecObjValue("");//BAIE detalhes contacto otp
        secObjInfo.setSecObjValue("");// //BAIE Contacto OTP ------> if Action Type = OTPChng

        pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.PartyData partyData = new pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.PartyData();
        IssuedIdent issuedIdent = new IssuedIdent();
        issuedIdent.setIssuedIdentValue("");//BAIE id internet banking
        partyData.setIssuedIdent(issuedIdent);

        pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.RefData refData = new pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP.RefData();
        refData.setRefIdent("");//BAIE id cliente

        secObjInfo.setPartyData(partyData);
        secObjInfo.setRefData(refData);

        secObjModRq.setSecObjInfo(secObjInfo);
        return secObjModRq;
    }
}
