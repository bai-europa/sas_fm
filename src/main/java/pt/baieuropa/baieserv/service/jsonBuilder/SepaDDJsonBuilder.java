package pt.baieuropa.baieserv.service.jsonBuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baidbmanager.logic.Executor;
import pt.baieuropa.baieserv.models.monetarias.*;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Slf4j
public class SepaDDJsonBuilder {

    public static Executor executor = null;


    public static void connect() {
        log.info("Establishing connection to database...");
        try {
            log.debug("Initializing the executor...");
            executor = new Executor();
            log.debug("Executor initialized SUCCESSFULLY.");

            log.debug("Loading and starting the executor...");
//            executor.loadAndStart(System.getenv("AS400.configuration"), null, null, null, "queries.yaml");
            executor.loadAndStart(System.getenv("AS400_CONFIGURATION"), null, null, null, System.getenv("QUERIES_CONFIGURATION"));
            log.debug("Executor loaded and started SUCCESSFULLY");
        } catch (Exception ex) {
            log.error("Connection Error - " + ex.getMessage());
        }

        log.info("Connection established SUCCESSFULLY.");
    }

    public static void disconnect() {
        log.debug("Closing connection...");

        executor.stop();

        log.debug("Connection closed.");
    }

    /**
     * Execute query to get parked movement information from file
     *
     * @param content
     * @return
     */
    public List<PmtAddRq> chooseParkedMovementsType(String content) throws Exception {
        JSONObject jsonObject = new JSONObject(content);
        PmtAddRq pmtAddRq = new PmtAddRq();
        List<PmtAddRq> pmtAddRqList = new ArrayList<>();
        String fileName = jsonObject.getString("fileName");

        try {
            String fileType = fileName.substring(0, 1);
            //BAIE ------>Query para retirar dados do ficheiro da BANKA
            LinkedList<Object> queryParams = new LinkedList<>();

            String queryType = "";
            if (fileType.equals("R")) {
                //BAIE ficheiros começados por R
                queryType = "PARKEDMOVEMENTS_R";
            } else if (fileType.equals("S")) {
                //BAIE ficheiros começados por S
                queryType = "PARKEDMOVEMENTS_S";
            }

            //BAIE Seleciona o inicio(Ex:RDD) do nome do file e adiciona ao parametro da query
            String ficheiro = fileName.substring(0, 3);
            queryParams.add(ficheiro);
            //BAIE Seleciona a data(2311) do nome do file e adiciona ao parametro da query
            String fileId = fileName.substring(3, 7);
            queryParams.add(fileId);
            //BAIE Executa a query
            ResultSet set = executor.getResultFromQuery("AS400", queryType, queryParams);

            if (set == null) {
                log.error("[ERROR] - QUERY NOT FOUND");
                return null;
            }

            //BAIE Corre a query
            while (set.next()) {
//                String situacao = set.getString("SDMVSITU").trim();
                //BAIE Create structure putting our data into PmtAddRq
                //TODO enviar campos necessarios para preenchimento do objecto
                //TODO verificar o que é necessario enviar
                String transactionDate = set.getString("SDMVDTLIQ").trim();
                String transactionTime = "";
//                String transactionTime = set.getString("").trim();
                String billingDate = set.getString("SDMVDTCOB").trim();
                String authorizationSigningDate = set.getString("SDMVDTASS").trim();
                String reasonTransfer = set.getString("SDMVISOME").trim();
//                String transferReasonCategory = set.getString("").trim();
//                String kindOfService = set.getString("").trim();
//                String channel = set.getString("").trim();
                String transferReasonCategory = "";
                String kindOfService = "";
                String channel = "";
//                String instrument = set.getString("").trim();
                String instrument = "";
                String debtorBankBIC = set.getString("SDMVBICBDV").trim();
//                String identificationOfLastDebtor = set.getString("").trim();
                String identificationOfLastDebtor="";
                String transactionId = set.getString("SDMVREFT").trim();
                String movementType = set.getString("SDMVTIPMOV").trim();
//                String authorizationChangeReason = set.getString("").trim();
                String authorizationChangeReason = "";
                String chargeReferenceCreditor = set.getString("SDMVREFCRE").trim();
                String originalAuthorizationID = set.getString("SDMVNADC").trim();
                String debtorBANKAEntityID = set.getString("SDMVNCLI").trim();
                String debtorName = set.getString("SDMVNOMEDV").trim();
//                String debtorCountry = set.getString("").trim();
                String debtorCountry = "";
                String debtorIBAN = set.getString("SDMVIBANDV").trim();
                String amount = set.getString("SDMVVALOR").trim();
                String currency = set.getString("SDMVMOEDA").trim();
                String creditorName = set.getString("SDMVNOMEC").trim();
//                String creditorCountry = set.getString("").trim();
                String creditorCountry = "";
                String creditorBankBIC = set.getString("SDMVBICBCR").trim();
                String creditorIBAN = set.getString("SDMVIBANCR").trim();


                pmtAddRq = createSepaDDStructure(transactionDate, transactionTime, billingDate, authorizationSigningDate,
                        reasonTransfer, transferReasonCategory, kindOfService, channel, instrument, debtorBankBIC,
                        identificationOfLastDebtor, transactionId, movementType, authorizationChangeReason,
                        chargeReferenceCreditor, originalAuthorizationID, debtorBANKAEntityID, debtorName, debtorCountry, debtorIBAN, amount,
                        currency, creditorName, creditorCountry, creditorBankBIC, creditorIBAN);

                pmtAddRqList.add(pmtAddRq);
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return pmtAddRqList;
    }

    /**
     * Execute query to get r-transaction information from file
     *
     * @param content
     * @return
     */
    public PmtAddRq chooseRTransactionType(String content) throws Exception {
        JSONObject jsonObject = new JSONObject(content);
        PmtAddRq pmtAddRq = new PmtAddRq();
        String fileName = jsonObject.getString("fileName");

        try {
            //TODO fazer queries
            //TODO fazer logica das

            //BAIE ------>Retirar dados por query (ficheiro banka)
            LinkedList<Object> queryParams = new LinkedList<>();
            queryParams.add(fileName);
            queryParams.add(fileName);
            ResultSet set = executor.getResultFromQuery("AS400", "SEPADD", queryParams);
            String queryData = set.toString();
            String operationNumber = set.getString("numeroOperacao").trim();
            String operationCode = set.getString("codigoOperacao").trim();
            String documentNumber = set.getString("numeroDocumento").trim();


            //BAIE Create structure putting our data into PmtAddRq
            //TODO enviar campos necessarios para preenchimento do objecto
            //TODO verificar o que é necessario enviar
            pmtAddRq = createSepaDDStructure("", "", "", "",
                    "", "", "", "", "", "",
                    "", "", "", "", "",
                    "", "", "", "", "", "", "",
                    "", "", "", "");

        } catch (Exception e) {
            log.error(e.getMessage());
            throw e;
        }
        return pmtAddRq;
    }

    /**
     * create sepaDD structure
     *
     * @return
     * @params from Banka
     */
    public PmtAddRq createSepaDDStructure(String transactionDate, String transactionTime, String billingDate, String authorizationSigningDate, String
            reasonTransfer, String transferReasonCategory, String kindOfService, String channel, String instrument, String debtorBankBIC, String
                                                  identificationOfLastDebtor, String transactionId, String movementType, String authorizationChangeReason, String
                                                  chargeReferenceCreditor, String originalAuthorizationID, String debtorBANKAEntityID, String debtorName, String debtorCountry, String debtorIBAN, String amount, String
                                                  currency, String creditorName, String creditorCountry, String creditorBankBIC, String creditorIBAN) {
        log.info("Create SepaDD Structure\n");

        PmtAddRq pmtAddRq = new PmtAddRq();
        pmtAddRq.setRqUID(transactionId);//BAIE Id da transacção
        PmtInfo pmtInfo = new PmtInfo();
        pmtInfo.setPrcDt(transactionDate);//BAIE Data da transacção ---> Data e hora
        pmtInfo.setDueDt(billingDate);//BAIE Data de cobrança

        PmtInstruction pmtInstruction = new PmtInstruction();
        RefData refData = new RefData();
        refData.setAuthorizationDt(authorizationSigningDate);//BAIE Data de assinatura da autorização
        refData.setServiceCd("");//TODO Não existe -->Tipo de serviço
        refData.setInstrument("");//TODO Não existe --> Instrumento
        refData.setMovementCd(movementType);//BAIE Tipo de movimento
        refData.setChangeReason(authorizationChangeReason);//BAIE Motivo para alteração da autorização
        refData.setOrgAuthorization(originalAuthorizationID);//BAIE Identificação da autorização original
        refData.setRefIdent(debtorBANKAEntityID);//BAIE ID Entidade BANKA do devedor

        pmtInstruction.setRefData(refData);
        pmtInstruction.setTransferReason(reasonTransfer);//BAIE Motivo da transferência
        pmtInstruction.setTransferReasonCd("");//TODO Não existe
        pmtInstruction.setPmtMethod("");//TODO Canal da transação --> Não existe
        pmtInstruction.setDDReference(chargeReferenceCreditor);//BAIE Referência da cobrança atribuída pelo credor

        FromAcctRef fromAcctRef = new FromAcctRef();
        AcctKeys acctKeys = new AcctKeys();
        acctKeys.setIBAN(debtorIBAN);//TODO IBAN do devedor

        fromAcctRef.setAcctKeys(acctKeys);
        pmtInstruction.setFromAcctRef(fromAcctRef);

        DebtorData debtorData = new DebtorData();
        PartyData partyData = new PartyData();
        IssuedIdent issuedIdent = new IssuedIdent();
        issuedIdent.setIssuedIdentType(debtorBankBIC);//BAIE BIC do banco do devedor

        Contact contact = new Contact();
        contact.setOrgContactName("");//TODO Não existe --> Identificação do último devedor
        contact.setContactName(debtorName);//BAIE Nome do devedor
        partyData.setContact(contact);

        GovIssuedIdent govIssuedIdent = new GovIssuedIdent();
        CountryCode countryCode = new CountryCode();
        countryCode.setCountryCodeValue(debtorCountry);//BAIE País do devedor
        govIssuedIdent.setCountryCode(countryCode);
        partyData.setIssuedIdent(issuedIdent);

        issuedIdent.setGovIssuedIdent(govIssuedIdent);
        debtorData.setPartyData(partyData);
        pmtInfo.setDebtorData(debtorData);

        CurAmt curAmt = new CurAmt();
        curAmt.setAmt(amount);//BAIE Montante a liquidar em (EUR)
        pmtInfo.setCurAmt(curAmt);

        PmtCreditDetail pmtCreditDetail = new PmtCreditDetail();
        ToAcctKeys toAcctKeys = new ToAcctKeys();
        toAcctKeys.setBIC(creditorBankBIC);//BAIE BIC do banco do credor
        toAcctKeys.setIBAN(creditorIBAN);//BAIE IBAN do credor
        pmtCreditDetail.setToAcctKeys(toAcctKeys);

        UltimateCreditorData ultimateCreditorData = new UltimateCreditorData();
        PartyData partyData1 = new PartyData();
        Contact contact1 = new Contact();
        contact1.setContactName(creditorName);//BAIE Nome do credor
        partyData1.setContact(contact1);

        IssuedIdent issuedIdent1 = new IssuedIdent();
        GovIssuedIdent govIssuedIdent1 = new GovIssuedIdent();
        CountryCode countryCode1 = new CountryCode();
        countryCode1.setCountryCodeValue(creditorCountry);//BAIE País do credor
        govIssuedIdent1.setCountryCode(countryCode1);
        issuedIdent1.setGovIssuedIdent(govIssuedIdent1);
        partyData1.setIssuedIdent(issuedIdent1);

        ultimateCreditorData.setPartyData(partyData1);
        pmtCreditDetail.setUltimateCreditorData(ultimateCreditorData);
        pmtInfo.setPmtCreditDetail(pmtCreditDetail);

        pmtInfo.setDebtorData(debtorData);
        pmtInfo.setPmtInstruction(pmtInstruction);
        pmtAddRq.setPmtInfo(pmtInfo);

        System.out.println(pmtAddRq);
        return pmtAddRq;
    }


}
