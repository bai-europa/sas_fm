package pt.baieuropa.baieserv.service.jsonBuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.CustPayeeInfo;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.CustPayeeModRq;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.DfltPmtData;
import pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.LastPmtCurAmt;

import java.io.IOException;

@Slf4j
public class BenefOrStandingOrderJsonBuilder {

    /**
     * Leitura de json com campos recebidos via nats
     * Transações de Alteração de Beneficiarios OU Alteração de Standing Olders
     *
     * @param content
     * @return
     */
    public CustPayeeModRq createBenefOrStandingOrdersStructure(String content) throws IOException {
        JSONObject json = new JSONObject(content);
        //BAIE criação de estrutura com pojos
        CustPayeeModRq custPayeeModRq = new CustPayeeModRq();
        custPayeeModRq.setRqUID("");//BAIE id da transação
        pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.PartyInfo partyInfo = new pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders.PartyInfo();
        partyInfo.setPrcDt("");//BAIE data e hora
        partyInfo.setLoginName("");//BAIE id internet banking
        partyInfo.setPartyId("");//BAIE id do cliente
        partyInfo.setSecObjPurpose("");//BAIE action type
        partyInfo.setStrongAuth("");//BAIE autenticação forte

        CustPayeeInfo custPayeeInfo = new CustPayeeInfo();
        custPayeeInfo.setLastPmtDt("");//BAIE Data de movimento------>Só para Standing orders
        custPayeeInfo.setPayeeAcctNum("");//BAIE iban beneficiario------>Só para Beneficiarios
        custPayeeInfo.setName("");//BAIE nome do beneficiario------>Só para Beneficiarios
        DfltPmtData dfltPmtData = new DfltPmtData();
        dfltPmtData.setCategory(""); //BAIE 2 tipo de movimento------>Só para Standing orders
        LastPmtCurAmt lastPmtCurAmt = new LastPmtCurAmt();
        lastPmtCurAmt.setAmt(""); //BAIE valor de movimento------>Só para Standing orders
        custPayeeInfo.setDfltPmtData(dfltPmtData);
        custPayeeInfo.setLastPmtCurAmt(lastPmtCurAmt);

        custPayeeModRq.setPartyInfo(partyInfo);
        custPayeeModRq.setCustPayeeInfo(custPayeeInfo);

        return custPayeeModRq;
    }
}
