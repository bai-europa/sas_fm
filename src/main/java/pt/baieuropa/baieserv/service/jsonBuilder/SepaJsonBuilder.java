package pt.baieuropa.baieserv.service.jsonBuilder;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.monetarias.*;
import java.io.IOException;
import java.text.ParseException;

import static pt.baieuropa.baieserv.ServiceApp.partiesURL;
import static pt.baieuropa.baieserv.ServiceApp.relationsURL;

@Slf4j
public class SepaJsonBuilder {


    //BAIE------------------------------------------------TRANSFERÊNCIAS SEPA-------------------------------------------

    /**
     * Transações Sepa
     * Chamada de serviços para recolha e leitura de informação relativa às entidades correspondentes a um cliente
     * @param content
     * @param transactionDirection
     * @returnOPC com erro
     */
    public PmtAddRq chooseSepaType(String content, String transactionDirection)throws Exception {
        JSONObject transactionJsonObject = new JSONObject(content);
        PmtAddRq pmtAddRq = new PmtAddRq();
        try {
            log.debug("Join transaction info");
            //BAIE----------------INVOCA SERVIÇO RELATIONS PARA SABER QUAL O NUMERO DE ENTIDADE---------------------------
            Response responseRelations = null;
            OkHttpClient clientRelations = new OkHttpClient();
            String urlRelations = relationsURL;

            //BAIE ClientNumber_Ext--> número de cliente
            //BAIE relationType--> primeira entidade do cliente
            urlRelations = urlRelations + "partyAcctRelId=" + transactionJsonObject.getString("ClientNumber_Ext") + "&relationType=01";
            log.debug("Calling WebService " + urlRelations);
            Request requestRelations = new Request.Builder()
                    .url(urlRelations)
                    .build();

            responseRelations = clientRelations.newCall(requestRelations).execute();
            JSONArray responseBodyRelations = new JSONArray(responseRelations.body().string());
            JSONObject objectJsonRelations = responseBodyRelations.getJSONObject(0);
            JSONObject partyAcctRelInfo = (JSONObject) objectJsonRelations.getJSONArray("partyAcctRelInfo").get(0);
            log.debug("Request sent successfully!");

            //BAIE-------------------INVOCA SERVIÇO PARA RECEBER A ESTRUTURA PARTIES A PARTIR DO NUMERO DE ENTIDADE-----------
            Response response = null;
            OkHttpClient client = new OkHttpClient();
            String url = partiesURL;
            String entityNumber = partyAcctRelInfo.getJSONObject("partyRef").getJSONObject("partyKeys").getString("partyId");
            url = url + "/" + entityNumber;

            log.debug("Calling WebService " + url);
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            response = client.newCall(request).execute();
            log.debug("Request sent successfully!");

            JSONArray responseBody = new JSONArray(response.body().string());

            JSONObject partyJsonObject = responseBody.getJSONObject(0);
            log.debug("Party json: "+partyJsonObject);

            // BAIE - ENVIADA OU RECEBIDA
            // ORDENADOR / BENEFICIARIO
            // NOME
            // MORADA
            // IBAN/CONTA
            UserTransactionData ordenador = null;
            UserTransactionData beneficiario = null;
            if (transactionDirection.equals("received")) {

                //BAIE o nosso party é o BENEFICIARIO
                ordenador = new UserTransactionData();

                ordenador.setName(transactionJsonObject.getString("CounterpartName"));
                ordenador.setAccount(transactionJsonObject.getString("CounterpartAccountNumber"));


                beneficiario = new UserTransactionData();

                if (partyJsonObject.has("orgPartyInfo")) {
                    JSONArray contactsJson0 = partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONArray("contacts");

                    for (int i = 0;i < contactsJson0.length();i++){
                        try{
                            if(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                                beneficiario.setAddress(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                        } catch (Exception ex){
                            // TODO nothing to do
                        }
                    }

                    beneficiario.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                    beneficiario.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                    beneficiario.setName(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONObject("orgName").getString("name"));
                    beneficiario.setCountry(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("residenceCountry").getString("countryCodeValue").trim());
                    beneficiario.setBeneficiarioType("E");
                } else {
                    JSONArray contactsJson0 = partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONArray("contacts");

                    for (int i = 0;i < contactsJson0.length();i++){
                        try{
                            if(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                                beneficiario.setAddress(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                        } catch (Exception ex){
                            // TODO nothing to do
                        }
                    }

                    beneficiario.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                    beneficiario.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                    beneficiario.setName(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONObject("personName").getString("fullName"));
                    beneficiario.setCountry(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("residenceCountry").getString("countryCodeSource").trim());
                    beneficiario.setBeneficiarioType("P");
                }


            } else if (transactionDirection.equals("creditsent") || transactionDirection.equals("debitsent")) {
                // BAIE o nosso party é o ORDENADOR
                ordenador = new UserTransactionData();

                if (partyJsonObject.has("orgPartyInfo")) {
                    JSONArray contactsJson0 =  partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONArray("contacts");

                    for (int i = 0;i < contactsJson0.length();i++){
                        try{
                            if(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                                ordenador.setAddress(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                        } catch (Exception ex){
                            // TODO nothing to do
                        }
                    }

                    ordenador.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                    ordenador.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                    ordenador.setName(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONObject("orgName").getString("name"));
                    ordenador.setCountry(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("residenceCountry").getString("countryCodeValue").trim());
                } else {
                    JSONArray contactsJson0 = partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONArray("contacts");

                    for (int i = 0;i < contactsJson0.length();i++){
                        try{
                            if(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                                ordenador.setAddress(contactsJson0.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                        } catch (Exception ex){
                            // TODO nothing to do
                        }
                    }

                    ordenador.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                    ordenador.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                    ordenador.setName(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONObject("personName").getString("fullName"));
                    ordenador.setCountry(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("residenceCountry").getString("countryCodeValue").trim());
                }

                beneficiario = new UserTransactionData();
                beneficiario.setName(transactionJsonObject.getString("CounterpartName"));
                beneficiario.setAccount(transactionJsonObject.getString("CounterpartAccountNumber"));


            } else if (transactionDirection.equals("DD")) {
                //TODO FAZER LOGICA PARA DEBITOS DIRETOS
//                log.debug("------EM DESENVOLVIMENTO-------");
            } else
                ;

            pmtAddRq = createSepaStructure(ordenador, beneficiario, transactionJsonObject);


        } catch (Exception ex) {
            ex.getMessage();
            throw ex;
        }
        return pmtAddRq;
    }


    /**
     * Transações Sepa
     * Leitura de json com campos da banka de transações SEPA (Digital,Manual e cartões)
     *
     * @param
     * @return
     */
    public PmtAddRq createSepaStructure(UserTransactionData ordenador, UserTransactionData beneficiario, JSONObject transactionJsonObject) throws IOException, ParseException {
        log.info("Create Sepa Structure\n");

        //BAIE criação de estrutura com pojos
        PmtAddRq pmtAddRq = new PmtAddRq();
        PmtInfo pmtInfo = new PmtInfo();
        pmtInfo.setPrcDt(transactionJsonObject.getString("TransactionDateTime"));

        PmtAddRq pmtAddRq1 = new PmtAddRq();
        pmtAddRq1.setRqUID(transactionJsonObject.getString("TransactionDocumentNumber"));//BAIE NºTRANSFERENCIA
        pmtInfo.setPmtAddRq(pmtAddRq1);

        PmtInstruction pmtInstruction = new PmtInstruction();
        pmtInstruction.setTPPId("");//BAIE <-----------------------------------------------------TPP
        pmtInstruction.setPmtType(transactionJsonObject.getString("TransactionType"));//BAIE TIPO TRANSFERENCIA
        pmtInstruction.setDueDt(transactionJsonObject.getString("MovementValueDate"));//BAIE DATA ENVIO
        if(!transactionJsonObject.getString("Channel").equals("BALCOES")) {
            pmtInstruction.setPmtMethod(transactionJsonObject.getString("Channel").substring(0, 8));//BAIE canal
        }else{
            pmtInstruction.setPmtMethod(transactionJsonObject.getString("Channel"));
        }
        pmtInstruction.setMemo(transactionJsonObject.getString("TransactionDescription"));//BAIE Descritivo da transação<-----------------------------------------------------
        if(transactionJsonObject.getString("Channel").trim().equals("INTERNET")) {
            pmtInstruction.setStrongAuth("P");//BAIE Indicador de autenticação forte
        }else{
            pmtInstruction.setStrongAuth("");//BAIE Indicador de autenticação forte
        }

        RefData refData = new RefData();
        refData.setRefIdent(ordenador.getClientNumber());//BAIE NUMERO CLIENTE ORDENANTE
        pmtInstruction.setRefData(refData);

        FromAcctRef fromAcctRef = new FromAcctRef();
        AcctKeys acctKeys = new AcctKeys();
        acctKeys.setAcctId(ordenador.getAccount());//BAIE CONTA ORDENANTE
        acctKeys.setIBAN(transactionJsonObject.getString("AccountIBAN"));//TODO IBAN
        acctKeys.setAcctId(transactionJsonObject.getString("TransactionNumber"));//BAIE Id da operação do cliente (ordenante)<-------------------------------
        fromAcctRef.setAcctKeys(acctKeys);

        AcctInfo acctInfo = new AcctInfo();
        TaxCountry taxCountry = new TaxCountry();
        taxCountry.setCountryCodeValue(ordenador.getCountry());//BAIE País do cliente ordenante
        acctInfo.setTaxCountry(taxCountry);
        acctInfo.setOpenDt(transactionJsonObject.getString("MovementLaunchDateTime"));//BAIE Data de abertura da operação do cliente (ordenante)<-------------------------------
        acctInfo.setMaturityDt(transactionJsonObject.getString("MovementLaunchDateTime"));//BAIE Data de maturidade da operação do cliente (ordenante)<-------------------------------
        fromAcctRef.setAcctInfo(acctInfo);
        pmtInstruction.setFromAcctRef(fromAcctRef);

        pmtInfo.setPmtInstruction(pmtInstruction);

        CurAmt curAmt = new CurAmt();
        curAmt.setCurCode(transactionJsonObject.getString("TransactionCurrency_Original"));//BAIE MOEDA<----------------------------CORRIGIR
        curAmt.setAmt(transactionJsonObject.getBigDecimal("TransactionAmount_Original").toString());//BAIE VALOR TRANSFERENCIA
        pmtInfo.setCurAmt(curAmt);

        PmtCreditDetail pmtCreditDetail = new PmtCreditDetail();
        ToAcctKeys toAcctKeys = new ToAcctKeys();
        toAcctKeys.setIBAN(transactionJsonObject.getString("CounterpartAccountNumber"));//BAIE IBAN BENEFICIARIO
        toAcctKeys.setBIC(transactionJsonObject.getString("CounterpartBankCode"));//BAIE BIC BENEFICIARIO
        toAcctKeys.setFIIdent(transactionJsonObject.getString("CounterpartBankCode"));//BAIE Banco do destinatário<-----------------------------------------Nome do banco destinatário-->Fica o BIC
        toAcctKeys.setAcctId(beneficiario.getAccount());//BAIE Id da conta do beneficiário
        pmtCreditDetail.setToAcctKeys(toAcctKeys);

        UltimateCreditorData ultimateCreditorData = new UltimateCreditorData();
        PartyData partyData1 = new PartyData();
        Contact contact1 = new Contact();
        contact1.setContactName(beneficiario.getName());//BAIE Nome do beneficiario


        Locator locator = new Locator();
        PostAddr postAddr = new PostAddr();
        postAddr.setAddr1(beneficiario.getAddress());//BAIE Morada destinatário
        locator.setPostAddr(postAddr);
        contact1.setLocator(locator);
        partyData1.setContact(contact1);

        pt.baieuropa.baieserv.models.monetarias.IssuedIdent issuedIdent = new pt.baieuropa.baieserv.models.monetarias.IssuedIdent();
        issuedIdent.setIssuedIdentValue(beneficiario.getBeneficiarioType());//TODO Tipo de beneficiario
        GovIssuedIdent govIssuedIdent = new GovIssuedIdent();
        CountryCode countryCode = new CountryCode();
        countryCode.setCountryCodeValue(beneficiario.getCountry());//BAIE codigo iso país destinatario
        govIssuedIdent.setCountryCode(countryCode);
        issuedIdent.setGovIssuedIdent(govIssuedIdent);
        partyData1.setIssuedIdent(issuedIdent);

        ultimateCreditorData.setPartyData(partyData1);
        pmtCreditDetail.setUltimateCreditorData(ultimateCreditorData);
        pmtInfo.setPmtCreditDetail(pmtCreditDetail);

        DebtorData debtorData = new DebtorData();
        PartyData partyData = new PartyData();
        Contact contact = new Contact();
        contact.setContactName(ordenador.getName());//BAIE NOME ORDENANTE
        partyData.setContact(contact);

        pt.baieuropa.baieserv.models.monetarias.IssuedIdent issuedIdent1 = new pt.baieuropa.baieserv.models.monetarias.IssuedIdent();
        issuedIdent1.setBirthDt(ordenador.getBirthDt());//BAIE Data de nascimento do cliente ordenante
//        issuedIdent1.setIssuedIdentValue("baie8@gmail.com");//BAIE Utilizador Internet Banking --> so se preenche caso seja digital internet banking--> RequestUser
        issuedIdent1.setIssuedIdentValue(transactionJsonObject.getString("RequestUser"));
        partyData.setIssuedIdent(issuedIdent1);
        debtorData.setPartyData(partyData);
        pmtInfo.setDebtorData(debtorData);

        pmtAddRq.setPmtInfo(pmtInfo);

        return pmtAddRq;
    }

}
