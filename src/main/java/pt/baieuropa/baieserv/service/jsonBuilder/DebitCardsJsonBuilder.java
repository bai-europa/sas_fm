package pt.baieuropa.baieserv.service.jsonBuilder;


import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.monetarias.*;
import java.io.IOException;

import static pt.baieuropa.baieserv.ServiceApp.partiesURL;
import static pt.baieuropa.baieserv.ServiceApp.relationsURL;

@Slf4j
public class DebitCardsJsonBuilder {

    //BAIE-------------------------------------------------------CARTOES DE DEBITO---------------------------------------------------------------------------------------------

    /**
     * Chamada de serviços para recolha e leitura de informação relativa ás entidades correspondentes a um cliente
     *
     * @param content
     * @return
     */
    public PmtAddRq chooseDebitCards(String content)throws IOException {
        JSONObject transactionJsonObject = new JSONObject(content);
        PmtAddRq pmtAddRq = new PmtAddRq();
        try {
            log.debug("Search transaction info");
            //BAIE----------------INVOCA SERVIÇO RELATIONS PARA SABER QUAL O NUMERO DE ENTIDADE---------------------------
            Response responseRelations = null;
            OkHttpClient clientRelations = new OkHttpClient();
            String urlRelations = relationsURL;

            //BAIE ClientNumber_Ext--> número de cliente
            //BAIE relationType--> primeira entidade do cliente
            urlRelations = urlRelations + "partyAcctRelId=" + transactionJsonObject.getString("ClientNumber_Ext") + "&relationType=01";
            log.debug("Calling WebService " + urlRelations);
            Request requestRelations = new Request.Builder()
                    .url(urlRelations)
                    .build();

            responseRelations = clientRelations.newCall(requestRelations).execute();
            JSONArray responseBodyRelations = new JSONArray(responseRelations.body().string());
            JSONObject objectJsonRelations = responseBodyRelations.getJSONObject(0);
            JSONObject partyAcctRelInfo = (JSONObject) objectJsonRelations.getJSONArray("partyAcctRelInfo").get(0);
            log.debug("Request sent successfully!");

            //BAIE-------------------INVOCA SERVIÇO PARA RECEBER A ESTRUTURA PARTIES A PARTIR DO NUMERO DE ENTIDADE-----------
            Response response = null;
            OkHttpClient client = new OkHttpClient();
            String url = partiesURL;
            String entityNumber = partyAcctRelInfo.getJSONObject("partyRef").getJSONObject("partyKeys").getString("partyId");
            url = url + "/" + entityNumber;

            log.debug("Calling WebService " + url);
            Request request = new Request.Builder()
                    .url(url)
                    .build();

            response = client.newCall(request).execute();
            log.debug("Request sent successfully!");

            JSONArray responseBody = new JSONArray(response.body().string());

            JSONObject partyJsonObject = responseBody.getJSONObject(0);
            log.debug("Party json: " + partyJsonObject);


            // BAIE - ENVIADA OU RECEBIDA
            UserTransactionData ordenador = null;
            UserTransactionData beneficiario = null;

            // BAIE o nosso party é o BENEFICIARIO

            ordenador = new UserTransactionData();

            ordenador.setName(transactionJsonObject.getString("CounterpartName"));
            ordenador.setAccount(transactionJsonObject.getString("CounterpartAccountNumber"));


            beneficiario = new UserTransactionData();

            if (partyJsonObject.has("orgPartyInfo")) {
                JSONArray temp = partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONArray("contacts");

                for (int i = 0;i < temp.length();i++){
                    try{
                        if(temp.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                            beneficiario.setAddress(temp.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                    } catch (Exception ex){
                        // TODO nothing to do
                    }
                }

                beneficiario.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                beneficiario.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                beneficiario.setName(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("orgData").getJSONObject("orgName").getString("name"));
                beneficiario.setCountry(partyJsonObject.getJSONObject("orgPartyInfo").getJSONObject("residenceCountry").getString("countryCodeValue"));
                beneficiario.setBeneficiarioType("E");
            } else {
                JSONArray temp = partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONArray("contacts");

                for (int i = 0;i < temp.length();i++){
                    try{
                        if(temp.getJSONObject(i).getJSONObject("locator").getString("addrType").equals("ENTIDADE"))
                            beneficiario.setAddress(temp.getJSONObject(i).getJSONObject("locator").getString("addr1"));
                    } catch (Exception ex){
                        // TODO nothing to do
                    }
                }

                beneficiario.setClientNumber(transactionJsonObject.getString("ClientNumber_Ext"));
                beneficiario.setAccount(transactionJsonObject.getString("AccountNumber_Ext"));
                beneficiario.setName(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("personData").getJSONObject("personName").getString("fullName"));
                beneficiario.setCountry(partyJsonObject.getJSONObject("personPartyInfo").getJSONObject("residenceCountry").getString("countryCodeSource"));
                beneficiario.setBeneficiarioType("P");
            }
            pmtAddRq = createDebitCardsStructure(ordenador, beneficiario, transactionJsonObject);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
        return pmtAddRq;
    }

    /**
     * Leitura de json com campos da banka de transações de cartoes de debito
     *
     * @return
     */
    public PmtAddRq createDebitCardsStructure(UserTransactionData ordenador, UserTransactionData beneficiario, JSONObject transactionJsonObject)throws IOException {
        log.info("Create DebitCard Structure\n");

        //BAIE criação de estrutura com pojos
        PmtAddRq pmtAddRq = new PmtAddRq();
        PmtInfo pmtInfo = new PmtInfo();
        pmtInfo.setPrcDt(transactionJsonObject.getString("TransactionDateTime"));//BAIE Data da transação

        PmtAddRq pmtAddRq1 = new PmtAddRq();
        pmtAddRq1.setRqUID(transactionJsonObject.getString("TransactionNumber"));//BAIE NºTRANSFERENCIA
        pmtInfo.setPmtAddRq(pmtAddRq1);

        PmtInstruction pmtInstruction = new PmtInstruction();
        pmtInstruction.setStrongAuth("");//BAIE Indicador de autenticação forte
        pmtInstruction.setDueDt(transactionJsonObject.getString("MovementValueDate"));//BAIE Data valor da transacção

        if(!transactionJsonObject.getString("Channel").equals("BALCOES")) {
            pmtInstruction.setPmtMethod(transactionJsonObject.getString("Channel").substring(0, 8));//BAIE canal da transação
        }else{
            pmtInstruction.setPmtMethod(transactionJsonObject.getString("Channel"));
        }
        pmtInstruction.setPmtType(transactionJsonObject.getString("TransactionType"));//BAIE tipo de transferencia
        pmtInstruction.setTPPId("");//BAIE id de TPP<-------------------------------
        pmtInstruction.setMemo("DebitCard");//BAIE Descritivo da transação<-------------------------------

        RefData refData = new RefData();
        refData.setRefIdent(ordenador.getClientNumber());//BAIE Id cliente da banka (ordenante)

        DebtorData debtorData = new DebtorData();
        PartyData partyData = new PartyData();
        Contact contact = new Contact();
        contact.setContactName(ordenador.getName());//BAIE Nome do client(ordenante)<-------------------------------
        partyData.setContact(contact);

        pt.baieuropa.baieserv.models.monetarias.IssuedIdent issuedIdent = new pt.baieuropa.baieserv.models.monetarias.IssuedIdent();
        issuedIdent.setBirthDt(ordenador.getBirthDt());//BAIE Data de nascimento do cliente (ordenante)<-------------------------------
        issuedIdent.setIssuedIdentValue(transactionJsonObject.getString("RequestUser"));//BAIE Utilizador Internet Banking<-------------------------------
        partyData.setIssuedIdent(issuedIdent);

        debtorData.setPartyData(partyData);

        FromAcctRef fromAcctRef = new FromAcctRef();
        AcctInfo acctInfo = new AcctInfo();
        acctInfo.setOpenDt(transactionJsonObject.getString("MovementLaunchDateTime"));//BAIE Data de abertura da operação do cliente (ordenante)<-------------------------------
        acctInfo.setMaturityDt(transactionJsonObject.getString("MovementLaunchDateTime"));//BAIE Data de maturidade da operação do cliente (ordenante)<-------------------------------

        TaxCountry taxCountry = new TaxCountry();
        taxCountry.setCountryCodeValue(ordenador.getCountry());//BAIE Pais do cliente ordenante<-------------------------------
        acctInfo.setTaxCountry(taxCountry);

        fromAcctRef.setAcctInfo(acctInfo);

        AcctKeys acctKeys = new AcctKeys();
        acctKeys.setIBAN(transactionJsonObject.getString("AccountIBAN"));//BAIE IBAN da conta do cliente (ordenante)<-------------------------------
        acctKeys.setAcctId(transactionJsonObject.getString("TransactionOperationCode"));//BAIE Id da operação do cliente (ordenante)<-------------------------------
        fromAcctRef.setAcctKeys(acctKeys);

        CurAmt curAmt = new CurAmt();
        curAmt.setAmt(transactionJsonObject.getBigDecimal("TransactionAmount_Converted").toString());//BAIE Montante da transacção em EUR
        curAmt.setCurCode(transactionJsonObject.getString("TransactionCurrency_Original"));//BAIE Moeda da transacção

        PmtCreditDetail pmtCreditDetail = new PmtCreditDetail();
        UltimateCreditorData ultimateCreditorData = new UltimateCreditorData();
        PartyData partyData1 = new PartyData();
        pt.baieuropa.baieserv.models.monetarias.IssuedIdent issuedIdent1 = new pt.baieuropa.baieserv.models.monetarias.IssuedIdent();
        issuedIdent1.setIssuedIdentValue(beneficiario.getBeneficiarioType());//BAIE tipo de beneficiario<-------------------------------
        GovIssuedIdent govIssuedIdent = new GovIssuedIdent();
        CountryCode countryCode = new CountryCode();
        countryCode.setCountryCodeValue(transactionJsonObject.getString("CounterpartCountryCode"));//BAIE Pais do beneficiario

        govIssuedIdent.setCountryCode(countryCode);
        issuedIdent1.setGovIssuedIdent(govIssuedIdent);
        partyData1.setIssuedIdent(issuedIdent1);

        ToAcctKeys toAcctKeys = new ToAcctKeys();
        toAcctKeys.setIBAN(transactionJsonObject.getString("AccountIBAN"));//BAIE iban do beneficiario
        toAcctKeys.setBIC(transactionJsonObject.getString("CounterpartBankCode"));//BAIE BIC do beneficiario
        toAcctKeys.setFIIdent(transactionJsonObject.getString("CounterpartBankCode"));//BAIE Nome do banco do beneficiário<-------------------------------
        toAcctKeys.setAcctId(beneficiario.getAccount());//BAIE Id da conta do beneficiário<-------------------------------

        Contact contact1 = new Contact();
        contact1.setContactName(beneficiario.getName());//BAIE Nome de beneficiario

        Locator locator = new Locator();
        PostAddr postAddr = new PostAddr();
        postAddr.setAddr1(beneficiario.getAddress());//BAIE morada do beneficiario<-------------------------------
        locator.setPostAddr(postAddr);
        contact1.setLocator(locator);
        partyData1.setContact(contact1);

        ultimateCreditorData.setPartyData(partyData1);
        pmtCreditDetail.setUltimateCreditorData(ultimateCreditorData);
        pmtCreditDetail.setToAcctKeys(toAcctKeys);

        pmtInstruction.setFromAcctRef(fromAcctRef);
        pmtInstruction.setRefData(refData);

        pmtInfo.setPmtCreditDetail(pmtCreditDetail);
        pmtInfo.setCurAmt(curAmt);
        pmtInfo.setDebtorData(debtorData);
        pmtInfo.setPmtInstruction(pmtInstruction);
        pmtAddRq.setPmtInfo(pmtInfo);

        return pmtAddRq;
    }
}
