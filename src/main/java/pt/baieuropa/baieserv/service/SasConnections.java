package pt.baieuropa.baieserv.service;

import com.sas.finance.fraud.transaction.*;
import com.sas.finance.fraud.transaction.field.Field;
import com.sas.finance.fraud.transaction.util.OutputStreamAdapter;
import org.apache.log4j.Logger;
import pt.baieuropa.baieserv.exceptions.MissingParametersException;
import pt.baieuropa.baieserv.exceptions.SasConnectionException;
import pt.baieuropa.baieserv.utils.ConfigUtils;

import javax.net.ssl.*;
import java.io.*;
import java.net.Socket;
import java.security.KeyStore;
import java.util.Properties;


//BAIE --------------------- ALL CONFIGURATIONS AND CONNECTIONS ABOUT SAS --------------------------------

public class SasConnections {
    // BAIE Init logger
    private static final Logger logger = Logger.getLogger(SasConnections.class);

    // BAIE Properties
    public static Properties sasConfig = ConfigUtils.loadProps(System.getenv("SAS_CONFIGURATION"));
    public static int odePort = Integer.parseInt(sasConfig.getProperty("odePort"));
    public static String odeHost = sasConfig.getProperty("odeHost");
    public static String useSSL;

    // BAIE Socket and streams
    public static Socket socket;
    public static InputStream inputStream;
    public static OutputStream outputStream = null;
    public static OutputStreamAdapter outputStreamAdapter;

    /**
     * Constructor
     * Socket connect
     * Create streams to socket
     */
    public SasConnections() {
    }

    public boolean connectToSas() throws SasConnectionException {
        try {
            if (new Boolean(useSSL))
                connectSSL();
            else {
                logger.debug("Connect to sockets\n");
                connect();
            }
            logger.debug("Create streams\n");
            createStreams();
            return true;
        } catch (Exception e) {
            throw new SasConnectionException("Sas connection error\n");
        }
    }

    /**
     * List of fields to parse from reply
     */
    public static final String[] respFields = {"smh_rtn_code", "smh_reason_code", "rrr_prelim_dec_code", "rrr_prelim_dec_info", "smh_info1_code", "smh_info2_code", "rrr_sys_rtn_code","rrr_action_code"};

    public static void connect() throws Exception {
//        socket = new Socket(odeHost, Integer.parseInt(odePort));
        socket = new Socket(odeHost, odePort);
        logger.debug("Socket created.");
    }

    public static void connectSSL() throws Exception {
        char[] pass = System.getProperty("javax.net.ssl.keyStorePassword").toCharArray();

        KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new FileInputStream(System.getProperty("javax.net.ssl.keyStore")), pass);
        logger.debug("Keystore loaded.");

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, pass);
        logger.debug("KeyManager created.");

        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);
        logger.debug("TrustManager created.");

        SSLContext sc = SSLContext.getInstance("TLS");
        sc.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
        logger.debug("Context created.");

        SSLSocketFactory ssf = sc.getSocketFactory();
//        SSLSocket sslsocket = (SSLSocket) ssf.createSocket(odeHost, Integer.parseInt(odePort));
        SSLSocket sslsocket = (SSLSocket) ssf.createSocket(odeHost, odePort);
        socket = sslsocket;
        logger.debug("Socket created.");

        sslsocket.startHandshake();
        logger.debug("Handshake done.");
    }

    public static void createStreams() throws Exception {
        inputStream = new BufferedInputStream(socket.getInputStream());
        outputStream = new BufferedOutputStream(socket.getOutputStream());
        outputStreamAdapter = new OutputStreamAdapter(outputStream);
        logger.debug("Streams opened.");
    }

    public static void closeStreams() throws Exception {
        inputStream.close();
        outputStream.close();
        socket.close();
        logger.debug("Streams closed.");
    }

    private static final Field.Visitor initializer = new Field.Visitor() {
        @Override
        public void visitField(Field field, Transaction txn) {
            field.initialize(txn);
        }
    };

    public static void send(LengthCodec lc, Transaction txn) throws Exception {
        logger.debug("Send socket request\n");
        lc.writeLength(outputStream, txn.getLength());

        for (SegmentType type : SegmentType.values()) {
            byte[] bytes = txn.getSegment(type);
            if (bytes != null)
                outputStream.write(bytes, 0, bytes.length);
        }
        outputStream.flush();
        logger.debug("Send socket finished\n");
    }

    public static Transaction recv(LengthCodec lc, MessageApiEncoding apie) throws Exception {
        Transaction txn = new Transaction(apie);
        int txnLen = lc.readLength(inputStream);

        do {
            Segment seg = apie.peekSegment(inputStream, 0);
            int segLen = seg.getLength(inputStream, apie.encoding);
            txn.updateSegment(inputStream, seg, segLen);
            txnLen -= segLen;
        }
        while (txnLen > 0);

        return txn;
    }
}
