package pt.baieuropa.baieserv.configurations;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ProcessFraudLog4jPropertiesConfiguration {
    private final static Logger logger = Logger.getLogger(ProcessFraudLog4jPropertiesConfiguration.class);
    public void configure()
    {
        // BAIE - PropertiesConfigurator is used to configure logger from properties file
        PropertyConfigurator.configure(System.getenv("LOG4J_CONFIGURATION"));
        logger.debug("Log4j appender configuration for ProcessFraudWorkflow successful.");
    }
}
