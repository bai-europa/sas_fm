package pt.baieuropa.baieserv.processor.subscribers;

import org.json.JSONObject;
import pt.baieuropa.nats.Runner;

import java.nio.charset.StandardCharsets;

public class SubscriberAuthorizeDD {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SubscriberAuthorizeDD.class.getName());

    public Runner subscribe(String action, String type) {
        return new Runner() {
            @Override
            public boolean run(byte[] input) {
                try {
                    String s = new String(input, StandardCharsets.UTF_8);
                    JSONObject jsonObject = new JSONObject(s);
                    log.debug("\nRead from nats: " + s + "\n");
                    switch (action) {
                        case "AuthorizeDD":
                            log.info("Autorização de débito directo!\n\n");
                            log.debug("Content: " + s + "\n");
                            //TODO
                            break;
                    }
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                    return false;
                }
            }
        };
    }
}
