package pt.baieuropa.baieserv.processor.subscribers;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.service.controllers.AccountDetailsController;
import pt.baieuropa.nats.Runner;

import java.nio.charset.StandardCharsets;

public class SubscriberClients {

    private static final Logger log = Logger.getLogger(SubscriberClients.class);

    public static AccountDetailsController accountDetailsController = new AccountDetailsController();

    public Runner subscribe(String action) {
        return new Runner() {
            @Override
            public boolean run(byte[] input) {
                try {
                    String s = new String(input, StandardCharsets.UTF_8);
                    JSONObject jsonObject = new JSONObject(s);
                    log.debug("\nRead from nats: " + s + "\n");
                    boolean result;
                    switch (action) {
                        case "EnterpriseClient":
                            log.info("Type : EnterpriseClient\n");
                            result = accountDetailsController.changeAccountDetails(s, "Enterprise");
                            return result;
                        case "ParticularClient":
                            log.info("Type : ParticularClient\n");
                            result = accountDetailsController.changeAccountDetails(s, "Particular");
                            return result;
                    }
                    return true;
                } catch (Exception e) {
                    log.error(e.getMessage());
                    return false;
                }
            }
        };
    }
}
