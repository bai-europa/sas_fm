package pt.baieuropa.baieserv.processor.subscribers;


import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.service.controllers.DebitCardController;
import pt.baieuropa.baieserv.service.controllers.InternalController;
import pt.baieuropa.baieserv.service.controllers.SepaController;
import pt.baieuropa.nats.Runner;

import java.nio.charset.StandardCharsets;

@Slf4j
public class SubscriberMonetarias {

    public static InternalController internalController = new InternalController();
    public static SepaController sepaController = new SepaController();
    public static DebitCardController debitCardController = new DebitCardController();

    public Runner subscribe(String action, String type) {
        return new Runner() {
            @Override
            public boolean run(byte[] input) {
                try {
                    String s = new String(input, StandardCharsets.UTF_8);
                    JSONObject jsonObject = new JSONObject(s);
                    log.debug("\nRead from nats: "+s+"\n");
                    String channel = jsonObject.getString("Channel").trim();
                    boolean result;
                    switch (action) {
                        case "Internal":
                            if (channel.equals("SUPERVISAO")) {
                                log.debug("INTERNAL:SUPERVISAO\n");
                                log.debug("Internal Type: "+type+"\n");
                                result = internalController.createManualInternal(s, type);//TODO balcoes MANUAL
                                return result;
                            } else if (channel.equals("INTERNET")) {
                                log.debug("INTERNAL:INTERNET\n");
                                log.debug("Internal Type: "+type+"\n");
                                result = internalController.createDigitalInternal(s, type);
                                return result;
                            } else if (channel.equals("BALCOES")) {
                                log.debug("INTERNAL:BALCOES\n");
                                log.debug("Internal Type: "+type+"\n");
                                result = internalController.createManualInternal(s, type);
                                return result;
                            }
                            break;
                        case "Sepa":
                            if (channel.equals("SUPERVISAO")) {
                                log.debug("SEPA:SUPERVISAO\n");
                                log.debug("SEPA Type: "+type+"\n");
                                result = sepaController.createManualSepa(s, type);//TODO MANUAL
                                return result ;
                            } else if (channel.equals("INTERNET")) {
                                log.debug("SEPA:INTERNET\n");
                                log.debug("SEPA Type: "+type+"\n");
                                result = sepaController.createDigitalSepa(s,type);
                                return result ;
                            } else if (channel.equals("BALCOES")) {
                                log.debug("SEPA:BALCOES\n");
                                result = sepaController.createManualSepa(s,type);
                                log.debug("SEPA Type: "+type+"\n");
                                return result;
                            }
                            break;
                        case "SepaDD":
                            if (channel.equals("SUPERVISAO")) {
                                log.debug("SEPADD:SUPERVISAO\n");
                                log.debug("SEPADD Type: "+type+"\n");
                                result = sepaController.createManualSepa(s,type);//TODO MANUAL
                                return result ;
                            } else if (channel.equals("INTERNET")) {
                                log.debug("SEPADD:INTERNET\n");
                                log.debug("SEPADD Type: "+type+"\n");
                                result = sepaController.createDigitalSepa(s,type);
                                return result;
                            } else if (channel.equals("BALCOES")) {
                                log.debug("SEPADD:BALCOES\n");
                                log.debug("SEPADD Type: "+type+"\n");
                                result = sepaController.createManualSepa(s,type);
                                return result;
                            }
                            break;
                        case "MonetariasCartoes":
                            result = debitCardController.createDebitCard(s);
                            return result;
                        case "IntructionDD":
                            //BAIE type: IDD
                            log.debug("Intrução de débito directo!\n\n");
                            //TODO
                            break;
                    }
                    return true;
                }catch(Exception e){
                    e.printStackTrace();
                    return false;
                }
            }
        };
    }
}
