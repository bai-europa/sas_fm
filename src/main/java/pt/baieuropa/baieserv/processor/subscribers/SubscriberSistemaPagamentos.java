package pt.baieuropa.baieserv.processor.subscribers;

import org.json.JSONObject;
import pt.baieuropa.baieserv.service.controllers.SistemaPagamentosController;
import pt.baieuropa.nats.Runner;

import java.nio.charset.StandardCharsets;

public class SubscriberSistemaPagamentos {

    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(SubscriberSistemaPagamentos.class.getName());
    public static SistemaPagamentosController sistemaPagamentosController = new SistemaPagamentosController();


    public Runner subscribe(String action) {
        return new Runner() {
            @Override
            public boolean run(byte[] input) {
                try {
                    String s = new String(input, StandardCharsets.UTF_8);
                    JSONObject jsonObject = new JSONObject(s);
                    log.debug("\nRead from nats: " + s + "\n");
                    boolean result;
                    switch (action) {
                        case "OPE":
                            log.info("OPE Type\n");
                            result = sistemaPagamentosController.createOPE(s);
                            return result;
                        case "OPR":
                            log.info("OPR Type\n");
                            result = sistemaPagamentosController.createOPR(s);
                            return result;
                        case "OPC":
                            log.info("OPC Type\n");
                            result = sistemaPagamentosController.createOPC(s);
                            return result;
                    }
                    return true;
                } catch (Exception e) {
                    log.error(e.getMessage());
                    return false;
                }
            }
        };
    }
}
