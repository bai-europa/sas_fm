package pt.baieuropa.baieserv.processor.subscribers;


import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.service.controllers.SepaDDController;
import pt.baieuropa.nats.Runner;

import java.nio.charset.StandardCharsets;

public class SubscriberMovimentosParqueados {
    private static final Logger log = Logger.getLogger(SubscriberMovimentosParqueados.class);
    public static SepaDDController sepaDDController = new SepaDDController();

    public Runner subscribe(String action) {
        return new Runner() {
            @Override
            public boolean run(byte[] input) {
                try {
                    String s = new String(input, StandardCharsets.UTF_8);
                    JSONObject jsonObject = new JSONObject(s);
                    log.debug("\nRead from nats: "+s+"\n");
                    //TODO fazer verificações dos débitos diretos
//                    String channel = jsonObject.getString("Channel");
                    boolean result;
                    switch (action) {
                        case "ParkedMV":
                            log.info("Movimentos Parqueados (SEPADD)");
                            result = sepaDDController.movimentosParqueados(s);
                            return result;
                        case "RTran":
                            log.info("Movimentos Parqueados (SEPADD)");
                            result = sepaDDController.rTransactions(s);
                            return result;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return true;
            }
        };
    }
}
