/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.processor;

import lombok.extern.slf4j.Slf4j;
import org.yaml.snakeyaml.Yaml;
import pt.baieuropa.baieserv.exceptions.MissingParametersException;
import pt.baieuropa.baieserv.exceptions.NatsRequestException;
import pt.baieuropa.baieserv.processor.subscribers.*;

import pt.baieuropa.nats.Stan;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidParameterException;
import java.util.Map;

@Slf4j
public class NatsController {

    private Stan stanController;

    private String hostname_String;
    private String port_String;
    private String client_ID;
    private String cluster_ID;
    private String parameterError;

    public boolean initializeNats(String serviceName) {
        try {

            stanController = new Stan();

            stanController.connectToNats(System.getenv("NATS_CONFIGURATION"));
            try {
                //BAIE AML Transferencias Internas
                stanController.subscribeToTopic("CurrentAccountMovementInternalSent:Request", serviceName, new SubscriberMonetarias().subscribe("Internal","sent"));
                //BAIE Transferencias SEPA
                stanController.subscribeToTopic("CurrentAccountMovementDebitSEPASent:Request", serviceName, new SubscriberMonetarias().subscribe("Sepa","debitsent"));
                stanController.subscribeToTopic("CurrentAccountMovementCreditSEPASent:Request", serviceName, new SubscriberMonetarias().subscribe("Sepa","creditsent"));
//                stanController.subscribeToTopic("CurrentAccountMovementCreditSEPAReceived:Request", serviceName, new SubscriberMonetarias().subscribe("Sepa","received"));
                //BAIE SEPA DD (Débitos Diretos) - operation code 820
                stanController.subscribeToTopic("SEPADDCharged:Request", serviceName, new SubscriberMonetarias().subscribe("SepaDD","DD"));
                //BAIE SEPA DD (Débitos Diretos)- operation code 821
//                stanController.subscribeToTopic("DirectDebitInstruction:Request", serviceName, new SubscriberMonetarias().subscribe("IntructionDD","IDD"));
                //BAIE SEPA DD (Autorização Debito Directo)
//                stanController.subscribeToTopic("AuthorizeDirectDebit:Request", serviceName, new SubscriberAuthorizeDD().subscribe("AuthorizeDD","AuthDD"));
                //BAIE Movimentos Parqueados e Realizados - "Débitos Diretos"
//                stanController.subscribeToTopic("ParkedMovements:Request", serviceName, new SubscriberMovimentosParqueados().subscribe("ParkedMV"));
                //BAIE R-Transaction - "Débitos Diretos"
                //stanController.subscribeToTopic("RTransactions:Request", serviceName, new SubscriberMovimentosParqueados().subscribe("RTran"));
                //BAIE Detalhes de conta (clientes)
                stanController.subscribeToTopic("EnterpriseUpdated:Request", serviceName, new SubscriberClients().subscribe("EnterpriseClient"));
                stanController.subscribeToTopic("EnterpriseCreated:Request", serviceName, new SubscriberClients().subscribe("EnterpriseClient"));
                stanController.subscribeToTopic("ParticularUpdated:Request", serviceName, new SubscriberClients().subscribe("ParticularClient"));
                stanController.subscribeToTopic("ParticularCreated:Request", serviceName, new SubscriberClients().subscribe("ParticularClient"));
                //BAIE Movimentos de Cartões de Débito - Monetarias Cartões
                stanController.subscribeToTopic("DebitCardTransaction:Request", serviceName, new SubscriberMonetarias().subscribe("MonetariasCartoes",null));

                //BAIE-------------------------------------------SISTEMA DE PAGAMENTOS----------------------------------------------------------
                //BAIE OPE & OPA(OPE+OPE+OPE...)
                stanController.subscribeToTopic("OPE:Created", serviceName, new SubscriberSistemaPagamentos().subscribe("OPE"));
                //BAIE OPC
                stanController.subscribeToTopic("OPC:Created", serviceName, new SubscriberSistemaPagamentos().subscribe("OPC"));
                //BAIE OPR
                stanController.subscribeToTopic("OPR:Created", serviceName, new SubscriberSistemaPagamentos().subscribe("OPR"));
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }

            return true;
        } catch (IOException | InterruptedException | pt.baieuropa.nats.exceptions.MissingParametersException e) {
            log.error(e.getMessage());
            return false;
        }
    }

    public boolean insertRequest(String topic, String content) throws NatsRequestException {
        try {
            stanController.insertRequestOnTopic(topic, content);
            return true;
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new NatsRequestException(e.getMessage());
        } finally {
//            log.info("Number of requests on topic "+ this.topic + " is : "+ stanController.getPendingRequestsFromTopic(this.topic));
        }
    }

    private void readPropertiesFromYAML(String AS400YAMLFileName) throws MissingParametersException, InvalidParameterException {

        log.info("File AS400 Name: " + AS400YAMLFileName);

        //TODO change this to a method
        try {
            Yaml yaml = new Yaml();
            InputStream inputStream = new FileInputStream(AS400YAMLFileName);
//            InputStream inputStream = this.getClass()
//                    .getClassLoader()
//                    .getResourceAsStream(AS400YAMLFileName);
            Map<String, Object> yamlObject = yaml.load(inputStream);

            yamlObject.forEach((name, value) -> {

                switch ((String) name) {
                    case "hostname_String":
                        hostname_String = (String) value;
                        break;
                    case "port_String":
                        port_String = (String) value;
                        break;
                    case "cluster_ID":
                        cluster_ID = (String) value;
                        break;
                    case "client_ID":
                        client_ID = (String) value;
                        break;
                    default:
                        parameterError = (String) name;
                        break;
                }

            });
        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            if (parameterError != null)
                throw new InvalidParameterException("The parameter " + parameterError + " is not recognized by the application. (hostname_String, port_String)");

            if ((hostname_String == null) || (port_String == null))
                throw new MissingParametersException("The YAML file with the configuration to the AS400 is missing one or more parameters");
        }
    }


}
