/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.processor;

import lombok.extern.slf4j.Slf4j;
import pt.baieuropa.baieserv.configurations.ProcessFraudLog4jPropertiesConfiguration;

@Slf4j
public class Process implements Processor {


    private NatsController natsController;


    public Process(String serviceName) {
        // BAIE - Setting LOG4J configuration for the project ProcessFraudWorkflow
        ProcessFraudLog4jPropertiesConfiguration processFraudLog4jPropertiesConfiguration = new ProcessFraudLog4jPropertiesConfiguration();
        processFraudLog4jPropertiesConfiguration.configure();
        // BAIE - Setting LOG4J configuration for the project BAIENats
        //BAIENatsLog4jPropertiesConfiguration BAIENatsLog4JPropertiesConfiguration = new BAIENatsLog4jPropertiesConfiguration();
        //BAIENatsLog4JPropertiesConfiguration.configure();

        natsController = new NatsController();
        natsController.initializeNats(serviceName);

        //BAIE função para conectar ao AS400--> necessário para realizar queries
        //SepaDDJsonBuilder.connect();
    }

    @Override
    public boolean initializeService() {
        return false;
    }

    @Override
    public boolean controlledStopService() {
        return false;
    }

    @Override
    public boolean forceStopService() {
        return false;
    }

    /**
     * Publish to nats
     *
     * @param subject
     * @param content
     * @return
     */
    public String publish(String subject, String content) {
        try {
            // BAI Sends Approved JSON Message
            natsController.insertRequest(subject, content);
            return "OK";
        } catch (Exception e) {
            log.error("Erro a publicar mensagem: \n" + e.getMessage());
            return "NOT OK";
        }
    }
}
