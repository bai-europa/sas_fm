/*
  Created by: loliveira
*/

package pt.baieuropa.baieserv.processor.database;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import pt.baieuropa.baieserv.models.MongoStructure;

@Slf4j
public class MongoDBManager {

    private SimpleMongoClientDbFactory mongoClientFactory;
    private MongoDatabase database;
    public static MongoOperations mongoOps;

    @Autowired
    MongoDbFactory mongoDbFactory;
    @Autowired
    MongoMappingContext mongoMappingContext;

    public MongoDBManager() {
        log.debug("Connecting to mongoDB\n");
        mongoClientFactory = new SimpleMongoClientDbFactory(MongoClients.create("mongodb://127.0.0.1"), "sasfraud");
        mongoOps = mongoTemplate(); //Call our configuration
    }

    public MongoTemplate mongoTemplate() {
        //remove _class
        MappingMongoConverter converter = new MappingMongoConverter(mongoClientFactory, new MongoMappingContext());
        converter.setTypeMapper(new DefaultMongoTypeMapper(null));

        MongoTemplate mongoTemplate = new MongoTemplate(mongoClientFactory, converter);

        return mongoTemplate;
    }

    public void insertOnCollection(String collection, MongoStructure mongoStructure) {
        mongoOps.save(mongoStructure, collection); //Save da estrutura na collection definida
    }
}
