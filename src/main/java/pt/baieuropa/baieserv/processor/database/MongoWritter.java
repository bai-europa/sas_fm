package pt.baieuropa.baieserv.processor.database;

import lombok.extern.slf4j.Slf4j;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import pt.baieuropa.baieserv.models.MongoStructure;

import java.sql.Timestamp;
import java.util.HashMap;

@Slf4j
public class MongoWritter {

    public static MongoDBManager mongo = new MongoDBManager();
    public static MongoStructure mongoStructure;


    //BAIE---------------------------------------INSERT DATA ON MONGO-----------------------------------------

    /**
     * Insert transaction data on mongo
     * @param transactiontype type of transaction
     * @param natsMessage content received from nats
     * @param object Object builded on this project
     * @param responseStructureHashMap response from Sas database
     */
    public static void insertNewMessage(String transactiontype, String natsMessage, Object object, HashMap<String, String> responseStructureHashMap) {
        JSONObject jsonObject = new JSONObject(natsMessage);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        mongoStructure = new MongoStructure();
        mongoStructure.setTransactiontype(transactiontype);
        mongoStructure.setNatsMessage(jsonObject.toMap());
        mongoStructure.setTimestamp(timestamp.toString());
        mongoStructure.setSendToSas(object);
        mongoStructure.setSasMessageResponse(responseStructureHashMap);
        mongo.insertOnCollection("fraud", mongoStructure);
    }



}
