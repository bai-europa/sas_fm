package pt.baieuropa.baieserv.models.sepaDD;

public class SepaDD {
    public String transactionDate = "";
    public String transactionTime = "";
    public String billingDate = "";
    public String authorizationSigningDate = "";
    public String reasonTransfer = "";
    public String transferReasonRategory = "";
    public String kindOfService = "";
    public String transactionChannel = "";
    public String instrument = "";
    public String debtorBankBIC = "";
    public String identificationOfLastDebtor = "";
    public String transactionId = "";
    public String movementType = "";
    public String authorizationChangeReason = "";
    public String chargeReferenceCreditor = "";
    public String originalAuthorizationID = "";
    public String debtorBANKAEntityID = "";
    public String debtorName = "";
    public String debtorCountry = "";
    public String debtorIBAN = "";
    public String amount = "";
    public String currency = "";
    public String creditorName = "";
    public String creditorCountry = "";
    public String creditorbankBIC = "";
    public String creditorIBAN = "";

    public SepaDD(String transactionDate, String transactionTime, String billingDate, String authorizationSigningDate, String reasonTransfer, String transferReasonRategory, String kindOfService, String transactionChannel, String instrument, String debtorBankBIC, String identificationOfLastDebtor, String transactionId, String movementType, String authorizationChangeReason, String chargeReferenceCreditor, String originalAuthorizationID, String debtorBANKAEntityID, String debtorName, String debtorCountry, String debtorIBAN, String amount, String currency, String creditorName, String creditorCountry, String creditorbankBIC, String creditorIBAN) {
        this.transactionDate = transactionDate;
        this.transactionTime = transactionTime;
        this.billingDate = billingDate;
        this.authorizationSigningDate = authorizationSigningDate;
        this.reasonTransfer = reasonTransfer;
        this.transferReasonRategory = transferReasonRategory;
        this.kindOfService = kindOfService;
        this.transactionChannel = transactionChannel;
        this.instrument = instrument;
        this.debtorBankBIC = debtorBankBIC;
        this.identificationOfLastDebtor = identificationOfLastDebtor;
        this.transactionId = transactionId;
        this.movementType = movementType;
        this.authorizationChangeReason = authorizationChangeReason;
        this.chargeReferenceCreditor = chargeReferenceCreditor;
        this.originalAuthorizationID = originalAuthorizationID;
        this.debtorBANKAEntityID = debtorBANKAEntityID;
        this.debtorName = debtorName;
        this.debtorCountry = debtorCountry;
        this.debtorIBAN = debtorIBAN;
        this.amount = amount;
        this.currency = currency;
        this.creditorName = creditorName;
        this.creditorCountry = creditorCountry;
        this.creditorbankBIC = creditorbankBIC;
        this.creditorIBAN = creditorIBAN;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public String getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(String billingDate) {
        this.billingDate = billingDate;
    }

    public String getAuthorizationSigningDate() {
        return authorizationSigningDate;
    }

    public void setAuthorizationSigningDate(String authorizationSigningDate) {
        this.authorizationSigningDate = authorizationSigningDate;
    }

    public String getReasonTransfer() {
        return reasonTransfer;
    }

    public void setReasonTransfer(String reasonTransfer) {
        this.reasonTransfer = reasonTransfer;
    }

    public String getTransferReasonRategory() {
        return transferReasonRategory;
    }

    public void setTransferReasonRategory(String transferReasonRategory) {
        this.transferReasonRategory = transferReasonRategory;
    }

    public String getKindOfService() {
        return kindOfService;
    }

    public void setKindOfService(String kindOfService) {
        this.kindOfService = kindOfService;
    }

    public String getTransactionChannel() {
        return transactionChannel;
    }

    public void setTransactionChannel(String transactionChannel) {
        this.transactionChannel = transactionChannel;
    }

    public String getInstrument() {
        return instrument;
    }

    public void setInstrument(String instrument) {
        this.instrument = instrument;
    }

    public String getDebtorBankBIC() {
        return debtorBankBIC;
    }

    public void setDebtorBankBIC(String debtorBankBIC) {
        this.debtorBankBIC = debtorBankBIC;
    }

    public String getIdentificationOfLastDebtor() {
        return identificationOfLastDebtor;
    }

    public void setIdentificationOfLastDebtor(String identificationOfLastDebtor) {
        this.identificationOfLastDebtor = identificationOfLastDebtor;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getAuthorizationChangeReason() {
        return authorizationChangeReason;
    }

    public void setAuthorizationChangeReason(String authorizationChangeReason) {
        this.authorizationChangeReason = authorizationChangeReason;
    }

    public String getChargeReferenceCreditor() {
        return chargeReferenceCreditor;
    }

    public void setChargeReferenceCreditor(String chargeReferenceCreditor) {
        this.chargeReferenceCreditor = chargeReferenceCreditor;
    }

    public String getOriginalAuthorizationID() {
        return originalAuthorizationID;
    }

    public void setOriginalAuthorizationID(String originalAuthorizationID) {
        this.originalAuthorizationID = originalAuthorizationID;
    }

    public String getDebtorBANKAEntityID() {
        return debtorBANKAEntityID;
    }

    public void setDebtorBANKAEntityID(String debtorBANKAEntityID) {
        this.debtorBANKAEntityID = debtorBANKAEntityID;
    }

    public String getDebtorName() {
        return debtorName;
    }

    public void setDebtorName(String debtorName) {
        this.debtorName = debtorName;
    }

    public String getDebtorCountry() {
        return debtorCountry;
    }

    public void setDebtorCountry(String debtorCountry) {
        this.debtorCountry = debtorCountry;
    }

    public String getDebtorIBAN() {
        return debtorIBAN;
    }

    public void setDebtorIBAN(String debtorIBAN) {
        this.debtorIBAN = debtorIBAN;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditorName() {
        return creditorName;
    }

    public void setCreditorName(String creditorName) {
        this.creditorName = creditorName;
    }

    public String getCreditorCountry() {
        return creditorCountry;
    }

    public void setCreditorCountry(String creditorCountry) {
        this.creditorCountry = creditorCountry;
    }

    public String getCreditorbankBIC() {
        return creditorbankBIC;
    }

    public void setCreditorbankBIC(String creditorbankBIC) {
        this.creditorbankBIC = creditorbankBIC;
    }

    public String getCreditorIBAN() {
        return creditorIBAN;
    }

    public void setCreditorIBAN(String creditorIBAN) {
        this.creditorIBAN = creditorIBAN;
    }
}
