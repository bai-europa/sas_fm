package pt.baieuropa.baieserv.models;

import pt.baieuropa.baieserv.models.alteracaoDetalhesConta.PartyModRq;

public class MongoAccountDetails {

    public PartyModRq partyModRq;

    public MongoAccountDetails(PartyModRq partyModRq) {
        this.partyModRq = partyModRq;
    }

    public PartyModRq getPartyModRq() {
        return partyModRq;
    }

    public void setPartyModRq(PartyModRq partyModRq) {
        this.partyModRq = partyModRq;
    }
}
