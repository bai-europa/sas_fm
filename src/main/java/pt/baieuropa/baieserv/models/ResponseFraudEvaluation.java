package pt.baieuropa.baieserv.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseFraudEvaluation {

    @JsonProperty("operation_nr")
    private String operation_nr;
    @JsonProperty("status")
    private String status;

    public ResponseFraudEvaluation() {
    }

    public ResponseFraudEvaluation(String operation_nr, String status) {
        this.operation_nr = operation_nr;
        this.status = status;
    }

    public String getOperation_nr() {
        return operation_nr;
    }

    public void setOperation_nr(String operation_nr) {
        this.operation_nr = operation_nr;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "operation_nr='" + operation_nr + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
