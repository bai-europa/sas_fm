package pt.baieuropa.baieserv.models;

public class ResponseStructure {
    private String fieldName;
    private String fieldResponseValue;

    public ResponseStructure() {
    }

    public ResponseStructure(String fieldName, String fieldResponseValue) {
        this.fieldName = fieldName;
        this.fieldResponseValue = fieldResponseValue;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldResponseValue() {
        return fieldResponseValue;
    }

    public void setFieldResponseValue(String fieldResponseValue) {
        this.fieldResponseValue = fieldResponseValue;
    }
}
