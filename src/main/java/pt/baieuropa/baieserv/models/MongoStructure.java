package pt.baieuropa.baieserv.models;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.HashMap;
import java.util.Map;


@Document(collection = "fraud")
public class MongoStructure {

    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private ObjectId id;
    private String transactiontype;
    private String timestamp;
    private Map natsMessage;
    private Object SasRequest;
    private HashMap<String,String> SasMessageResponse;


    public MongoStructure() {
    }

    public MongoStructure(String transactiontype, String timestamp,Map natsMessage, Object sendToSas, HashMap<String, String> sasMessageResponse) {
        this.transactiontype = transactiontype;
        this.timestamp = timestamp;
        this.natsMessage = natsMessage;
        this.SasRequest = sendToSas;
        SasMessageResponse = sasMessageResponse;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public Map getNatsMessage() {
        return natsMessage;
    }

    public void setNatsMessage(Map natsMessage) {
        this.natsMessage = natsMessage;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Object getSendToSas() {
        return SasRequest;
    }

    public void setSendToSas(Object sendToSas) {
        this.SasRequest = sendToSas;
    }

    public HashMap<String, String> getSasMessageResponse() {
        return SasMessageResponse;
    }

    public void setSasMessageResponse(HashMap<String, String> sasMessageResponse) {
        SasMessageResponse = sasMessageResponse;
    }
}