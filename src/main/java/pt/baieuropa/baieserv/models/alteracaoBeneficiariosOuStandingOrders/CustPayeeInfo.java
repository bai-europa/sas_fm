
package pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "LastPmtCurAmt",
    "DfltPmtData",
    "LastPmtDt",
    "PayeeAcctNum",
    "Name"
})
public class CustPayeeInfo {

    @JsonProperty("LastPmtCurAmt")
    private LastPmtCurAmt lastPmtCurAmt;
    @JsonProperty("DfltPmtData")
    private DfltPmtData dfltPmtData;
    @JsonProperty("LastPmtDt")
    private String lastPmtDt;
    @JsonProperty("PayeeAcctNum")
    private String payeeAcctNum;
    @JsonProperty("Name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("LastPmtCurAmt")
    public LastPmtCurAmt getLastPmtCurAmt() {
        return lastPmtCurAmt;
    }

    @JsonProperty("LastPmtCurAmt")
    public void setLastPmtCurAmt(LastPmtCurAmt lastPmtCurAmt) {
        this.lastPmtCurAmt = lastPmtCurAmt;
    }

    public CustPayeeInfo withLastPmtCurAmt(LastPmtCurAmt lastPmtCurAmt) {
        this.lastPmtCurAmt = lastPmtCurAmt;
        return this;
    }

    @JsonProperty("DfltPmtData")
    public DfltPmtData getDfltPmtData() {
        return dfltPmtData;
    }

    @JsonProperty("DfltPmtData")
    public void setDfltPmtData(DfltPmtData dfltPmtData) {
        this.dfltPmtData = dfltPmtData;
    }

    public CustPayeeInfo withDfltPmtData(DfltPmtData dfltPmtData) {
        this.dfltPmtData = dfltPmtData;
        return this;
    }

    @JsonProperty("LastPmtDt")
    public String getLastPmtDt() {
        return lastPmtDt;
    }

    @JsonProperty("LastPmtDt")
    public void setLastPmtDt(String lastPmtDt) {
        this.lastPmtDt = lastPmtDt;
    }

    public CustPayeeInfo withLastPmtDt(String lastPmtDt) {
        this.lastPmtDt = lastPmtDt;
        return this;
    }

    @JsonProperty("PayeeAcctNum")
    public String getPayeeAcctNum() {
        return payeeAcctNum;
    }

    @JsonProperty("PayeeAcctNum")
    public void setPayeeAcctNum(String payeeAcctNum) {
        this.payeeAcctNum = payeeAcctNum;
    }

    public CustPayeeInfo withPayeeAcctNum(String payeeAcctNum) {
        this.payeeAcctNum = payeeAcctNum;
        return this;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    public CustPayeeInfo withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CustPayeeInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(lastPmtCurAmt).append(dfltPmtData).append(lastPmtDt).append(payeeAcctNum).append(name).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustPayeeInfo) == false) {
            return false;
        }
        CustPayeeInfo rhs = ((CustPayeeInfo) other);
        return new EqualsBuilder().append(lastPmtCurAmt, rhs.lastPmtCurAmt).append(dfltPmtData, rhs.dfltPmtData).append(lastPmtDt, rhs.lastPmtDt).append(payeeAcctNum, rhs.payeeAcctNum).append(name, rhs.name).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
