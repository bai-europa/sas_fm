
package pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CustPayeeInfo",
    "RqUID",
    "PartyInfo"
})
public class CustPayeeModRq {

    @JsonProperty("CustPayeeInfo")
    private CustPayeeInfo custPayeeInfo;
    @JsonProperty("RqUID")
    private String rqUID;
    @JsonProperty("PartyInfo")
    private PartyInfo partyInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CustPayeeInfo")
    public CustPayeeInfo getCustPayeeInfo() {
        return custPayeeInfo;
    }

    @JsonProperty("CustPayeeInfo")
    public void setCustPayeeInfo(CustPayeeInfo custPayeeInfo) {
        this.custPayeeInfo = custPayeeInfo;
    }

    public CustPayeeModRq withCustPayeeInfo(CustPayeeInfo custPayeeInfo) {
        this.custPayeeInfo = custPayeeInfo;
        return this;
    }

    @JsonProperty("RqUID")
    public String getRqUID() {
        return rqUID;
    }

    @JsonProperty("RqUID")
    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public CustPayeeModRq withRqUID(String rqUID) {
        this.rqUID = rqUID;
        return this;
    }

    @JsonProperty("PartyInfo")
    public PartyInfo getPartyInfo() {
        return partyInfo;
    }

    @JsonProperty("PartyInfo")
    public void setPartyInfo(PartyInfo partyInfo) {
        this.partyInfo = partyInfo;
    }

    public CustPayeeModRq withPartyInfo(PartyInfo partyInfo) {
        this.partyInfo = partyInfo;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CustPayeeModRq withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(custPayeeInfo).append(rqUID).append(partyInfo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CustPayeeModRq) == false) {
            return false;
        }
        CustPayeeModRq rhs = ((CustPayeeModRq) other);
        return new EqualsBuilder().append(custPayeeInfo, rhs.custPayeeInfo).append(rqUID, rhs.rqUID).append(partyInfo, rhs.partyInfo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
