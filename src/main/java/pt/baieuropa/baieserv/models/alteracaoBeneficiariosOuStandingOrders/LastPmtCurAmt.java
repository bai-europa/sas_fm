
package pt.baieuropa.baieserv.models.alteracaoBeneficiariosOuStandingOrders;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Amt"
})
public class LastPmtCurAmt {

    @JsonProperty("Amt")
    private String amt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Amt")
    public String getAmt() {
        return amt;
    }

    @JsonProperty("Amt")
    public void setAmt(String amt) {
        this.amt = amt;
    }

    public LastPmtCurAmt withAmt(String amt) {
        this.amt = amt;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public LastPmtCurAmt withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(amt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof LastPmtCurAmt) == false) {
            return false;
        }
        LastPmtCurAmt rhs = ((LastPmtCurAmt) other);
        return new EqualsBuilder().append(amt, rhs.amt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
