
package pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SecObjInfo",
    "RqUID"
})
public class SecObjModRq {

    @JsonProperty("SecObjInfo")
    private SecObjInfo secObjInfo;
    @JsonProperty("RqUID")
    private String rqUID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SecObjInfo")
    public SecObjInfo getSecObjInfo() {
        return secObjInfo;
    }

    @JsonProperty("SecObjInfo")
    public void setSecObjInfo(SecObjInfo secObjInfo) {
        this.secObjInfo = secObjInfo;
    }

    public SecObjModRq withSecObjInfo(SecObjInfo secObjInfo) {
        this.secObjInfo = secObjInfo;
        return this;
    }

    @JsonProperty("RqUID")
    public String getRqUID() {
        return rqUID;
    }

    @JsonProperty("RqUID")
    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public SecObjModRq withRqUID(String rqUID) {
        this.rqUID = rqUID;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SecObjModRq withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(secObjInfo).append(rqUID).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SecObjModRq) == false) {
            return false;
        }
        SecObjModRq rhs = ((SecObjModRq) other);
        return new EqualsBuilder().append(secObjInfo, rhs.secObjInfo).append(rqUID, rhs.rqUID).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
