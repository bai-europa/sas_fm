
package pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IssuedIdent"
})
public class PartyData {

    @JsonProperty("IssuedIdent")
    private IssuedIdent issuedIdent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("IssuedIdent")
    public IssuedIdent getIssuedIdent() {
        return issuedIdent;
    }

    @JsonProperty("IssuedIdent")
    public void setIssuedIdent(IssuedIdent issuedIdent) {
        this.issuedIdent = issuedIdent;
    }

    public PartyData withIssuedIdent(IssuedIdent issuedIdent) {
        this.issuedIdent = issuedIdent;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(issuedIdent).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyData) == false) {
            return false;
        }
        PartyData rhs = ((PartyData) other);
        return new EqualsBuilder().append(issuedIdent, rhs.issuedIdent).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
