
package pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import pt.baieuropa.baieserv.models.monetarias.GovIssuedIdent;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IssuedIdentValue",
        "GovIssuedIdent"
})
public class IssuedIdent {

    @JsonProperty("IssuedIdentValue")
    private String issuedIdentValue;

    @JsonProperty("GovIssuedIdent")
    private GovIssuedIdent govIssuedIdent;

    @JsonProperty("GovIssuedIdent")
    public GovIssuedIdent getGovIssuedIdent() {
        return govIssuedIdent;
    }

    @JsonProperty("GovIssuedIdent")
    public void setGovIssuedIdent(GovIssuedIdent govIssuedIdent) {
        this.govIssuedIdent = govIssuedIdent;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("IssuedIdentValue")
    public String getIssuedIdentValue() {
        return issuedIdentValue;
    }

    @JsonProperty("IssuedIdentValue")
    public void setIssuedIdentValue(String issuedIdentValue) {
        this.issuedIdentValue = issuedIdentValue;
    }

    public IssuedIdent withIssuedIdentValue(String issuedIdentValue) {
        this.issuedIdentValue = issuedIdentValue;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public IssuedIdent withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(issuedIdentValue).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof IssuedIdent) == false) {
            return false;
        }
        IssuedIdent rhs = ((IssuedIdent) other);
        return new EqualsBuilder().append(issuedIdentValue, rhs.issuedIdentValue).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
