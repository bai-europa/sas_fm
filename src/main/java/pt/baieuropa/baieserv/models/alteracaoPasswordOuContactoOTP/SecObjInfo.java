
package pt.baieuropa.baieserv.models.alteracaoPasswordOuContactoOTP;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "StrongAuth",
    "PartyData",
    "SecObjPurpose",
    "SecObjValue",
    "RefData",
    "PrcDt"
})
public class SecObjInfo {

    @JsonProperty("StrongAuth")
    private String strongAuth;
    @JsonProperty("PartyData")
    private PartyData partyData;
    @JsonProperty("SecObjPurpose")
    private String secObjPurpose;
    @JsonProperty("SecObjValue")
    private String secObjValue;
    @JsonProperty("RefData")
    private RefData refData;
    @JsonProperty("PrcDt")
    private String prcDt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StrongAuth")
    public String getStrongAuth() {
        return strongAuth;
    }

    @JsonProperty("StrongAuth")
    public void setStrongAuth(String strongAuth) {
        this.strongAuth = strongAuth;
    }

    public SecObjInfo withStrongAuth(String strongAuth) {
        this.strongAuth = strongAuth;
        return this;
    }

    @JsonProperty("PartyData")
    public PartyData getPartyData() {
        return partyData;
    }

    @JsonProperty("PartyData")
    public void setPartyData(PartyData partyData) {
        this.partyData = partyData;
    }

    public SecObjInfo withPartyData(PartyData partyData) {
        this.partyData = partyData;
        return this;
    }

    @JsonProperty("SecObjPurpose")
    public String getSecObjPurpose() {
        return secObjPurpose;
    }

    @JsonProperty("SecObjPurpose")
    public void setSecObjPurpose(String secObjPurpose) {
        this.secObjPurpose = secObjPurpose;
    }

    public SecObjInfo withSecObjPurpose(String secObjPurpose) {
        this.secObjPurpose = secObjPurpose;
        return this;
    }

    @JsonProperty("SecObjValue")
    public String getSecObjValue() {
        return secObjValue;
    }

    @JsonProperty("SecObjValue")
    public void setSecObjValue(String secObjValue) {
        this.secObjValue = secObjValue;
    }

    public SecObjInfo withSecObjValue(String secObjValue) {
        this.secObjValue = secObjValue;
        return this;
    }

    @JsonProperty("RefData")
    public RefData getRefData() {
        return refData;
    }

    @JsonProperty("RefData")
    public void setRefData(RefData refData) {
        this.refData = refData;
    }

    public SecObjInfo withRefData(RefData refData) {
        this.refData = refData;
        return this;
    }

    @JsonProperty("PrcDt")
    public String getPrcDt() {
        return prcDt;
    }

    @JsonProperty("PrcDt")
    public void setPrcDt(String prcDt) {
        this.prcDt = prcDt;
    }

    public SecObjInfo withPrcDt(String prcDt) {
        this.prcDt = prcDt;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SecObjInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(strongAuth).append(partyData).append(secObjPurpose).append(secObjValue).append(refData).append(prcDt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SecObjInfo) == false) {
            return false;
        }
        SecObjInfo rhs = ((SecObjInfo) other);
        return new EqualsBuilder().append(strongAuth, rhs.strongAuth).append(partyData, rhs.partyData).append(secObjPurpose, rhs.secObjPurpose).append(secObjValue, rhs.secObjValue).append(refData, rhs.refData).append(prcDt, rhs.prcDt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
