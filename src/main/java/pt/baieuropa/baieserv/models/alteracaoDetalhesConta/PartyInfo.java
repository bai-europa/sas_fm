
package pt.baieuropa.baieserv.models.alteracaoDetalhesConta;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "StrongAuth",
    "PartyPref",
    "SecObjPurpose",
    "Correspondence",
    "PrcDt",
    "Name"
})
public class PartyInfo {

    @JsonProperty("StrongAuth")
    private String strongAuth;
    @JsonProperty("PartyPref")
    private PartyPref partyPref;
    @JsonProperty("SecObjPurpose")
    private String secObjPurpose;
    @JsonProperty("Correspondence")
    private Correspondence correspondence;
    @JsonProperty("PrcDt")
    private String prcDt;
    @JsonProperty("Name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("StrongAuth")
    public String getStrongAuth() {
        return strongAuth;
    }

    @JsonProperty("StrongAuth")
    public void setStrongAuth(String strongAuth) {
        this.strongAuth = strongAuth;
    }

    public PartyInfo withStrongAuth(String strongAuth) {
        this.strongAuth = strongAuth;
        return this;
    }

    @JsonProperty("PartyPref")
    public PartyPref getPartyPref() {
        return partyPref;
    }

    @JsonProperty("PartyPref")
    public void setPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
    }

    public PartyInfo withPartyPref(PartyPref partyPref) {
        this.partyPref = partyPref;
        return this;
    }

    @JsonProperty("SecObjPurpose")
    public String getSecObjPurpose() {
        return secObjPurpose;
    }

    @JsonProperty("SecObjPurpose")
    public void setSecObjPurpose(String secObjPurpose) {
        this.secObjPurpose = secObjPurpose;
    }

    public PartyInfo withSecObjPurpose(String secObjPurpose) {
        this.secObjPurpose = secObjPurpose;
        return this;
    }

    @JsonProperty("Correspondence")
    public Correspondence getCorrespondence() {
        return correspondence;
    }

    @JsonProperty("Correspondence")
    public void setCorrespondence(Correspondence correspondence) {
        this.correspondence = correspondence;
    }

    public PartyInfo withCorrespondence(Correspondence correspondence) {
        this.correspondence = correspondence;
        return this;
    }

    @JsonProperty("PrcDt")
    public String getPrcDt() {
        return prcDt;
    }

    @JsonProperty("PrcDt")
    public void setPrcDt(String prcDt) {
        this.prcDt = prcDt;
    }

    public PartyInfo withPrcDt(String prcDt) {
        this.prcDt = prcDt;
        return this;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    public PartyInfo withName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(strongAuth).append(partyPref).append(secObjPurpose).append(correspondence).append(prcDt).append(name).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyInfo) == false) {
            return false;
        }
        PartyInfo rhs = ((PartyInfo) other);
        return new EqualsBuilder().append(strongAuth, rhs.strongAuth).append(partyPref, rhs.partyPref).append(secObjPurpose, rhs.secObjPurpose).append(correspondence, rhs.correspondence).append(prcDt, rhs.prcDt).append(name, rhs.name).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
