
package pt.baieuropa.baieserv.models.alteracaoDetalhesConta;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "FinancialAmt",
    "Language"
})
public class PartyPref {

    @JsonProperty("FinancialAmt")
    private String financialAmt;
    @JsonProperty("Language")
    private String language;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("FinancialAmt")
    public String getFinancialAmt() {
        return financialAmt;
    }

    @JsonProperty("FinancialAmt")
    public void setFinancialAmt(String financialAmt) {
        this.financialAmt = financialAmt;
    }

    public PartyPref withFinancialAmt(String financialAmt) {
        this.financialAmt = financialAmt;
        return this;
    }

    @JsonProperty("Language")
    public String getLanguage() {
        return language;
    }

    @JsonProperty("Language")
    public void setLanguage(String language) {
        this.language = language;
    }

    public PartyPref withLanguage(String language) {
        this.language = language;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyPref withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(financialAmt).append(language).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyPref) == false) {
            return false;
        }
        PartyPref rhs = ((PartyPref) other);
        return new EqualsBuilder().append(financialAmt, rhs.financialAmt).append(language, rhs.language).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
