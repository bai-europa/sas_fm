
package pt.baieuropa.baieserv.models.alteracaoDetalhesConta;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PartyId",
    "LoginIdent"
})
public class PartyKeys {

    @JsonProperty("PartyId")
    private String partyId;
    @JsonProperty("LoginIdent")
    private LoginIdent loginIdent;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PartyId")
    public String getPartyId() {
        return partyId;
    }

    @JsonProperty("PartyId")
    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public PartyKeys withPartyId(String partyId) {
        this.partyId = partyId;
        return this;
    }

    @JsonProperty("LoginIdent")
    public LoginIdent getLoginIdent() {
        return loginIdent;
    }

    @JsonProperty("LoginIdent")
    public void setLoginIdent(LoginIdent loginIdent) {
        this.loginIdent = loginIdent;
    }

    public PartyKeys withLoginIdent(LoginIdent loginIdent) {
        this.loginIdent = loginIdent;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyKeys withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyId).append(loginIdent).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyKeys) == false) {
            return false;
        }
        PartyKeys rhs = ((PartyKeys) other);
        return new EqualsBuilder().append(partyId, rhs.partyId).append(loginIdent, rhs.loginIdent).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
