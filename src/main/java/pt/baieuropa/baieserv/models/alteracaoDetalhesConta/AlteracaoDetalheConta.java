
package pt.baieuropa.baieserv.models.alteracaoDetalhesConta;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PartyModRq"
})
public class AlteracaoDetalheConta {

    @JsonProperty("PartyModRq")
    private PartyModRq partyModRq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PartyModRq")
    public PartyModRq getPartyModRq() {
        return partyModRq;
    }

    @JsonProperty("PartyModRq")
    public void setPartyModRq(PartyModRq partyModRq) {
        this.partyModRq = partyModRq;
    }

    public AlteracaoDetalheConta withPartyModRq(PartyModRq partyModRq) {
        this.partyModRq = partyModRq;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AlteracaoDetalheConta withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyModRq).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AlteracaoDetalheConta) == false) {
            return false;
        }
        AlteracaoDetalheConta rhs = ((AlteracaoDetalheConta) other);
        return new EqualsBuilder().append(partyModRq, rhs.partyModRq).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
