package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "OpenDt",
        "MaturityDt",
        "TaxCountry"
})
public class AcctInfo {
    @JsonProperty("OpenDt")
    private String OpenDt;

    @JsonProperty("MaturityDt")
    private String MaturityDt;

    @JsonProperty("TaxCountry")
    private TaxCountry taxCountry;

    @JsonProperty("TaxCountry")
    public TaxCountry getTaxCountry() {
        return taxCountry;
    }

    @JsonProperty("TaxCountry")
    public void setTaxCountry(TaxCountry taxCountry) {
        this.taxCountry = taxCountry;
    }

    @JsonProperty("OpenDt")
    public String getOpenDt() {
        return OpenDt;
    }

    @JsonProperty("OpenDt")
    public void setOpenDt(String openDt) {
        OpenDt = openDt;
    }

    @JsonProperty("MaturityDt")
    public String getMaturityDt() {
        return MaturityDt;
    }

    @JsonProperty("MaturityDt")
    public void setMaturityDt(String maturityDt) {
        MaturityDt = maturityDt;
    }
}
