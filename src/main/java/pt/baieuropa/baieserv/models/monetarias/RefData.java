
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "RefIdent",
        "AuthorizationDt",
        "ServiceCd",
        "Instrument",
        "MovementCd",
        "ChangeReason",
        "OrgAuthorization"
})
public class RefData {

    @JsonProperty("RefIdent")
    private String refIdent;
    @JsonProperty("AuthorizationDt")
    private String AuthorizationDt;
    @JsonProperty("ServiceCd")
    private String ServiceCd;
    @JsonProperty("Instrument")
    private String Instrument;
    @JsonProperty("MovementCd")
    private String MovementCd;
    @JsonProperty("ChangeReason")
    private String ChangeReason;
    @JsonProperty("OrgAuthorization")
    private String OrgAuthorization;

    @JsonProperty("OrgAuthorization")
    public String getOrgAuthorization() {
        return OrgAuthorization;
    }

    @JsonProperty("OrgAuthorization")
    public void setOrgAuthorization(String orgAuthorization) {
        OrgAuthorization = orgAuthorization;
    }

    @JsonProperty("MovementCd")
    public String getMovementCd() {
        return MovementCd;
    }

    @JsonProperty("MovementCd")
    public void setMovementCd(String movementCd) {
        MovementCd = movementCd;
    }

    @JsonProperty("ChangeReason")
    public String getChangeReason() {
        return ChangeReason;
    }

    @JsonProperty("ChangeReason")
    public void setChangeReason(String changeReason) {
        ChangeReason = changeReason;
    }

    @JsonProperty("ServiceCd")
    public String getServiceCd() {
        return ServiceCd;
    }

    @JsonProperty("ServiceCd")
    public void setServiceCd(String serviceCd) {
        ServiceCd = serviceCd;
    }

    @JsonProperty("Instrument")
    public String getInstrument() {
        return Instrument;
    }

    @JsonProperty("Instrument")
    public void setInstrument(String instrument) {
        Instrument = instrument;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AuthorizationDt")
    public String getAuthorizationDt() {
        return AuthorizationDt;
    }

    @JsonProperty("AuthorizationDt")
    public void setAuthorizationDt(String authorizationDt) {
        AuthorizationDt = authorizationDt;
    }

    @JsonProperty("RefIdent")
    public String getRefIdent() {
        return refIdent;
    }

    @JsonProperty("RefIdent")
    public void setRefIdent(String refIdent) {
        this.refIdent = refIdent;
    }

    public RefData withRefIdent(String refIdent) {
        this.refIdent = refIdent;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public RefData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(refIdent).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RefData) == false) {
            return false;
        }
        RefData rhs = ((RefData) other);
        return new EqualsBuilder().append(refIdent, rhs.refIdent).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
