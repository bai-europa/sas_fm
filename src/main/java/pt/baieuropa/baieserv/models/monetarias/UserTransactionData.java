package pt.baieuropa.baieserv.models.monetarias;

public class UserTransactionData {

    // CLIENTE
    private String clientNumber = "";
    // NOME
    private String name = "";
    // MORADA
    private String address = "";
    // IBAN/CONTA
    private String account = "";
    // PAIS
    private String country = "";
    //Tipo de Beneficiario
    private String beneficiarioType = "";
    //Aniversario
    private String birthDt = "";

    public String getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(String clientNumber) {
        this.clientNumber = clientNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBeneficiarioType() {
        return beneficiarioType;
    }

    public void setBeneficiarioType(String beneficiarioType) {
        this.beneficiarioType = beneficiarioType;
    }

    public String getBirthDt() {
        return birthDt;
    }

    public void setBirthDt(String birthDt) {
        this.birthDt = birthDt;
    }
}
