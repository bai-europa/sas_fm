package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "PostAddr"
})
public class Locator {
    @JsonProperty("PostAddr")
    private PostAddr postAddr;


    public Locator() {
    }

    @JsonProperty("PostAddr")
    public PostAddr getPostAddr() {
        return postAddr;
    }

    @JsonProperty("PostAddr")
    public void setPostAddr(PostAddr postAddr) {
        this.postAddr = postAddr;
    }
}
