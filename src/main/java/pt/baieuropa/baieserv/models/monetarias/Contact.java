
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ContactName",
        "Locator",
        "OrgContactName"
})
public class Contact {

    @JsonProperty("ContactName")
    private String contactName;
    @JsonProperty("Locator")
    private Locator locator;
    @JsonProperty("OrgContactName")
    private String OrgContactName;

    @JsonProperty("OrgContactName")
    public String getOrgContactName() {
        return OrgContactName;
    }

    @JsonProperty("OrgContactName")
    public void setOrgContactName(String orgContactName) {
        OrgContactName = orgContactName;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ContactName")
    public String getContactName() {
        return contactName;
    }

    @JsonProperty("ContactName")
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @JsonProperty("Locator")
    public Locator getLocator() {
        return locator;
    }

    @JsonProperty("Locator")
    public void setLocator(Locator locator) {
        this.locator = locator;
    }

    public Contact withContactName(String contactName) {
        this.contactName = contactName;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Contact withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(contactName).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Contact) == false) {
            return false;
        }
        Contact rhs = ((Contact) other);
        return new EqualsBuilder().append(contactName, rhs.contactName).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
