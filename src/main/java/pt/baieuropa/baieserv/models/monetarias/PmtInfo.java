
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PrcDt",
    "CurAmt",
    "PmtCreditDetail",
    "DebtorData",
    "PmtInstruction",
    "PmtAddRq",
        "DueDt"
})
public class PmtInfo {

    @JsonProperty("PrcDt")
    private String PrcDt;
    @JsonProperty("CurAmt")
    private CurAmt curAmt;
    @JsonProperty("DueDt")
    String DueDt;
    @JsonProperty("PmtCreditDetail")
    private PmtCreditDetail pmtCreditDetail;
    @JsonProperty("DebtorData")
    private DebtorData debtorData;
    @JsonProperty("PmtInstruction")
    private PmtInstruction pmtInstruction;
    @JsonProperty("PmtAddRq")
    private PmtAddRq pmtAddRq;

    @JsonProperty("DueDt")
    public String getDueDt() {
        return DueDt;
    }

    @JsonProperty("DueDt")
    public void setDueDt(String dueDt) {
        DueDt = dueDt;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PrcDt")
    public String getPrcDt() {
        return PrcDt;
    }
    @JsonProperty("PrcDt")
    public void setPrcDt(String prcDt) {
        PrcDt = prcDt;
    }

    @JsonProperty("CurAmt")
    public CurAmt getCurAmt() {
        return curAmt;
    }

    @JsonProperty("CurAmt")
    public void setCurAmt(CurAmt curAmt) {
        this.curAmt = curAmt;
    }

    public PmtInfo withCurAmt(CurAmt curAmt) {
        this.curAmt = curAmt;
        return this;
    }

    @JsonProperty("PmtCreditDetail")
    public PmtCreditDetail getPmtCreditDetail() {
        return pmtCreditDetail;
    }

    @JsonProperty("PmtCreditDetail")
    public void setPmtCreditDetail(PmtCreditDetail pmtCreditDetail) {
        this.pmtCreditDetail = pmtCreditDetail;
    }

    public PmtInfo withPmtCreditDetail(PmtCreditDetail pmtCreditDetail) {
        this.pmtCreditDetail = pmtCreditDetail;
        return this;
    }

    @JsonProperty("DebtorData")
    public DebtorData getDebtorData() {
        return debtorData;
    }

    @JsonProperty("DebtorData")
    public void setDebtorData(DebtorData debtorData) {
        this.debtorData = debtorData;
    }

    public PmtInfo withDebtorData(DebtorData debtorData) {
        this.debtorData = debtorData;
        return this;
    }

    @JsonProperty("PmtInstruction")
    public PmtInstruction getPmtInstruction() {
        return pmtInstruction;
    }

    @JsonProperty("PmtInstruction")
    public void setPmtInstruction(PmtInstruction pmtInstruction) {
        this.pmtInstruction = pmtInstruction;
    }

    @JsonProperty("PmtAddRq")
    public PmtAddRq getPmtAddRq() {
        return pmtAddRq;
    }

    @JsonProperty("PmtAddRq")
    public void setPmtAddRq(PmtAddRq pmtAddRq) {
        this.pmtAddRq = pmtAddRq;
    }

    public PmtInfo withPmtInstruction(PmtInstruction pmtInstruction) {
        this.pmtInstruction = pmtInstruction;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PmtInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(curAmt).append(pmtCreditDetail).append(debtorData).append(pmtInstruction).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PmtInfo) == false) {
            return false;
        }
        PmtInfo rhs = ((PmtInfo) other);
        return new EqualsBuilder().append(curAmt, rhs.curAmt).append(pmtCreditDetail, rhs.pmtCreditDetail).append(debtorData, rhs.debtorData).append(pmtInstruction, rhs.pmtInstruction).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
