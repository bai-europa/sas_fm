
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "RqUID",
    "PmtInfo"
})
public class PmtAddRq {

    @JsonProperty("RqUID")
    private String rqUID;
    @JsonProperty("PmtInfo")
    private PmtInfo pmtInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("RqUID")
    public String getRqUID() {
        return rqUID;
    }

    @JsonProperty("RqUID")
    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public PmtAddRq withRqUID(String rqUID) {
        this.rqUID = rqUID;
        return this;
    }

    @JsonProperty("PmtInfo")
    public PmtInfo getPmtInfo() {
        return pmtInfo;
    }

    @JsonProperty("PmtInfo")
    public void setPmtInfo(PmtInfo pmtInfo) {
        this.pmtInfo = pmtInfo;
    }

    public PmtAddRq withPmtInfo(PmtInfo pmtInfo) {
        this.pmtInfo = pmtInfo;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PmtAddRq withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(rqUID).append(pmtInfo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PmtAddRq) == false) {
            return false;
        }
        PmtAddRq rhs = ((PmtAddRq) other);
        return new EqualsBuilder().append(rqUID, rhs.rqUID).append(pmtInfo, rhs.pmtInfo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
