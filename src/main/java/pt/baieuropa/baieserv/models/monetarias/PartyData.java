
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Contact",
    "IssuedIdent"
})
public class PartyData {

    @JsonProperty("Contact")
    private Contact contact;
    @JsonProperty("IssuedIdent")
    private IssuedIdent issuedIdent;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Contact")
    public Contact getContact() {
        return contact;
    }

    @JsonProperty("Contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    @JsonProperty("IssuedIdent")
    public IssuedIdent getIssuedIdent() {
        return issuedIdent;
    }

    @JsonProperty("IssuedIdent")
    public void setIssuedIdent(IssuedIdent issuedIdent) {
        this.issuedIdent = issuedIdent;
    }

    public PartyData withContact(Contact contact) {
        this.contact = contact;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PartyData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(contact).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PartyData) == false) {
            return false;
        }
        PartyData rhs = ((PartyData) other);
        return new EqualsBuilder().append(contact, rhs.contact).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
