
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PartyData"
})
public class DebtorData {

    @JsonProperty("PartyData")
    private PartyData partyData;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PartyData")
    public PartyData getPartyData() {
        return partyData;
    }

    @JsonProperty("PartyData")
    public void setPartyData(PartyData partyData) {
        this.partyData = partyData;
    }

    public DebtorData withPartyData(PartyData partyData) {
        this.partyData = partyData;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public DebtorData withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(partyData).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof DebtorData) == false) {
            return false;
        }
        DebtorData rhs = ((DebtorData) other);
        return new EqualsBuilder().append(partyData, rhs.partyData).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
