
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "CurCode",
    "Amt"
})
public class CurAmt {

    @JsonProperty("CurCode")
    private String curCode;
    @JsonProperty("Amt")
    private String amt;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("CurCode")
    public String getCurCode() {
        return curCode;
    }

    @JsonProperty("CurCode")
    public void setCurCode(String curCode) {
        this.curCode = curCode;
    }

    public CurAmt withCurCode(String curCode) {
        this.curCode = curCode;
        return this;
    }

    @JsonProperty("Amt")
    public String getAmt() {
        return amt;
    }

    @JsonProperty("Amt")
    public void setAmt(String amt) {
        this.amt = amt;
    }

    public CurAmt withAmt(String amt) {
        this.amt = amt;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public CurAmt withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(curCode).append(amt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CurAmt) == false) {
            return false;
        }
        CurAmt rhs = ((CurAmt) other);
        return new EqualsBuilder().append(curCode, rhs.curCode).append(amt, rhs.amt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
