package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CountryCode"
})
public class GovIssuedIdent {
    @JsonProperty("CountryCode")
    private CountryCode countryCode;

    @JsonProperty("CountryCode")
    public CountryCode getCountryCode() {
        return countryCode;
    }

    @JsonProperty("CountryCode")
    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }
}
