
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "IBAN",
        "BIC",
        "FIIdent",
        "AcctId"
})
public class ToAcctKeys {

    @JsonProperty("IBAN")
    private String iBAN;
    @JsonProperty("BIC")
    private String bIC;
    @JsonProperty("FIIdent")
    private String FIIdent;
    @JsonProperty("AcctId")
    private String AcctId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



    @JsonProperty("IBAN")
    public String getIBAN() {
        return iBAN;
    }

    @JsonProperty("IBAN")
    public void setIBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public ToAcctKeys withIBAN(String iBAN) {
        this.iBAN = iBAN;
        return this;
    }

    @JsonProperty("BIC")
    public String getBIC() {
        return bIC;
    }

    @JsonProperty("BIC")
    public void setBIC(String bIC) {
        this.bIC = bIC;
    }

    public ToAcctKeys withBIC(String bIC) {
        this.bIC = bIC;
        return this;
    }

    @JsonProperty("FIIdent")
    public String getFIIdent() {
        return FIIdent;
    }
    @JsonProperty("FIIdent")
    public void setFIIdent(String FIIdent) {
        this.FIIdent = FIIdent;
    }
    @JsonProperty("AcctId")
    public String getAcctId() {
        return AcctId;
    }
    @JsonProperty("AcctId")
    public void setAcctId(String acctId) {
        AcctId = acctId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public ToAcctKeys withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(iBAN).append(bIC).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof ToAcctKeys) == false) {
            return false;
        }
        ToAcctKeys rhs = ((ToAcctKeys) other);
        return new EqualsBuilder().append(iBAN, rhs.iBAN).append(bIC, rhs.bIC).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
