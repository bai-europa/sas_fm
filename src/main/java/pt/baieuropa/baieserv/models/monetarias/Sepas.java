
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PmtAddRq"
})
public class Sepas {

    @JsonProperty("PmtAddRq")
    private PmtAddRq pmtAddRq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("PmtAddRq")
    public PmtAddRq getPmtAddRq() {
        return pmtAddRq;
    }

    @JsonProperty("PmtAddRq")
    public void setPmtAddRq(PmtAddRq pmtAddRq) {
        this.pmtAddRq = pmtAddRq;
    }

    public Sepas withPmtAddRq(PmtAddRq pmtAddRq) {
        this.pmtAddRq = pmtAddRq;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Sepas withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pmtAddRq).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Sepas) == false) {
            return false;
        }
        Sepas rhs = ((Sepas) other);
        return new EqualsBuilder().append(pmtAddRq, rhs.pmtAddRq).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
