package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CountryCodeValue"
})
public class TaxCountry {
    @JsonProperty("CountryCodeValue")
    private String CountryCodeValue;

    @JsonProperty("CountryCodeValue")
    public String getCountryCodeValue() {
        return CountryCodeValue;
    }

    @JsonProperty("CountryCodeValue")
    public void setCountryCodeValue(String countryCodeValue) {
        CountryCodeValue = countryCodeValue;
    }
}
