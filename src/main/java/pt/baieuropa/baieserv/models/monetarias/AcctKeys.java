
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AcctId",
    "IBAN"
})
public class AcctKeys {

    @JsonProperty("AcctId")
    private String acctId;
    @JsonProperty("IBAN")
    private String iBAN;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AcctId")
    public String getAcctId() {
        return acctId;
    }

    @JsonProperty("AcctId")
    public void setAcctId(String acctId) {
        this.acctId = acctId;
    }

    public AcctKeys withAcctId(String acctId) {
        this.acctId = acctId;
        return this;
    }

    @JsonProperty("IBAN")
    public String getIBAN() {
        return iBAN;
    }

    @JsonProperty("IBAN")
    public void setIBAN(String iBAN) {
        this.iBAN = iBAN;
    }

    public AcctKeys withIBAN(String iBAN) {
        this.iBAN = iBAN;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AcctKeys withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctId).append(iBAN).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof AcctKeys) == false) {
            return false;
        }
        AcctKeys rhs = ((AcctKeys) other);
        return new EqualsBuilder().append(acctId, rhs.acctId).append(iBAN, rhs.iBAN).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
