package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "Addr1"
})
public class PostAddr {
    @JsonProperty("Addr1")
    private String Addr1;

    public PostAddr() {
    }

    @JsonProperty("Addr1")
    public String getAddr1() {
        return Addr1;
    }

    @JsonProperty("Addr1")
    public void setAddr1(String addr1) {
        Addr1 = addr1;
    }
}
