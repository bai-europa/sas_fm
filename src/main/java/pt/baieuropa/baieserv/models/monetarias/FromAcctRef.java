
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AcctKeys",
        "AcctInfo"
})
public class FromAcctRef {

    @JsonProperty("AcctKeys")
    private AcctKeys acctKeys;

    @JsonProperty("AcctInfo")
    private AcctInfo acctInfo;


    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AcctKeys")
    public AcctKeys getAcctKeys() {
        return acctKeys;
    }

    @JsonProperty("AcctKeys")
    public void setAcctKeys(AcctKeys acctKeys) {
        this.acctKeys = acctKeys;
    }

    public FromAcctRef withAcctKeys(AcctKeys acctKeys) {
        this.acctKeys = acctKeys;
        return this;
    }

    @JsonProperty("AcctInfo")
    public AcctInfo getAcctInfo() {
        return acctInfo;
    }

    @JsonProperty("AcctInfo")
    public void setAcctInfo(AcctInfo acctInfo) {
        this.acctInfo = acctInfo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public FromAcctRef withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(acctKeys).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof FromAcctRef) == false) {
            return false;
        }
        FromAcctRef rhs = ((FromAcctRef) other);
        return new EqualsBuilder().append(acctKeys, rhs.acctKeys).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
