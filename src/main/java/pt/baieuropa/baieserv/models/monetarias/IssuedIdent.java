
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "IssuedIdentValue",
        "GovIssuedIdent",
        "BirthDt",
        "IssuedIdentType"
})
public class IssuedIdent {

    @JsonProperty("IssuedIdentValue")
    private String issuedIdentValue;

    @JsonProperty("GovIssuedIdent")
    private GovIssuedIdent govIssuedIdent;

    @JsonProperty("GovIssuedIdent")
    private String BirthDt;

    @JsonProperty("IssuedIdentType")
    private String IssuedIdentType;

    @JsonProperty("IssuedIdentType")
    public String getIssuedIdentType() {
        return IssuedIdentType;
    }

    @JsonProperty("IssuedIdentType")
    public void setIssuedIdentType(String issuedIdentType) {
        IssuedIdentType = issuedIdentType;
    }

    @JsonProperty("IssuedIdentValue")
    public String getIssuedIdentValue() {
        return issuedIdentValue;
    }

    @JsonProperty("IssuedIdentValue")
    public void setIssuedIdentValue(String issuedIdentValue) {
        this.issuedIdentValue = issuedIdentValue;
    }

    @JsonProperty("GovIssuedIdent")
    public GovIssuedIdent getGovIssuedIdent() {
        return govIssuedIdent;
    }

    @JsonProperty("GovIssuedIdent")
    public void setGovIssuedIdent(GovIssuedIdent govIssuedIdent) {
        this.govIssuedIdent = govIssuedIdent;
    }

    @JsonProperty("BirthDt")
    public String getBirthDt() {
        return BirthDt;
    }

    @JsonProperty("BirthDt")
    public void setBirthDt(String birthDt) {
        BirthDt = birthDt;
    }
}
