
package pt.baieuropa.baieserv.models.monetarias;

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "DueDt",
        "FromAcctRef",
        "RefData",
        "PmtType",
        "PmtMethod",
        "TPPId",
        "Memo",
        "StrongAuth",
        "TransferReason",
        "TransferReasonCd",
        "DDReference",
        "OrgAuthorization"
})
public class PmtInstruction {

    @JsonProperty("DueDt")
    private String dueDt;
    @JsonProperty("FromAcctRef")
    private FromAcctRef fromAcctRef;
    @JsonProperty("RefData")
    private RefData refData;
    @JsonProperty("PmtType")
    private String pmtType;
    @JsonProperty("PmtMethod")
    private String PmtMethod;
    @JsonProperty("TPPId")
    private String TPPId;
    @JsonProperty("Memo")
    private String Memo;
    @JsonProperty("StrongAuth")
    private String StrongAuth;
    @JsonProperty("TransferReason")
    private String TransferReason;
    @JsonProperty("TransferReasonCd")
    private String TransferReasonCd;
    @JsonProperty("DDReference")
    private String DDReference;
    @JsonProperty("OrgAuthorization")
    private String OrgAuthorization;

    @JsonProperty("OrgAuthorization")
    public String getOrgAuthorization() {
        return OrgAuthorization;
    }

    @JsonProperty("OrgAuthorization")
    public void setOrgAuthorization(String orgAuthorization) {
        OrgAuthorization = orgAuthorization;
    }

    @JsonProperty("DDReference")
    public String getDDReference() {
        return DDReference;
    }

    @JsonProperty("DDReference")
    public void setDDReference(String DDReference) {
        this.DDReference = DDReference;
    }

    @JsonProperty("TransferReason")
    public String getTransferReason() {
        return TransferReason;
    }

    @JsonProperty("TransferReason")
    public void setTransferReason(String transferReason) {
        TransferReason = transferReason;
    }

    @JsonProperty("TransferReasonCd")
    public String getTransferReasonCd() {
        return TransferReasonCd;
    }

    @JsonProperty("TransferReasonCd")
    public void setTransferReasonCd(String transferReasonCd) {
        TransferReasonCd = transferReasonCd;
    }

    @JsonProperty("PmtMethod")
    public String getPmtMethod() {
        return PmtMethod;
    }

    @JsonProperty("PmtMethod")
    public void setPmtMethod(String pmtMethod) {
        PmtMethod = pmtMethod;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("DueDt")
    public String getDueDt() {
        return dueDt;
    }

    @JsonProperty("DueDt")
    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }

    public PmtInstruction withDueDt(String dueDt) {
        this.dueDt = dueDt;
        return this;
    }

    @JsonProperty("FromAcctRef")
    public FromAcctRef getFromAcctRef() {
        return fromAcctRef;
    }

    @JsonProperty("FromAcctRef")
    public void setFromAcctRef(FromAcctRef fromAcctRef) {
        this.fromAcctRef = fromAcctRef;
    }

    public PmtInstruction withFromAcctRef(FromAcctRef fromAcctRef) {
        this.fromAcctRef = fromAcctRef;
        return this;
    }

    @JsonProperty("RefData")
    public RefData getRefData() {
        return refData;
    }

    @JsonProperty("RefData")
    public void setRefData(RefData refData) {
        this.refData = refData;
    }

    public PmtInstruction withRefData(RefData refData) {
        this.refData = refData;
        return this;
    }

    @JsonProperty("PmtType")
    public String getPmtType() {
        return pmtType;
    }

    @JsonProperty("PmtType")
    public void setPmtType(String pmtType) {
        this.pmtType = pmtType;
    }

    public PmtInstruction withPmtType(String pmtType) {
        this.pmtType = pmtType;
        return this;
    }

    @JsonProperty("TPPId")
    public String getTPPId() {
        return TPPId;
    }
    @JsonProperty("TPPId")
    public void setTPPId(String TPPId) {
        this.TPPId = TPPId;
    }


    @JsonProperty("Memo")
    public String getMemo() {
        return Memo;
    }

    @JsonProperty("Memo")
    public void setMemo(String memo) {
        Memo = memo;
    }

    @JsonProperty("StrongAuth")
    public String getStrongAuth() {
        return StrongAuth;
    }

    @JsonProperty("StrongAuth")
    public void setStrongAuth(String strongAuth) {
        StrongAuth = strongAuth;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public PmtInstruction withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(dueDt).append(fromAcctRef).append(refData).append(pmtType).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PmtInstruction) == false) {
            return false;
        }
        PmtInstruction rhs = ((PmtInstruction) other);
        return new EqualsBuilder().append(dueDt, rhs.dueDt).append(fromAcctRef, rhs.fromAcctRef).append(refData, rhs.refData).append(pmtType, rhs.pmtType).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
