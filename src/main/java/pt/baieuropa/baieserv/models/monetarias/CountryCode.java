package pt.baieuropa.baieserv.models.monetarias;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "CountryCodeValue"
})
public class CountryCode {
    @JsonProperty("GovIssuedIdent")
    private String CountryCodeValue;

    public CountryCode() {
    }

    @JsonProperty("GovIssuedIdent")
    public String getCountryCodeValue() {
        return CountryCodeValue;
    }

    @JsonProperty("GovIssuedIdent")
    public void setCountryCodeValue(String countryCodeValue) {
        CountryCodeValue = countryCodeValue;
    }
}
