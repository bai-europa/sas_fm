
package pt.baieuropa.baieserv.models.informacaoLogin;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "PrcDt",
        "StrongAuth",
        "SecObjPurpose",
        "Browser",
        "OPSystem",
        "SecObjIP",
        "SecObjGeoLoc",
        "Device"
})
public class SecObjInfo {

    @JsonProperty("PrcDt")
    private String prcDt;

    @JsonProperty("StrongAuth")
    private String StrongAuth;

    @JsonProperty("SecObjPurpose")
    private String SecObjPurpose;

    @JsonProperty("Browser")
    private String Browser;

    @JsonProperty("OPSystem")
    private String OPSystem;

    @JsonProperty("SecObjIP")
    private String SecObjIP;

    @JsonProperty("SecObjGeoLoc")
    private String SecObjGeoLoc;

    @JsonProperty("Device")
    private String Device;

    @JsonProperty("OPSystem")
    public String getOPSystem() {
        return OPSystem;
    }

    @JsonProperty("OPSystem")
    public void setOPSystem(String OPSystem) {
        this.OPSystem = OPSystem;
    }

    @JsonProperty("SecObjIP")
    public String getSecObjIP() {
        return SecObjIP;
    }

    @JsonProperty("SecObjIP")
    public void setSecObjIP(String secObjIP) {
        SecObjIP = secObjIP;
    }

    @JsonProperty("SecObjGeoLoc")
    public String getSecObjGeoLoc() {
        return SecObjGeoLoc;
    }

    @JsonProperty("SecObjGeoLoc")
    public void setSecObjGeoLoc(String secObjGeoLoc) {
        SecObjGeoLoc = secObjGeoLoc;
    }

    @JsonProperty("Device")
    public String getDevice() {
        return Device;
    }

    @JsonProperty("Device")
    public void setDevice(String device) {
        Device = device;
    }

    @JsonProperty("PrcDt")
    public String getPrcDt() {
        return prcDt;
    }

    @JsonProperty("PrcDt")
    public void setPrcDt(String prcDt) {
        this.prcDt = prcDt;
    }

    @JsonProperty("Browser")
    public String getBrowser() {
        return Browser;
    }

    @JsonProperty("Browser")
    public void setBrowser(String browser) {
        Browser = browser;
    }

    @JsonProperty("SecObjPurpose")
    public String getSecObjPurpose() {
        return SecObjPurpose;
    }

    @JsonProperty("SecObjPurpose")
    public void setSecObjPurpose(String secObjPurpose) {
        SecObjPurpose = secObjPurpose;
    }

    @JsonProperty("StrongAuth")
    public String getStrongAuth() {
        return StrongAuth;
    }

    @JsonProperty("StrongAuth")
    public void setStrongAuth(String strongAuth) {
        StrongAuth = strongAuth;
    }

    public SecObjInfo withPrcDt(String prcDt) {
        this.prcDt = prcDt;
        return this;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SecObjInfo withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(prcDt).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SecObjInfo) == false) {
            return false;
        }
        SecObjInfo rhs = ((SecObjInfo) other);
        return new EqualsBuilder().append(prcDt, rhs.prcDt).append(additionalProperties, rhs.additionalProperties).isEquals();
    }


}
