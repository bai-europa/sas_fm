
package pt.baieuropa.baieserv.models.informacaoLogin;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SecObjAddRq"
})
public class InformacaoLogin {

    @JsonProperty("SecObjAddRq")
    private SecObjAddRq secObjAddRq;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SecObjAddRq")
    public SecObjAddRq getSecObjAddRq() {
        return secObjAddRq;
    }

    @JsonProperty("SecObjAddRq")
    public void setSecObjAddRq(SecObjAddRq secObjAddRq) {
        this.secObjAddRq = secObjAddRq;
    }

    public InformacaoLogin withSecObjAddRq(SecObjAddRq secObjAddRq) {
        this.secObjAddRq = secObjAddRq;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public InformacaoLogin withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(secObjAddRq).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof InformacaoLogin) == false) {
            return false;
        }
        InformacaoLogin rhs = ((InformacaoLogin) other);
        return new EqualsBuilder().append(secObjAddRq, rhs.secObjAddRq).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
