
package pt.baieuropa.baieserv.models.informacaoLogin;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SecObjInfo",
    "RqUID",
    "PartyInfo"
})
public class SecObjAddRq {

    @JsonProperty("SecObjInfo")
    private SecObjInfo secObjInfo;
    @JsonProperty("RqUID")
    private String rqUID;
    @JsonProperty("PartyInfo")
    private PartyInfo partyInfo;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("SecObjInfo")
    public SecObjInfo getSecObjInfo() {
        return secObjInfo;
    }

    @JsonProperty("SecObjInfo")
    public void setSecObjInfo(SecObjInfo secObjInfo) {
        this.secObjInfo = secObjInfo;
    }

    public SecObjAddRq withSecObjInfo(SecObjInfo secObjInfo) {
        this.secObjInfo = secObjInfo;
        return this;
    }

    @JsonProperty("RqUID")
    public String getRqUID() {
        return rqUID;
    }

    @JsonProperty("RqUID")
    public void setRqUID(String rqUID) {
        this.rqUID = rqUID;
    }

    public SecObjAddRq withRqUID(String rqUID) {
        this.rqUID = rqUID;
        return this;
    }

    @JsonProperty("PartyInfo")
    public PartyInfo getPartyInfo() {
        return partyInfo;
    }

    @JsonProperty("PartyInfo")
    public void setPartyInfo(PartyInfo partyInfo) {
        this.partyInfo = partyInfo;
    }

    public SecObjAddRq withPartyInfo(PartyInfo partyInfo) {
        this.partyInfo = partyInfo;
        return this;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public SecObjAddRq withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(secObjInfo).append(rqUID).append(partyInfo).append(additionalProperties).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof SecObjAddRq) == false) {
            return false;
        }
        SecObjAddRq rhs = ((SecObjAddRq) other);
        return new EqualsBuilder().append(secObjInfo, rhs.secObjInfo).append(rqUID, rhs.rqUID).append(partyInfo, rhs.partyInfo).append(additionalProperties, rhs.additionalProperties).isEquals();
    }

}
