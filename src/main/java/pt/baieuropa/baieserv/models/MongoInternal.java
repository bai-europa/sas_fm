package pt.baieuropa.baieserv.models;

import pt.baieuropa.baieserv.models.monetarias.PmtAddRq;

public class MongoInternal {

    PmtAddRq pmtAddRq;

    public MongoInternal(PmtAddRq pmtAddRq) {
        this.pmtAddRq = pmtAddRq;
    }

    public PmtAddRq getPmtAddRq() {
        return pmtAddRq;
    }

    public void setPmtAddRq(PmtAddRq pmtAddRq) {
        this.pmtAddRq = pmtAddRq;
    }
}
